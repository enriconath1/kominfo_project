@extends('backend.layouts.default')
@section('page-title','Daftar Intitusi')


@section('content')
	<div class="row">
		<div class="col-lg-12">
			<!--begin:: Widgets/Stats-->
			<div class="m-portlet">
				<div class="m-portlet__body  m-portlet__body--no-padding">
					<div class="row m-row--no-padding m-row--col-separator-xl">
						<div class="col-md-12 col-lg-6 col-xl-3">
							<!--begin::Total Profit-->
							<div class="m-widget24" style="padding-bottom: 30px;">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										Jumlah Serangan
									</h4>
									<br>
									<br>
									<span class="m-widget24__stats m--font-info">
										<div id="total-attack-loader">
											<div class='m-loader m-loader--brand' style="width: 30px; display: inline-block;"></div>
										</div>
										<span id="total-attack-value"></span>
									</span>
									<div class="m--space-10"></div>
								</div>
							</div>
							<!--end::Total Profit-->
						</div>
						<div class="col-md-12 col-lg-6 col-xl-3">
							<!--begin::Total Profit-->
							<div class="m-widget24" style="padding-bottom: 30px;">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										IP Yang Terdeteksi
									</h4>
									<br>
									<br>
									<span class="m-widget24__stats m--font-brand">
										<div id="unique-attack-loader">
											<div class='m-loader m-loader--brand' style="width: 30px; display: inline-block;"></div>
										</div>
										<span id="unique-attack-value"></span>
									</span>
									<div class="m--space-10"></div>
								</div>
							</div>
							<!--end::Total Profit-->
						</div>
						<div class="col-md-12 col-lg-6 col-xl-3">
							<!--begin::Total Profit-->
							<div class="m-widget24" style="padding-bottom: 30px;">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										Serangan Malware
									</h4>
									<br>
									<br>
									<span class="m-widget24__stats m--font-danger">
										<div id="total-malware-attack-loader">
											<div class='m-loader m-loader--brand' style="width: 30px; display: inline-block;"></div>
										</div>
										<span id="total-malware-attack-value"></span>
									</span>
									<div class="m--space-10"></div>
								</div>
							</div>
							<!--end::Total Profit-->
						</div>
						<div class="col-md-12 col-lg-6 col-xl-3">
							<!--begin::Total Profit-->
							<div class="m-widget24" style="padding-bottom: 30px;">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										Jumlah Negara
									</h4>
									<br>
									<br>
									<span class="m-widget24__stats m--font-success">
										<div id="total-country-loader">
											<div class='m-loader m-loader--brand' style="width: 30px; display: inline-block;"></div>
										</div>
										<span id="total-country-value"></span>
									</span>
									<div class="m--space-10"></div>
								</div>
							</div>
							<!--end::Total Profit-->
						</div>
					</div>
				</div>
			</div>
			<!--end:: Widgets/Stats-->
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="m-portlet">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										Daftar Institusi
									</h3>
								</div>
							</div>
						</div>
						<div class="m-portlet__body">
							<!--begin::Section-->
							<div class="m-section">
								<div class="m-section__content">
									<div id="loading-table">
										<div class='m-loader m-loader--brand'></div>
									</div>
									<table class="table hidden" id="institution-table">
										<thead>
											<tr>
												<th> Nama Institusi</th>
												<th> Jumlah Serangan </th>
											</tr>
										</thead>
										<tbody style="overflow: scroll; height: 500px;">
										</tbody>
									</table>
								</div>
							</div>
							<!--end::Section-->
						</div>
						<!--end::Section-->
					</div>
					<!--end::Portlet-->
				</div>
			</div>
		</div>

		<!--begin::Modal-->
		<div class="modal fade" id="modal-institution-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">
							Detail Alamat IP
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">
								&times;
							</span>
						</button>
					</div>
					<div class="modal-body">
						<ul class="nav nav-tabs  m-tabs-line m-tabs-line--success" role="tablist">
							<li class="nav-item m-tabs__item">
								<a class="nav-link m-tabs__link active" id="who-is-tab-button" data-toggle="tab" href="#who-is-tabs" role="tab">
									<i class="la la-cloud-upload"></i>
									who.is
								</a>
							</li>
							<li class="nav-item m-tabs__item">
								<a class="nav-link m-tabs__link" id="target-port-tab-button" data-toggle="tab" href="#target-port-tabs" role="tab">
									<i class="la la-cloud-upload"></i>
									Port Sasaran
								</a>
							</li>
							<li class="nav-item m-tabs__item">
								<a class="nav-link m-tabs__link" id="malware-tab-button" data-toggle="tab" href="#malware-tabs" role="tab">
									<i class="la la-puzzle-piece"></i>
									Malware
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="who-is-tabs" role="tabpanel">
								<div id="who-is-loading" style="margin-top:20px">
									<div class='m-loader m-loader--brand'></div>
								</div>
								<div id="who-is" class="hidden"></div>
							</div>
							<div class="tab-pane" id="target-port-tabs" role="tabpanel">
								<div id="target-port-loading" style="margin-top:20px">
									<div class='m-loader m-loader--brand'></div>
								</div>
								<div id="target-port">
									<table class="table m-table m-table--head-bg-brand" id="target-port-table">
										<thead>
											<tr>
												<th>Port</th>
												<th>Jumlah Serangan</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane" id="malware-tabs" role="tabpanel">
								<div id="malware-loading" style="margin-top:20px">
									<div class='m-loader m-loader--brand'></div>
								</div>
								<div id="malware">
									<table class="table m-table m-table--head-bg-brand" id="malware-table">
										<thead>
											<tr>
												<th>Malware Hash</th>
												<th>Jumlah Malware</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">
							Close
						</button>
					</div>
				</div>
			</div>
		</div>
		<!--end::Modal-->
@endsection

@section('style')
	<!--begin::Page Vendors -->
	<!-- <link href="/assets/vendors/custom/jqvmap/jqvmap.bundle.css" rel="stylesheet" type="text/css" /> -->
	<!-- <link href="/css/scrolltable.css" rel="stylesheet" type="text/css" /> -->
	<!--end::Page Vendors -->
@endsection
@section('head-script')
@endsection

@section('script')

	<!--begin::Page Vendors -->
	<!-- <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script> -->

	<!-- <script src="/assets/vendors/custom/jqvmap/jqvmap.bundle.js" type="text/javascript"></script> -->
	<!-- <script src="/js/dashboard.js" type="text/javascript"></script> -->
	<!--end::Page Vendors -->
	<script>

	</script>
	
	<!--begin::Page Resources -->
	<!-- <script src="/assets/demo/default/custom/components/maps/jqvmap.js" type="text/javascript"></script> -->
	<!--end::Page Resources -->
	<!--begin::Page Resources -->
	<!-- <script src="/assets/demo/default/custom/components/forms/widgets/select2.js" type="text/javascript"></script> -->
	<!--end::Page Resources -->

	<script>	
	var baseUrl="{{ env('APP_API_URL') }}";
	var limit=100;
	var offset=0;
	var limitDetailPort=10;
	var offsetDetailPort=0;
	var limitDetailMalware=10;
	var offsetDetailMalware=0;
	function ipDetail(ip){
		$('#modal-institution-detail').modal('show'); 
		// tabs
		$('#who-is-tab-button').addClass("active");
		$('#target-port-tab-button').removeClass("active");
		$('#malware-tab-button').removeClass("active");

		$('#who-is-tabs').addClass("active");
		$('#target-port-tabs').removeClass("active");
		$('#malware-tabs').removeClass("active");
		// $('#who-is-tab-button').attr("aria-expanded","true");
		// $('#target-port-tab-button').attr("aria-expanded","false");
		// $('#malware-tab-button').attr("aria-expanded","false");


        $('#who-is-loading').removeClass('hidden');
		$("#who-is").addClass("hidden");
        $('#target-port-loading').removeClass('hidden');
		$("#target-port-table").addClass("hidden");
        $('#malware-loading').removeClass('hidden');
		$("#malware-table").addClass("hidden");
    	console.log("load IP Detail "+ip);
		

		axios.get(baseUrl + "/api/ip/detail/who-is/"+ip).then(response => {
			console.log('Whois Data:');
	        console.log(response.data);

	        $.each(response.data.rawdata, function(key, value){
	            $("#who-is").append(value + "</br>");
	        });
	        $('#who-is-loading').addClass('hidden');
            $("#who-is").removeClass("hidden");
			console.log('Detail IP Whois Mounted');
		});

		axios.get(baseUrl + "/api/ip/detail/port-list/"+ip+"/"+limitDetailPort+"/"+offsetDetailPort).then(response => {
			console.log('Port List Data:');
			offsetDetailPort+=limitDetailPort;
	        console.log(response.data.data);
	        if(jQuery.isEmptyObject(response.data.data)){
				$('#target-port-table tbody').append("<tr><td colspan='2' style='text-align:center;'>Tidak ada data.</td></tr>");
	        } else {
	        	$('#target-port-table tbody').html('');
		        $.each(response.data.data, function(key, value){
					selectCode = "<tr>";
					selectCode+= "<td>"+value.attackedPort+"</td>";
					selectCode+= "<td>"+numberWithCommas(value.count)+"</td>";
					selectCode+="</tr>";		
					$('#target-port-table tbody').append(selectCode);
		        });
	        }
	        $('#target-port-loading').addClass('hidden');
            $("#target-port-table").removeClass("hidden");
			console.log('Detail IP Port List Mounted');
		});

		axios.get(baseUrl + "/api/ip/detail/malware-list/"+ip+"/"+offsetDetailMalware+"/"+limitDetailMalware).then(response => {
			console.log('Malware Data:');
			offsetDetailMalware+=limitDetailMalware;
	        console.log(response.data.data);

	        if(jQuery.isEmptyObject(response.data.data)){
				$('#malware-table tbody').append("<tr><td colspan='2' style='text-align:center;'>Tidak ada data.</td></tr>");
	        } else {
	        	$('#malware-table tbody').html('');
		        $.each(response.data.data, function(key, value){
					selectCode = "<tr>";
					selectCode+= "<td><a href='"+value.malwareUrl+"' target='_blank'>"+value.malwareMd5+"</a></td>";
					selectCode+= "<td>"+numberWithCommas(value.count)+"</td>";
					selectCode+="</tr>";		
					$('#malware-table tbody').append(selectCode);
		        });
		    }
	        $('#malware-loading').addClass('hidden');
            $("#malware-table").removeClass("hidden");

			console.log('Detail IP Malware Mounted');
		});
	}


	jQuery(document).ready(function() {    
	        // enable clear button 
	        $('.datepicker').datepicker({
	            todayBtn: "linked",
	            clearBtn: true,
	            todayHighlight: true,
	            format : "dd-mm-yyyy",
	            templates: {
	                leftArrow: '<i class="la la-angle-left"></i>',
	                rightArrow: '<i class="la la-angle-right"></i>'
	            }
	        });
	        console.log('DatePicker Created');

			// axios.get(baseUrl + "/api/country/country-list").then(response => {
			// 	$.each(response.data.data,function(key,value){
			// 		selectCode = "<option value='"+value.countryCode+"''>"+value.countryName+"</option>";		
			// 		$('#countrySelect').append(selectCode);
			// 	});
			// 	$('#countrySelect').prop('disabled',false);
			// 	console.log('Country Mounted');
			// });
    		$('#countrySelect').select2();
	        console.log('Select2 Created');

			axios.get(baseUrl + "/api/institution/institution-list").then(response => {
				offset+=limit;
				console.log('Institusi List Data:');
				console.log(response.data);
				$.each(response.data.data,function(key,value){
					selectCode = "<tr>";
					selectCode+= "<td>"+value.instituteName+"</td>";
					selectCode+= "<td>"+numberWithCommas(value.count)+"</td>";
					selectCode+="</tr>";		
					$('#institution-table tbody').append(selectCode);
				});
    			$('#loading-table').addClass('hidden');
    			$('#institution-table').removeClass('hidden');
				// $('#countrySelect').prop('disabled',false);
				console.log('Institusi Mounted');
			});
			
			axios.get(baseUrl + "/api/widget/total-attack").then(response => {
				$('#total-attack-value').text(numberWithCommas(response.data.data.count));
				$('#total-attack-value').removeClass('hidden');
				$('#total-attack-loader').addClass('hidden');
				console.log('Widget totalAttack Mounted');
			});

			axios.get(baseUrl + "/api/malware/total-malware-attack").then(response => {
				$('#total-malware-attack-value').text(numberWithCommas(response.data.data.count));
				$('#total-malware-attack-value').removeClass('hidden');
				$('#total-malware-attack-loader').addClass('hidden');
				console.log('Widget totalMalwareAttack Mounted');
			});

			axios.get(baseUrl + "/api/widget/total-unique-attack").then(response => {
				$('#unique-attack-value').text(numberWithCommas(response.data.data.count));
				$('#unique-attack-value').removeClass('hidden');
				$('#unique-attack-loader').addClass('hidden');
				console.log('Widget Unique Attack Mounted');
			});

			axios.get(baseUrl + "/api/widget/total-country").then(response => {
				$('#total-country-value').text(numberWithCommas(response.data.data.count));
				$('#total-country-value').removeClass('hidden');
				$('#total-country-loader').addClass('hidden');
				console.log('Widget totalCountry Mounted');
			});
		});
	</script>

@endsection