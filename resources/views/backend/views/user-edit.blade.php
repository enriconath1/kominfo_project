@extends('backend.layouts.default')
@section('page-title','Edit User')


@section('content')
	<!--end:: Widgets/Stats-->
	<div class="row">
		<div class="col-md-12">
			<!--begin::Portlet-->
			<div class="m-portlet m-portlet--tab">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Form Edit User
							</h3>
						</div>
					</div>
				</div>
				<!--begin::Form-->
				@foreach($userList as $user)
				<form class="m-form m-form--fit m-form--label-align-right" action="{{route('backend.user.update', $user->id)}}" method="POST">
					{{ csrf_field() }}
					<div class="m-portlet__body">
						<div class="row">
							<div class="col-md-6">
								
								<div class="form-group m-form__group">
									<label for="user-name">
										Nama :
									</label>
									<input type="text" class="form-control m-input" id="user-name" name="user-name" value="{{ $user->name }}" >
								</div>
								<div class="form-group m-form__group">
									<label for="user-phone">
										No Telp :
									</label>
									<input type="text" class="form-control m-input" id="user-phone" name="user-phone" value="{{ $user->telp }}">
									<input type="hidden" class="form-control" name="sessionKey" value="{{$sessionKey}}">
									
								</div>
								@endforeach
								<div class="form-group m-form__group">
									<label for="user-institution-select">
										Institusi :
									</label>
									
									<select class="form-control select2" id="user-institution-select" name="user-institution-select">
										<option value="{{ $user->institutionId }}">{{ $user->institutionName }}</option>
										@foreach($institute as $institusi)
										<option value="{{ $institusi->instituteId }}">{{$institusi->instituteName}}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group m-form__group">
									<label for="user-role">
										Peran :
									</label>
									
									<select class="form-control select2" id="user-role" name="user-role">
										<option value="{{$user->roleId}}">{{$user->roleName}}</option>
										@foreach($role as $role)
										<option value="{{ $role->id }}">{{$role->roleName}}</option>
										@endforeach
									</select>
								</div>

								
							</div>
							<div class="col-md-6">
							</div>
						</div>
					</div>
					<div class="m-portlet__foot m-portlet__foot--fit">
						<div class="m-form__actions">
							<button type="submit" class="btn btn-primary pull-right">
								Edit User
							</button>
						</div>
					</div>
				</form>
				<!--end::Form-->


				




			</div>
			<!--end::Portlet-->
		</div>
	</div>
@endsection

@section('style')
@endsection
@section('head-script')
@endsection

@section('script')

	<script>


	jQuery(document).ready(function() {    
	    console.log('Select2 Created');
	});

	</script>

@endsection