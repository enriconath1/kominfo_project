@extends('backend.layouts.default')
@section('page-title','Edit Sensor')


@section('content')

	<!--begin:: Widgets/Stats-->
	<div class="m-portlet">
		<div class="m-portlet__body  m-portlet__body--no-padding">
			<div class="row m-row--no-padding m-row--col-separator-xl">
			</div>
		</div>
	</div>
	<!--end:: Widgets/Stats-->
	<div class="row">
		<div class="col-md-12">
			<!--begin::Portlet-->
			<div class="m-portlet m-portlet--tab">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Form Edit Sensor
							</h3>
						</div>
					</div>
				</div>

				<!--begin::Form-->
				<form class="m-form m-form--fit m-form--label-align-right" id="regForm">
					<div>
						<div class="tab">
							<center><h2>Pilih Kategori:</h2></center>
		
							<div style="margin: 10px 0px;">
								<label class="m-radio">
									<input type="radio" name="is_category_registered" value="true" checked>
									Sudah Terdaftar
									<span></span>
								</label>
								<select class="form-control m-input" id="kategori_id" name="kategori_id">
									<option value="1">Pemerintah</option>
									<option value="1">CERT</option>
									<option value="1">Perseroan Terbatas</option>
								</select>
							</div>
							<div style="margin: 10px 0px;">
								<label class="m-radio">
									<input type="radio" name="is_category_registered" value="false">
									Tambah Baru
									<span></span>
								</label>
								<div class="form-group">
									<input type="text" class="form-control m-input" id="new_kategori" placeholder="Masukkan Nama Kategori Baru" name="nama_kategori">
								</div>
							</div>
						</div>
						<div class="tab">
							<center><h2>Pilih Institusi:</h2></center>
		
							<div style="margin: 10px 0px;">
								<label class="m-radio">
									<input type="radio" name="is_institute_registered" value="true" checked>
									Sudah Terdaftar
									<span></span>
								</label>
								<select class="form-control m-input" id="institusi_id" name="institusi_id">
									<option value="1">ID-SIRTII</option>
								</select>
							</div>
							<div style="margin: 10px 0px;">
								<label class="m-radio">
									<input type="radio" name="is_institute_registered" value="false">
									Tambah Baru
									<span></span>
								</label>
								<div id="new_institusi">
									<div class="form-group">
										<input type="text" class="form-control m-input" id="nama_institusi" placeholder="Masukkan Nama Kategori Baru" name="nama_institusi">
									</div>
									<div class="form-group">
										<input type="text" class="form-control m-input" id="alamat_institusi" placeholder="Masukkan Nama Kategori Baru" name="alamat_institusi">
									</div>
									<div class="form-group">
										<input type="text" class="form-control m-input" id="kategori_institusi" placeholder="Masukkan Nama Kategori Baru" name="kategori_institusi" disabled>
									</div>
									<select class="form-control m-input" id="provinsi" name="provinsi">
										<option value="1">JAKARTA</option>
										<option value="1">BANTEN</option>
									</select>
								</div>
							</div>
						</div>
						<div class="tab">
							<center><h2>Pilih User:</h2></center>
		
							<div style="margin: 10px 0px;">
								<label class="m-radio">
									<input type="radio" name="is_institute_registered" value="true" checked>
									Sudah Terdaftar
									<span></span>
								</label>
								<div class="user" style="margin: 10px 0px;">
									<h3>User 1:</h3>
									<select class="form-control m-input" id="user_id1" name="user_id1">
										<option value="1">ID-SIRTII</option>
									</select>
								</div>
								<div class="user" style="margin: 10px 0px;">
									<h3>User 2:</h3>
									<select class="form-control m-input" id="user_id2" name="user_id2">
										<option value="1">ID-SIRTII</option>
									</select>
								</div>
							</div>
							<div style="margin: 10px 0px;">
								<label class="m-radio">
									<input type="radio" name="is_institute_registered" value="false">
									Tambah Baru
									<span></span>
								</label>
								<div id="new_institusi" class="row">
									<div class="col-md-6">
										<h3>User 1</h3>
										<div class="form-group">
											<input type="text" class="form-control m-input" id="nama_user" placeholder="Masukkan Nama" name="nama_user">
										</div>
										<div class="form-group">
											<input type="text" class="form-control m-input" id="nomor_telp_user" placeholder="Masukkan Nomor Telepon" name="nomor_telp_user">
										</div>
										<div class="form-group">
											<input type="text" class="form-control m-input" id="alamat_user" placeholder="Masukkan Alamat" name="alamat_user">
										</div>
										<select class="form-control m-input" id="user_institusi" name="user_institusi" disabled style="margin: 10px 0px;">
											<option value="1">{{$faker->name}}</option>
											<option value="1">{{$faker->name}}</option>
										</select>
										<select class="form-control m-input" id="paran_user_institusi" name="paran_user_institusi" style="margin: 10px 0px;">
											<option value="1">{{$faker->name}}</option>
											<option value="1">{{$faker->name}}</option>
										</select>
										<div class="form-group">
											<input type="password" class="form-control m-input" id="password" placeholder="Masukkan Password" name="password">
										</div>
									</div>
									<div class="col-md-6">
										<h3>User 2</h3>
										<div class="form-group">
											<input type="text" class="form-control m-input" id="nama_user" placeholder="Masukkan Nama" name="nama_user">
										</div>
										<div class="form-group">
											<input type="text" class="form-control m-input" id="nomor_telp_user" placeholder="Masukkan Nomor Telepon" name="nomor_telp_user">
										</div>
										<div class="form-group">
											<input type="text" class="form-control m-input" id="alamat_user" placeholder="Masukkan Alamat" name="alamat_user">
										</div>
										<select class="form-control m-input" id="user_institusi" name="user_institusi" disabled>
											<option value="1">{{$faker->name}}</option>
											<option value="1">{{$faker->name}}</option>
										</select>
										<select class="form-control m-input" id="paran_user_institusi" name="paran_user_institusi">
											<option value="1">{{$faker->name}}</option>
											<option value="1">{{$faker->name}}</option>
										</select>
										<div class="form-group">
											<input type="password" class="form-control m-input" id="password" placeholder="Masukkan Password" name="password">
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
					<!-- One "tab" for each step in the form: -->

					<div style="overflow:auto;">
					  <div style="float:right;">
					    <button type="button" id="prevBtn">Previous</button>
					    <button type="button" id="nextBtn">Next</button>
					  </div>
					</div>

					<!-- Circles which indicates the steps of the form: -->
					<div style="text-align:center;margin-top:40px;">
					  <span class="step"></span>
					  <span class="step"></span>
					  <span class="step"></span>
					  <span class="step"></span>
					  <span class="step"></span>
					</div>
				</form>
				<!--end::Form-->
			</div>
			<!--end::Portlet-->
		</div>
	</div>
@endsection

@section('style')
	<link rel="stylesheet" href="{{ asset('css/form-wizard.css') }}">
@endsection

@section('script')
	<script src="{{ asset('js/form-wizard.js')}}" type="text/javascript"></script>
	<!--begin::Page Resources -->
	<script src="/assets/demo/default/custom/components/forms/widgets/select2.js" type="text/javascript"></script>
	<!--end::Page Resources -->
@endsection