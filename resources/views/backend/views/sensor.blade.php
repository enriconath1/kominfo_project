@extends('backend.layouts.default')
@section('page-title','Daftar Malware')


@section('content')
	<div class="row">
		<div class="col-lg-12">
			<!--begin:: Widgets/Stats-->
			<div class="m-portlet">
				<div class="m-portlet__body  m-portlet__body--no-padding">
					<div class="row m-row--no-padding m-row--col-separator-xl">
						<div class="col-md-12 col-lg-6 col-xl-3">
							<!--begin::Total Profit-->
							<div class="m-widget24" style="padding-bottom: 30px;">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										Jumlah Serangan
									</h4>
									<br>
									<br>
									<span class="m-widget24__stats m--font-info">
										<div id="total-attack-loader">
											<div class='m-loader m-loader--brand' style="width: 30px; display: inline-block;"></div>
										</div>
										<span id="total-attack-value"></span>
									</span>
									<div class="m--space-10"></div>
								</div>
							</div>
							<!--end::Total Profit-->
						</div>
						<div class="col-md-12 col-lg-6 col-xl-3">
							<!--begin::Total Profit-->
							<div class="m-widget24" style="padding-bottom: 30px;">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										IP Yang Terdeteksi
									</h4>
									<br>
									<br>
									<span class="m-widget24__stats m--font-brand">
										<div id="unique-attack-loader">
											<div class='m-loader m-loader--brand' style="width: 30px; display: inline-block;"></div>
										</div>
										<span id="unique-attack-value"></span>
									</span>
									<div class="m--space-10"></div>
								</div>
							</div>
							<!--end::Total Profit-->
						</div>
						<div class="col-md-12 col-lg-6 col-xl-3">
							<!--begin::Total Profit-->
							<div class="m-widget24" style="padding-bottom: 30px;">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										Serangan Malware
									</h4>
									<br>
									<br>
									<span class="m-widget24__stats m--font-danger">
										<div id="total-malware-attack-loader">
											<div class='m-loader m-loader--brand' style="width: 30px; display: inline-block;"></div>
										</div>
										<span id="total-malware-attack-value"></span>
									</span>
									<div class="m--space-10"></div>
								</div>
							</div>
							<!--end::Total Profit-->
						</div>
						<div class="col-md-12 col-lg-6 col-xl-3">
							<!--begin::Total Profit-->
							<div class="m-widget24" style="padding-bottom: 30px;">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										Malware Tidak Terdeteksi
									</h4>
									<br>
									<br>
									<span class="m-widget24__stats m--font-success">
										<div id="count-malware-not-detected-loader">
											<div class='m-loader m-loader--brand' style="width: 30px; display: inline-block;"></div>
										</div>
										<span id="count-malware-not-detected-value"></span>
									</span>
									<div class="m--space-10"></div>
								</div>
							</div>
							<!--end::Total Profit-->
						</div>
					</div>
				</div>
			</div>
			<!--end:: Widgets/Stats-->
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="m-portlet">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										Daftar Sensor
									</h3>
								</div>
							</div>
						</div>
						<div class="m-portlet__body">
							<!--begin::Section-->
							<div class="m-section">
								<div class="m-section__content">
									<div id="loading-table">
										<div class='m-loader m-loader--brand'></div>
									</div>
									<table class="table hidden" id="sensor-table">
										<thead>
											<tr>
												<th>
													Sensor IP
												</th>
												<th>
													Nama Sensor
												</th>
												<th>
													Serangan Diterima
												</th>
												<th>
													Aktif
												</th>
												<th>
													Edit
												</th>
											</tr>
										</thead>
										<tbody style="overflow: scroll; height: 500px;">
										</tbody>
									</table>
								</div>
							</div>
							<!--end::Section-->
						</div>
						<!--end::Section-->
					</div>
					<!--end::Portlet-->
				</div>
			</div>
		</div>

		<!--begin::Modal-->
		<div class="modal fade" id="modal-ip-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">
							Detail Port
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">
								&times;
							</span>
						</button>
					</div>
					<div class="modal-body">
						<ul class="nav nav-tabs  m-tabs-line m-tabs-line--success" role="tablist">
							<li class="nav-item m-tabs__item">
								<a class="nav-link m-tabs__link active" id="sensor-data-tab-button" data-toggle="tab" href="#sensor-data-tabs" role="tab">
									<i class="la la-cloud-upload"></i>
									Sumber Serangan
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane" id="sensor-data-tabs" role="tabpanel">
								<div id="sensor-data-loading" style="margin-top:20px">
									<div class='m-loader m-loader--brand'></div>
								</div>
								<div id="sensor-data">
									<table class="table m-table m-table--head-bg-brand" id="sensor-data-table">
										<thead>
											<tr>
												<th>Alamat IP</th>
												<th>Negara</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">
							Close
						</button>
					</div>
				</div>
			</div>
		</div>
		<!--end::Modal-->
@endsection

@section('style')
	<!--begin::Page Vendors -->
	<!-- <link href="/assets/vendors/custom/jqvmap/jqvmap.bundle.css" rel="stylesheet" type="text/css" /> -->
	<!-- <link href="/css/scrolltable.css" rel="stylesheet" type="text/css" /> -->
	<!--end::Page Vendors -->

	<style>
	

	.tooltiptext {
	    visibility: hidden;
	    width: 120px;
	    background-color: black;
	    color: #fff;
	    text-align: center;
	    border-radius: 6px;
	    padding: 5px 0;


	    /* Position the tooltip */
	    position: absolute;
	    z-index: 1;
	    
    
	}

	.m-badge:hover .tooltiptext {
	    visibility: visible;
	}
	</style>




@endsection
@section('head-script')
@endsection

@section('script')
	<script>	
	var baseUrl="{{ env('APP_API_URL') }}";
	var limit=10;
	var offset=0;
	var limitDetailPort=10;
	var offsetDetailPort=0;
	var limitDetailMalware=10;
	var offsetDetailMalware=0;
	function sensorDetail(sensor){
		$('#modal-ip-detail').modal('show'); 
		// tabs
		$('#sensor-data-tab-button').removeClass("active");
		
		$('#sensor-data-tabs').removeClass("active");

        $('#sensor-data-loading').removeClass('hidden');
		$("#sensor-data-table").addClass("hidden");
       
    	console.log("load sensor Detail "+sensor);
		

		

		axios.get(baseUrl + "/api/sensor/get-ip-sensor/" + sensor).then(response => {
			console.log('Port List Data:');
			offsetDetailPort+=10;
	        
	        if(jQuery.isEmptyObject(response.data.data)){
				$('#sensor-data-table tbody').append("<tr><td colspan='2' style='text-align:center;'>Tidak ada data.</td></tr>");
	        } else {
	        	$('#sensor-data-table tbody').html('');
	        	console.log("masuk if");
		        $.each(response.data.data, function(key, value){



					selectCode = "<tr>";
					selectCode+= "<td>"+value.countryIP+"</td>";
					selectCode+= "<td>"+value.countryName+"</td>";
					selectCode+="</tr>";		
					$('#sensor-data-table tbody').append(selectCode);
		        });
	        }
	        $('#sensor-data-loading').addClass('hidden');
            $("#sensor-data-table").removeClass("hidden");
			console.log('Detail sensor List Mounted');
		});

		
	}


	jQuery(document).ready(function() {    
	        // enable clear button 
	        $('.datepicker').datepicker({
	            todayBtn: "linked",
	            clearBtn: true,
	            todayHighlight: true,
	            format : "dd-mm-yyyy",
	            templates: {
	                leftArrow: '<i class="la la-angle-left"></i>',
	                rightArrow: '<i class="la la-angle-right"></i>'
	            }
	        });
	        console.log('DatePicker Created');
    		$('#countrySelect').select2();
	        console.log('Select2 Created');

		// 	axios.get(baseUrl + "/api/country/country-list").then(response => {
		// 		$.each(response.data.data,function(key,value){
		// 			selectCode = "<option value='"+value.countryCode+"''>"+value.countryName+"</option>";		
		// 			$('#countrySelect').append(selectCode);
		// 		});
		// 		$('#countrySelect').prop('disabled',false);
		// 		console.log('Country Mounted');
		// 	});

		axios.get(baseUrl + "/api/sensor/sensor-list").then(response => {
			offset+=10;
			console.log('Sensor List Data:');
			console.log(response.data);
			var now = new Date();
			$.each(response.data.data,function(key,value){
				var lastSubmit = new Date(value.lastSubmited);
                var lastSubmitDate = checkTime(lastSubmit.getDate()) + "-" + checkTime(lastSubmit.getMonth()+1) + "-" + lastSubmit.getFullYear() + " " + checkTime(lastSubmit.getHours()) + ":" + checkTime(lastSubmit.getMinutes()) + ":" + checkTime(lastSubmit.getSeconds());
				selectCode = "<tr>";
				selectCode+= "<td onClick=sensorDetail('"+value.sensorID+"') style='cursor:pointer;'  >"+value.sensorIP+"</td>";
				selectCode+= "<td>"+value.sensorName+"</td>";
				selectCode+= "<td>"+numberWithCommas(value.count)+"</td>";
                if ((now - lastSubmit) >= 7200000){
					selectCode+= "<td> <span class='m-badge m-badge--danger ' > <span class='tooltiptext'>"+lastSubmitDate+"</span></span> </td>";
                } else {
					selectCode+= "<td> <span class='m-badge m-badge--success ' ><span class='tooltiptext'>"+lastSubmitDate+"</span></span> </td>";
                }
				selectCode+= "<td> <a href='{{ route('backend.sensor.edit') }}' class='btn btn-primary'><i class='la la-edit'></i></a></td>";
				selectCode+="</tr>";		
				$('#sensor-table tbody').append(selectCode);
			});
 			$('#loading-table').addClass('hidden');
 			$('#sensor-table').removeClass('hidden');
			console.log('Sensor Mounted');
		});
			
			axios.get(baseUrl + "/api/widget/total-attack").then(response => {
				$('#total-attack-value').text(numberWithCommas(response.data.data.count));
				$('#total-attack-value').removeClass('hidden');
				$('#total-attack-loader').addClass('hidden');
				console.log('Widget totalAttack Mounted');
			});

			axios.get(baseUrl + "/api/malware/total-malware-attack").then(response => {
				$('#total-malware-attack-value').text(numberWithCommas(response.data.data.count));
				$('#total-malware-attack-value').removeClass('hidden');
				$('#total-malware-attack-loader').addClass('hidden');
				console.log('Widget totalMalwareAttack Mounted');
			});

			axios.get(baseUrl + "/api/widget/total-unique-attack").then(response => {
				$('#unique-attack-value').text(numberWithCommas(response.data.data.count));
				$('#unique-attack-value').removeClass('hidden');
				$('#unique-attack-loader').addClass('hidden');
				console.log('Widget Unique Attack Mounted');
			});

			axios.get(baseUrl + "/api/malware/count-malware-not-detected").then(response => {
				$('#count-malware-not-detected-value').text(numberWithCommas(response.data.data.count));
				$('#count-malware-not-detected-value').removeClass('hidden');
				$('#count-malware-not-detected-loader').addClass('hidden');
				console.log('Widget Count Malware Not Detected Mounted');
			});
		});
	</script>

@endsection