@extends('backend.layouts.default')
@section('page-title','Tambah User')


@section('content')
	<!--end:: Widgets/Stats-->
	<div class="row">
		<div class="col-md-12">
			<!--begin::Portlet-->
			<div class="m-portlet m-portlet--tab">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Form Tambah User
							</h3>
						</div>
					</div>
				</div>
				<!--begin::Form-->
				<form class="m-form m-form--fit m-form--label-align-right" action="{{route('api.backend.addUser')}}" method="POST">					
					<div class="m-portlet__body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group m-form__group">
									<label for="user-name">
										Nama :
									</label>
									<input type="text" class="form-control m-input" id="user-name" name="user-name" placeholder="Masukkan Nama User">
								</div>
								<div class="form-group m-form__group">
									<label for="user-phone">
										No Telp :
									</label>
									<input type="text" class="form-control m-input" id="user-phone" name="user-phone" placeholder="Masukkan Nomor Telepon">
								</div>
								<div class="form-group m-form__group">
									<label for="user-email">
										Email :
									</label>
									<input type="text" class="form-control m-input" id="user-email" name="user-email" placeholder="Masukkan Email User">
								</div>
								<div class="form-group m-form__group">
									<label for="user-institution-select">
										Institusi :
									</label>
									
									<select class="form-control select2" id="user-institution-select" name="user-institution-select">
										<option value="0">Pilih Institusi</option>
										@foreach($institute as $institusi)
										<option value="{{ $institusi->instituteId }}">{{$institusi->instituteName}}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group m-form__group">
									<label for="user-role">
										Peran :
									</label>
									
									<select class="form-control select2" id="user-role" name="user-role">
										<option value="0">Pilih Peran</option>
										@foreach($role as $role)
										<option value="{{ $role->id }}">{{$role->roleName}}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group m-form__group">
									<label for="user-password">
										Password :
									</label>
									<input type="text" class="form-control m-input" id="user-password" name="user-password" placeholder="Masukkan Password">
								</div>
								<div class="form-group m-form__group">
									<label for="user-repassword">
										Konfirmasi Password :
									</label>
									<input type="text" class="form-control m-input" id="user-repassword" name="user-repassword" placeholder="Masukkan Konfirmasi Password">
									<input type="hidden" class="form-control" name="sessionKey" value="{{$sessionKey}}">
								</div>
							</div>
							<div class="col-md-6">
							</div>
						</div>
					</div>
					<div class="m-portlet__foot m-portlet__foot--fit">
						<div class="m-form__actions">
							<button type="submit" class="btn btn-primary pull-right">
								Tambah User
							</button>
						</div>
					</div>
				</form>
				<!--end::Form-->


				




			</div>
			<!--end::Portlet-->
		</div>
	</div>
@endsection

@section('style')
@endsection
@section('head-script')
@endsection

@section('script')

	<script>


	jQuery(document).ready(function() {    
	    console.log('Select2 Created');
	});

	</script>

@endsection