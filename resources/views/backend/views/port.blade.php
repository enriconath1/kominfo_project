@extends('backend.layouts.default')
@section('page-title','Daftar Port')


@section('content')
	<div class="row">
		<div class="col-lg-12">
			<!--begin:: Widgets/Stats-->
			<div class="m-portlet">
				<div class="m-portlet__body  m-portlet__body--no-padding">
					<div class="row m-row--no-padding m-row--col-separator-xl">
						<div class="col-md-12 col-lg-6 col-xl-3">
							<!--begin::Total Profit-->
							<div class="m-widget24" style="padding-bottom: 30px;">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										Jumlah Serangan
									</h4>
									<br>
									<br>
									<span class="m-widget24__stats m--font-info">
										<div id="total-attack-loader">
											<div class='m-loader m-loader--brand' style="width: 30px; display: inline-block;"></div>
										</div>
										<span id="total-attack-value"></span>
									</span>
									<div class="m--space-10"></div>
								</div>
							</div>
							<!--end::Total Profit-->
						</div>
						<div class="col-md-12 col-lg-6 col-xl-3">
							<!--begin::Total Profit-->
							<div class="m-widget24" style="padding-bottom: 30px;">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										IP Yang Terdeteksi
									</h4>
									<br>
									<br>
									<span class="m-widget24__stats m--font-brand">
										<div id="unique-attack-loader">
											<div class='m-loader m-loader--brand' style="width: 30px; display: inline-block;"></div>
										</div>
										<span id="unique-attack-value"></span>
									</span>
									<div class="m--space-10"></div>
								</div>
							</div>
							<!--end::Total Profit-->
						</div>
						<div class="col-md-12 col-lg-6 col-xl-3">
							<!--begin::Total Profit-->
							<div class="m-widget24" style="padding-bottom: 30px;">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										Serangan Malware
									</h4>
									<br>
									<br>
									<span class="m-widget24__stats m--font-danger">
										<div id="total-malware-attack-loader">
											<div class='m-loader m-loader--brand' style="width: 30px; display: inline-block;"></div>
										</div>
										<span id="total-malware-attack-value"></span>
									</span>
									<div class="m--space-10"></div>
								</div>
							</div>
							<!--end::Total Profit-->
						</div>
						<div class="col-md-12 col-lg-6 col-xl-3">
							<!--begin::Total Profit-->
							<div class="m-widget24" style="padding-bottom: 30px;">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										Jumlah Negara
									</h4>
									<br>
									<br>
									<span class="m-widget24__stats m--font-success">
										<div id="total-country-loader">
											<div class='m-loader m-loader--brand' style="width: 30px; display: inline-block;"></div>
										</div>
										<span id="total-country-value"></span>
									</span>
									<div class="m--space-10"></div>
								</div>
							</div>
							<!--end::Total Profit-->
						</div>
					</div>
				</div>
			</div>
			<!--end:: Widgets/Stats-->
			<div class="row">
				<div class="col-md-4">
					<!--begin::Portlet-->
					<div class="m-portlet m-portlet--tab">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<span class="m-portlet__head-icon m--hide">
										<i class="la la-gear"></i>
									</span>
									<h3 class="m-portlet__head-text">
										Pencarian
									</h3>
								</div>
							</div>
						</div>
						<!--begin::Form-->
						<form class="m-form m-form--fit m-form--label-align-right" method="GET" onsubmit="countrySearch()">
							<div class="m-portlet__body">
								<div class="form-group m-form__group">
									<label for="search_by_ip">
										Cari Port :
									</label>
									<input type="text" class="form-control m-input" id="filter-port" placeholder="Masukkan Port">
								</div>
								<div class="form-group m-form__group">
									<label for="country">
										Negara :
									</label>
								    <select id="countrySelect" class="form-control" >
								      <option value="all">Semua Negara</option>
								    </select>
								</div>
								<div id="filter-date">
									<div class="form-group m-form__group">
										<label for="search_by_ip">
											Dari Tanggal
										</label>
										<div class='input-group date datepicker' style="width: 100%; padding:0px">
											<input type='text' class="form-control m-input" id="dateFrom"/>
											<span class="input-group-addon">
												<i class="la la-calendar"></i>
											</span>
										</div>
									</div>
									<div class="form-group m-form__group">
										<label for="search_by_ip">
											Sampai Tanggal
										</label>
										<div class='input-group date datepicker' style="width: 100%; padding:0px">
											<input type='text' class="form-control m-input" id="dateTo"/>
											<span class="input-group-addon">
												<i class="la la-calendar"></i>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="m-portlet__foot m-portlet__foot--fit">
								<div class="m-form__actions">
									<button type="submit" class="btn btn-primary pull-right">
										Apply
									</button>
								</div>
							</div>
						</form>
						<!--end::Form-->
					</div>
					<!--end::Portlet-->
				</div>
				<div class="col-md-8">
					<!--begin::Portlet-->
					<div class="m-portlet">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										Daftar Port
									</h3>
								</div>
							</div>
						</div>
						<div class="m-portlet__body">
							<!--begin::Section-->
							<div class="m-section">
								<div class="m-section__content">
									<div id="loading-table">
										<div class='m-loader m-loader--brand'></div>
									</div>
									<table class="table hidden" id="port-table">
										<thead>
											<tr>
												<th>
													Port Sasaran
												</th>
												<th>
													Jumlah Serangan
												</th>
											</tr>
										</thead>
										<tbody style="overflow: scroll; height: 500px;">
											<tr>
												<td colspan="2">
													<button class="btn btn-primary" style="width:100%;" id="load-more">Load More</button>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!--end::Section-->
						</div>
						<!--end::Section-->
					</div>
					<!--end::Portlet-->
				</div>
			</div>
		</div>

		<!--begin::Modal-->
		<div class="modal fade" id="modal-ip-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">
							Detail Port
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">
								&times;
							</span>
						</button>
					</div>
					<div class="modal-body">
						<ul class="nav nav-tabs  m-tabs-line m-tabs-line--success" role="tablist">
							<li class="nav-item m-tabs__item">
								<a class="nav-link m-tabs__link active" id="source-attack-tab-button" data-toggle="tab" href="#source-attack-tabs" role="tab">
									<i class="la la-cloud-upload"></i>
									Sumber Serangan
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane" id="source-attack-tabs" role="tabpanel">
								<div id="source-attack-loading" style="margin-top:20px">
									<div class='m-loader m-loader--brand'></div>
								</div>
								<div id="source-attack">
									<table class="table m-table m-table--head-bg-brand" id="source-attack-table">
										<thead>
											<tr>
												<th>Alamat IP</th>
												<th>Negara</th>
												<th>Jumlah Serangan</th>
											</tr>
										</thead>
										<tbody>

										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">
							Close
						</button>
					</div>
				</div>
			</div>
		</div>
		<!--end::Modal-->
@endsection

@section('style')
	<!--begin::Page Vendors -->
	<!-- <link href="/assets/vendors/custom/jqvmap/jqvmap.bundle.css" rel="stylesheet" type="text/css" /> -->
	<!-- <link href="/css/scrolltable.css" rel="stylesheet" type="text/css" /> -->
	<!--end::Page Vendors -->
@endsection
@section('head-script')
@endsection

@section('script')

	<!--begin::Page Vendors -->
	<!-- <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script> -->

	<!-- <script src="/assets/vendors/custom/jqvmap/jqvmap.bundle.js" type="text/javascript"></script> -->
	<!-- <script src="/js/dashboard.js" type="text/javascript"></script> -->
	<!--end::Page Vendors -->
	<script>

	</script>
	
	<!--begin::Page Resources -->
	<!-- <script src="/assets/demo/default/custom/components/maps/jqvmap.js" type="text/javascript"></script> -->
	<!--end::Page Resources -->
	<!--begin::Page Resources -->
	<!-- <script src="/assets/demo/default/custom/components/forms/widgets/select2.js" type="text/javascript"></script> -->
	<!--end::Page Resources -->

	<script>	
	var baseUrl="{{ env('APP_API_URL') }}";

	var limit=10;
	var offset=0;
	var limitDetailPort=10;
	var offsetDetailPort=0;
	var limitDetailMalware=10;
	var offsetDetailMalware=0;
	var country = null;

	function portDetail(port){
		console.log("port kebuka "+port);
		$('#modal-ip-detail').modal('show'); 
		// tabs
		$('#source-attack-tab-button').addClass("active");

		
		$('#source-attack-tabs').addClass("active");
		


        
        $('#source-attack-loading').removeClass('hidden');
		$("#source-attack-table").addClass("hidden");
		

		

		axios.get(baseUrl + "/api/port/port-list-data/"+port+ "/"  +offset+"/"+limit).then(response => {
			console.log('Port List Data:');
			offsetDetailPort+=10;
	        console.log(response.data.data);

	        if(jQuery.isEmptyObject(response.data.data)){
	        	console.log("port dalem"+port);

				$('#source-attack-table tbody').append("<tr><td colspan='2' style='text-align:center;'>Tidak ada data.</td></tr>");
	        } else {
	        	console.log("port isi"+port);
	        	$('#source-attack-table tbody').html('');
		        $.each(response.data.data, function(key, value){
		        	console.log("ulang");
					selectCode = "<tr>";
					selectCode+= "<td>"+value.ipsource+"</td>";
					selectCode+= "<td>"+value.countryName+"</td>";
					selectCode+= "<td>"+numberWithCommas(value.count)+"</td>";


					// selectCode+= "<td>cek</td>";
					// selectCode+= "<td>cek</td>";
					// selectCode+= "<td>cek</td>";

					selectCode+="</tr>";		
					$('#source-attack-table tbody').append(selectCode);
		        });
	        }
	        $('#source-attack-loading').addClass('hidden');
            $("#source-attack-table").removeClass("hidden");
			console.log('Detail IP Port List Mounted');
		});
		console.log("port masuk"+port);
	}


	function countrySearch(){


			var countryName = document.getElementById("countrySelect");
			var country = countryName.value;
			localStorage.setItem("port_country", country);

			
			console.log(country);

			

	}


	jQuery(document).ready(function() {    
	        // enable clear button 
	        $('.datepicker').datepicker({
	            todayBtn: "linked",
	            clearBtn: true,
	            todayHighlight: true,
	            format : "dd-mm-yyyy",
	            templates: {
	                leftArrow: '<i class="la la-angle-left"></i>',
	                rightArrow: '<i class="la la-angle-right"></i>'
	            }
	        });
	        console.log('DatePicker Created');
    		$('#countrySelect').select2();
	        console.log('Select2 Created');

		// 	axios.get(baseUrl + "/api/country/country-list").then(response => {
		// 		$.each(response.data.data,function(key,value){
		// 			selectCode = "<option value='"+value.countryCode+"''>"+value.countryName+"</option>";		
		// 			$('#countrySelect').append(selectCode);
		// 		});
		// 		$('#countrySelect').prop('disabled',false);
		// 		console.log('Country Mounted');
		// 	});
		var country = localStorage.getItem("port_country");
		console.log("cek negara: " + country);


		if(country == null){
			axios.get(baseUrl + "/api/port/port-list/ID/Date/Date/"+offset+"/"+limit).then(response => {
				offset+=10;
				console.log('Port List Data:');
				console.log(response.data);
				$.each(response.data.data,function(key,value){
					selectCode = "<tr onClick=portDetail('"+value.portDestination+"') style='cursor:pointer;' >";
					selectCode+= "<td>"+value.service+"</td>";
					selectCode+= "<td>"+numberWithCommas(value.countPort)+"</td>";

					selectCode+="</tr>";		
					$('#port-table tbody tr:last').before(selectCode);
				});
	 			$('#loading-table').addClass('hidden');
	 			$('#port-table').removeClass('hidden');
				console.log('Port Mounted');
			});
		}
		else if(country != null){
			axios.get(baseUrl + "/api/port/port-list/"+country+"/Date/Date/"+offset+"/"+limit).then(response => {
					offset+=10;
					console.log('Port List Data:');
					console.log(response.data);
					$.each(response.data.data,function(key,value){
						selectCode = "<tr onClick=portDetail('"+value.portDestination+"') style='cursor:pointer;' >";
						selectCode+= "<td>"+value.service+"</td>";
						selectCode+= "<td>"+numberWithCommas(value.countPort)+"</td>";

						selectCode+="</tr>";		
						$('#port-table tbody tr:last').before(selectCode);
					});
		 			$('#loading-table').addClass('hidden');
		 			$('#port-table').removeClass('hidden');
					console.log('Port Mounted');
			});
		}
			
			axios.get(baseUrl + "/api/widget/total-attack").then(response => {
				$('#total-attack-value').text(numberWithCommas(response.data.data.count));
				$('#total-attack-value').removeClass('hidden');
				$('#total-attack-loader').addClass('hidden');
				console.log('Widget totalAttack Mounted');
			});

			axios.get(baseUrl + "/api/malware/total-malware-attack").then(response => {
				$('#total-malware-attack-value').text(numberWithCommas(response.data.data.count));
				$('#total-malware-attack-value').removeClass('hidden');
				$('#total-malware-attack-loader').addClass('hidden');
				console.log('Widget totalMalwareAttack Mounted');
			});

			axios.get(baseUrl + "/api/widget/total-unique-attack").then(response => {
				$('#unique-attack-value').text(numberWithCommas(response.data.data.count));
				$('#unique-attack-value').removeClass('hidden');
				$('#unique-attack-loader').addClass('hidden');
				console.log('Widget Unique Attack Mounted');
			});

			axios.get(baseUrl + "/api/widget/total-country").then(response => {
				$('#total-country-value').text(numberWithCommas(response.data.data.count));
				$('#total-country-value').removeClass('hidden');
				$('#total-country-loader').addClass('hidden');
				console.log('Widget totalCountry Mounted');
			});


			axios.get(baseUrl+"/api/country/country-list").then(response => {
				$.each(response.data.data,function(key,value){
					selectCode = "<option value='"+value.countryCode+"''>"+value.countryName+"</option>";		
					$('#countrySelect').append(selectCode);
				});
				$('#countrySelect').val('ID'); 
				$('#countrySelect').prop('disabled',false);
				console.log('Country Mounted');
			});
    		$('#countrySelect').select2();
	        console.log('Select2 Created');


	        if(country == null){
				$('#load-more').click(function(){
					$('#load-more').addClass('m-loader');
					$('#load-more').addClass('m-loader--light');
					$('#load-more').addClass('m-loader--left');
					axios.get(baseUrl + "/api/port/port-list/ID/Date/Date/"+offset+"/"+limit).then(response => {
						offset+=10;
						console.log('Port List Load More Data:');
						$.each(response.data.data,function(key,value){
							selectCode = "<tr onClick=portDetail('"+value.portDestination+"') style='cursor:pointer;' >";
							selectCode+= "<td>"+value.service+"</td>";
							selectCode+= "<td>"+numberWithCommas(value.countPort)+"</td>";
							selectCode+="</tr>";		
							$('#port-table tbody tr:last').before(selectCode);
						});
		    			$('#loading-table').addClass('hidden');
		    			$('#port-table').removeClass('hidden');
						$('#load-more').removeClass('m-loader');
						$('#load-more').removeClass('m-loader--light');
						$('#load-more').removeClass('m-loader--left');
						console.log('Port Load More');
					});
				});
			}
			else if(country != null){
				$('#load-more').click(function(){
					$('#load-more').addClass('m-loader');
					$('#load-more').addClass('m-loader--light');
					$('#load-more').addClass('m-loader--left');
					axios.get(baseUrl + "/api/port/port-list/"+country+"/Date/Date/"+offset+"/"+limit).then(response => {
						offset+=10;
						console.log('Port List Load More Data:');
						$.each(response.data.data,function(key,value){
							selectCode = "<tr onClick=portDetail('"+value.portDestination+"') style='cursor:pointer;' >";
							selectCode+= "<td>"+value.service+"</td>";
							selectCode+= "<td>"+numberWithCommas(value.countPort)+"</td>";
							selectCode+="</tr>";		
							$('#port-table tbody tr:last').before(selectCode);
						});
		    			$('#loading-table').addClass('hidden');
		    			$('#port-table').removeClass('hidden');
						$('#load-more').removeClass('m-loader');
						$('#load-more').removeClass('m-loader--light');
						$('#load-more').removeClass('m-loader--left');
						console.log('Port Load More');
					});
				});
			}



		});
	</script>

@endsection