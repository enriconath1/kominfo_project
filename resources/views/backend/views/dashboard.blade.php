@extends('backend.layouts.default')


@section('content')
	<div class="row" >
		<div class="col-lg-12">
			<!--begin:: Widgets/Stats-->
			<div class="m-portlet">
				<div class="m-portlet__body  m-portlet__body--no-padding">
					<div class="row m-row--no-padding m-row--col-separator-xl">
						<div class="col-md-12 col-lg-6 col-xl-3">
							<!--begin::Total Profit-->
							<div class="m-widget24" style="padding-bottom: 30px;">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										Jumlah Serangan
									</h4>
									<br>
									<br>
									<span class="m-widget24__stats m--font-info">
										<div id="total-attack-loader">
											<div class='m-loader m-loader--brand' style="width: 30px; display: inline-block;"></div>
										</div>
										<span id="total-attack-value"></span>
									</span>
									<div class="m--space-10"></div>
								</div>
							</div>
							<!--end::Total Profit-->
						</div>
						<div class="col-md-12 col-lg-6 col-xl-3">
							<!--begin::Total Profit-->
							<div class="m-widget24" style="padding-bottom: 30px;">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										IP Yang Terdeteksi
									</h4>
									<br>
									<br>
									<span class="m-widget24__stats m--font-brand">
										<div id="unique-attack-loader">
											<div class='m-loader m-loader--brand' style="width: 30px; display: inline-block;"></div>
										</div>
										<span id="unique-attack-value"></span>
									</span>
									<div class="m--space-10"></div>
								</div>
							</div>
							<!--end::Total Profit-->
						</div>
						<div class="col-md-12 col-lg-6 col-xl-3">
							<!--begin::Total Profit-->
							<div class="m-widget24" style="padding-bottom: 30px;">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										Serangan Malware
									</h4>
									<br>
									<br>
									<span class="m-widget24__stats m--font-danger">
										<div id="total-malware-attack-loader">
											<div class='m-loader m-loader--brand' style="width: 30px; display: inline-block;"></div>
										</div>
										<span id="total-malware-attack-value"></span>
									</span>
									<div class="m--space-10"></div>
								</div>
							</div>
							<!--end::Total Profit-->
						</div>
						<div class="col-md-12 col-lg-6 col-xl-3">
							<!--begin::Total Profit-->
							<div class="m-widget24" style="padding-bottom: 30px;">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">Jumlah Negara</h4>
									<br>
									<br>
									<span class="m-widget24__stats m--font-success">
										<div id="total-country-loader">
											<div class='m-loader m-loader--brand' style="width: 30px; display: inline-block;"></div>
										</div>
										<span id="total-country-value"></span>
									</span>
									<div class="m--space-10"></div>
								</div>
							</div>
							<!--end::Total Profit-->
						</div>
					</div>
				</div>
			</div>
			<!--end:: Widgets/Stats-->
		</div>

	<div class="row" id="sortable">


		<div class="col-lg-6" id="top-country-donut-widget">
			<div class="m-portlet m-portlet--tab">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								NEGARA TERPOPULER
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<span style="float: right">
							<i class="fa icons fa-times" onclick="hideWidgetCountryDonut()"></i>
						</span>	
					</div>
				</div>
				<div class="row  align-items-center" style="height: 250px">
					<div class="col">
						<div id="donutchart_country" class="m-widget14__chart" style="height: 200px">										
						</div>
					</div>
					<!-- <div class="col" id="top-five-country-data">
						<div class="m-widget14__legends " id = "top-five-country">
						</div>
					</div> -->
				</div>
			</div>
		</div>


		<div class="col-lg-6" id="top-port-donut-widget">
			<div class="m-portlet m-portlet--tab">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								PORT TERPOPULER
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<span style="float: right">
							<i class="fa icons fa-times" onclick="hideWidgetPortDonut()"></i>
						</span>	
					</div>
				</div>
				<div class="row  align-items-center" style="height: 250px">
					<div class="col" >
						<div id="donutchart_port" class="m-widget14__chart1" style="height: 200px"></div>
					</div>
					<!-- <div class="col" id="top-five-port-data">
						<div class="m-widget14__legends" id = "top-five-port">
										
						</div>
					</div> -->
				</div>
			</div>

		</div>


		<div class="col-lg-6" id="top-malware-donut-widget">
			<div class="m-portlet m-portlet--tab">
				<div class="m-portlet__head" >
					<div class="m-portlet__head-caption" style="width: 100%">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								MALWARE TERPOPULER
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<span style="float: right">
							<i class="fa icons fa-times" onclick="hideWidgetMalwareDonut()"></i>
						</span>	
					</div>
				</div>
				<div class="row  align-items-center" style="height: 250px">
					<div class="col" id="top-five-malware-data">
						<div id="donutchart_malware" class="m-widget14__chart1" style="height: 200px"></div>
					</div>
					<!-- <div class="col">
						<div class="m-widget14__legends" id = "top-five-malware">
										
						</div>
					</div> -->
				</div>
			</div>

		</div>



		<div class="col-lg-6" id="top-country-pie-widget">
			<div class="m-portlet m-portlet--tab">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption" style="width: 100%">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								NEGARA TERPOPULER
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<span style="float: right">
							<i class="fa icons fa-times" onclick="hideWidgetCountryPie()"></i>
						</span>	
					</div>
				</div>
				<div class="row  align-items-center" style="height: 250px">
					<div class="col">
						<div id="piechart_country" class="m-widget14__chart" style="height: 300px">										
						</div>
					</div>
					<!-- <div class="col" id="top-five-country-data">
						<div class="m-widget14__legends " id = "top-five-country">
						</div>
					</div> -->
				</div>
			</div>
		</div>

		<div class="col-lg-6" id="top-port-pie-widget">
			<div class="m-portlet m-portlet--tab">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption" style="width: 100%">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								PORT TERPOPULER
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<span style="float: right">
							<i class="fa icons fa-times" onclick="hideWidgetPortPie()"></i>
						</span>	
					</div>
				</div>
				<div class="row  align-items-center" style="height: 250px">
					<div class="col">
						<div id="piechart_port" class="m-widget14__chart" style="height: 300px">										
						</div>
					</div>
					<!-- <div class="col" id="top-five-country-data">
						<div class="m-widget14__legends " id = "top-five-country">
						</div>
					</div> -->
				</div>
			</div>
		</div>



		<div class="col-lg-6" id="top-malware-pie-widget">
			<div class="m-portlet m-portlet--tab">
				<div class="m-portlet__head" >
					<div class="m-portlet__head-caption" style="width: 100%">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								MALWARE TERPOPULER
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<span style="float: right">
							<i class="fa icons fa-times" onclick="hideWidgetMalwarePie()"></i>
						</span>	
					</div>
				</div>
				<div class="row  align-items-center" style="height: 250px">
					<div class="col" id="top-five-malware-data">
						<div id="piechart_malware" class="m-widget14__chart1" style="height: 300px"></div>
					</div>
					<!-- <div class="col">
						<div class="m-widget14__legends" id = "top-five-malware">
										
						</div>
					</div> -->
				</div>
			</div>

		</div>


		<div class="col-lg-6" id="live-tick-widget">
			<!--begin::Portlet-->
			<div class="m-portlet m-portlet--tab">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption" style="width: 100%">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Live Tick
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<span style="float: right">
							<i class="fa icons fa-times" onclick="hideWidgetLive()"></i>
						</span>	
					</div>
				</div>
				<div class="m-portlet__body">
					<table class="table">
						<thead>
							<tr>
								<th>Time</th>
								<th>Country</th>
								<th>Port</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th scope="row">05:36:10</th>
								<td>Kepulauan Turks dan Caicos</td>
								<td>14726</td>
							</tr>
							<tr>
								<th scope="row">11:10:17</th>
								<td>Botswana</td>
								<td>8623</td>
							</tr>
							<tr>
								<th scope="row">08:59:11</th>
								<td>Mali</td>
								<td>10870</td>
							</tr>
							<tr>
								<th scope="row">23:40:21</th>
								<td>Djibouti</td>
								<td>4506</td>
							</tr>
							<tr>
								<th scope="row">10:18:00</th>
								<td>Eritrea</td>
								<td>10525</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<!--end::Portlet-->
		</div>
	

		<div class="col-lg-6" id="world-map-widget">
			<!--begin::Portlet-->
			<div class="m-portlet m-portlet--tab">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption" style="width: 100%">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Peta Serangan Dunia
							</h3>					
						</div>
						
						
					</div>
					<div class="m-portlet__head-tools">
						<span style="float: right">
							<i class="fa icons fa-times" onclick="hideWidgetWorld()"></i>
						</span>	
					</div>
					
				</div>
				<div class="m-portlet__body">
					<div id="m_jqvmap_world" class="m-jqvmap" style="height:300px;"></div>
				</div>
			</div>
			<!--end::Portlet-->
		</div>


		<div class="col-lg-6" id="indonesia-map-widget">
			<div class="m-portlet m-portlet--tab">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption" style="width: 100%">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Peta Serangan Indonesia
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<span style="float: right">
							<i class="fa icons fa-times" onclick="hideWidgetIndonesia()"></i>
						</span>	
					</div>
				</div>
				<div class="m-portlet__body">
					<div id="indo_vmap" class="m-jqvmap" style="height:300px;"></div>
				</div>
			</div>
		</div>
	</div>



	<div class="modal fade" id="modal-custom-widget" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">
						Modal Widget
					</h5>
				</div>
				<div class="modal-body">
					<div class="checkbox">
					    <label class="checkbox-inline">
					    	<input type="checkbox" id="inlineCheckbox1" name="checkbox[]" value="top-five-donut-country"> Top Five Country Donut Chart
					    </label><br>
					    <label class="checkbox-inline">
					    	<input type="checkbox" id="inlineCheckbox2" name="checkbox[]" value="top-five-donut-port"> Top Five Port Donut Chart
					    </label><br>
					    <label class="checkbox-inline">
					    	<input type="checkbox" id="inlineCheckbox3" name="checkbox[]" value="top-five-donut-malware"> Top Five Malware Donut Chart
					    </label><br>

					    <label class="checkbox-inline">
					    	<input type="checkbox" id="inlineCheckbox4" name="checkbox[]" value="top-five-pie-country"> Top Five Country Pie Chart
					    </label><br>
					    <label class="checkbox-inline">
					    	<input type="checkbox" id="inlineCheckbox5" name="checkbox[]" value="top-five-pie-port"> Top Five Port Pie Chart
					    </label><br>
					    <label class="checkbox-inline">
					    	<input type="checkbox" id="inlineCheckbox6" name="checkbox[]" value="top-five-pie-malware"> Top Five Malware Pie Chart
					    </label><br>

					    <label class="checkbox-inline">
					    	<input type="checkbox" id="inlineCheckbox7" name="checkbox[]" value="live-tick"> Live Tick
					    </label><br>
					    <label class="checkbox-inline">
					    	<input type="checkbox" id="inlineCheckbox8" name="checkbox[]" value="world-map"> World Map
					    </label><br>
					    <label class="checkbox-inline">
					    	<input type="checkbox" id="inlineCheckbox9" name="checkbox[]" value="indonesia-map"> Indonesia Map
					    </label><br>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" onclick="acceptWidget()" class="btn btn-secondary" data-dismiss="modal">
						Submit
					</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">
						Close
					</button>
				</div>
			</div>
		</div>
	</div>



	<button id="editWidget" class="btn btn-primary col-md-12 widgetButton" style="margin-bottom: 5px" onclick="customWidget()">Edit Widget</button>

    <button id="showAllWidget" class="btn btn-primary col-md-12 widgetButton" onclick="manageWidget()" >Show All Widget</button>

        <div id="new-chart"></div>
        <div id="new-chart-button" style="display: none">
            <div class="col-md-6" style="margin-left: 25%; margin-right:25%;">
                <div class="box" style="box-shadow:0 1px 15px rgba(0,0,0,0);">
                    <center>
                        <i class="fa fa-plus-circle" style="font-size:180px; color:#9CF;" id="addChart" data-toggle="modal" data-target="#addchart"></i>
                    </center>
                    <!--</div>-->
                </div>
            </div>
        </div>
	</div>
@endsection

@section('style')
	<!--begin::Page Vendors -->
	<link href="/backend-assets/vendors/custom/jqvmap/jqvmap.bundle.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="/resources/demos/style.css">
	<!--end::Page Vendors -->
@endsection
@section('head-script')
@endsection

@section('script')

	<!--begin::Page Vendors -->
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

	<!-- <script src="/backend-assets/vendors/custom/jqvmap/jqvmap.bundle.js" type="text/javascript"></script> -->
	<!-- <script src="/js/dashboard.js" type="text/javascript"></script> -->
	<!-- <script src="/js/dashboard.js" type="text/javascript"></script> -->
	<!--end::Page Vendors -->

	
	<!--begin::Page Resources -->

	<!-- <script type="text/javascript">

	</script> -->
	<script>
		var baseurl="{{ env('APP_API_URL') }}";
		
		function hasClass(element, cls) {
			    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
		}

		function customWidget(){
			$('#modal-custom-widget').modal('show'); 

			
			
			var checkBox1 = document.getElementById("inlineCheckbox1");
			var checkBox2 = document.getElementById("inlineCheckbox2");
			var checkBox3 = document.getElementById("inlineCheckbox3");
			var checkBox4 = document.getElementById("inlineCheckbox4");
			var checkBox5 = document.getElementById("inlineCheckbox5");
			var checkBox6 = document.getElementById("inlineCheckbox6");
			var checkBox7 = document.getElementById("inlineCheckbox7");
			var checkBox8 = document.getElementById("inlineCheckbox8");
			var checkBox9 = document.getElementById("inlineCheckbox9");



		    var widget1 = document.getElementById("top-country-donut-widget");
		    var widget2 = document.getElementById("top-port-donut-widget");
		    var widget3 = document.getElementById("top-malware-donut-widget");
		    var widget4 = document.getElementById("top-country-pie-widget");
		    var widget5 = document.getElementById("top-port-pie-widget");
		    var widget6 = document.getElementById("top-malware-pie-widget");
		    var widget7 = document.getElementById("live-tick-widget");
		    var widget8 = document.getElementById("world-map-widget");
		    var widget9 = document.getElementById("indonesia-map-widget");

		    console.log("cek custom widget " + hasClass(widget1, 'hidden') );


		    if ( hasClass(widget1, 'hidden') == false ){
		    	checkBox1.checked = true ;
		    } else {
		    	checkBox1.checked = false;
		    }

		    if ( hasClass(widget2, 'hidden') == false ){
		    	checkBox2.checked = true ;
		    } else {
		    	checkBox2.checked = false;
		    }

		    if ( hasClass(widget3, 'hidden') == false ){
		    	checkBox3.checked = true ;
		    } else {
		    	checkBox3.checked = false;
		    }

		    if ( hasClass(widget4, 'hidden') == false ){
		    	checkBox4.checked = true ;
		    } else {
		    	checkBox4.checked = false;
		    }

		    if ( hasClass(widget5, 'hidden') == false ){
		    	checkBox5.checked = true ;
		    } else {
		    	checkBox5.checked = false;
		    }

		    if ( hasClass(widget6, 'hidden') == false ){
		    	checkBox6.checked = true ;
		    } else {
		    	checkBox6.checked = false;
		    }
		    if ( hasClass(widget7, 'hidden') == false ){
		    	checkBox7.checked = true ;
		    } else {
		    	checkBox7.checked = false;
		    }
		    if ( hasClass(widget8, 'hidden') == false ){
		    	checkBox8.checked = true ;
		    } else {
		    	checkBox8.checked = false;
		    }
		    if ( hasClass(widget9, 'hidden') == false ){
		    	checkBox9.checked = true ;
		    } else {
		    	checkBox9.checked = false;
		    }


		}


		function acceptWidget(){


			var checkBox1 = document.getElementById("inlineCheckbox1");
			var checkBox2 = document.getElementById("inlineCheckbox2");
			var checkBox3 = document.getElementById("inlineCheckbox3");
			var checkBox4 = document.getElementById("inlineCheckbox4");
			var checkBox5 = document.getElementById("inlineCheckbox5");
			var checkBox6 = document.getElementById("inlineCheckbox6");
			var checkBox7 = document.getElementById("inlineCheckbox7");
			var checkBox8 = document.getElementById("inlineCheckbox8");
			var checkBox9 = document.getElementById("inlineCheckbox9");


		    var widget1 = document.getElementById("top-country-donut-widget");
		    var widget2 = document.getElementById("top-port-donut-widget");
		    var widget3 = document.getElementById("top-malware-donut-widget");
		    var widget4 = document.getElementById("top-country-pie-widget");
		    var widget5 = document.getElementById("top-port-pie-widget");
		    var widget6 = document.getElementById("top-malware-pie-widget");
		    var widget7 = document.getElementById("live-tick-widget");
		    var widget8 = document.getElementById("world-map-widget");
		    var widget9 = document.getElementById("indonesia-map-widget");


		    if(checkBox1.checked == true){
		    	$('#top-country-donut-widget').removeClass('hidden');
		    } else{
		    	$('#top-country-donut-widget').addClass('hidden');
		    }

		    if(checkBox2.checked == true){
		    	$('#top-port-donut-widget').removeClass('hidden');
		    } else{
		    	$('#top-port-donut-widget').addClass('hidden');
		    }

		    if(checkBox3.checked == true){
		    	$('#top-malware-donut-widget').removeClass('hidden');
		    } else{
		    	$('#top-malware-donut-widget').addClass('hidden');
		    }

		    if(checkBox4.checked == true){
		    	$('#top-country-pie-widget').removeClass('hidden');
		    } else{
		    	$('#top-country-pie-widget').addClass('hidden');
		    }

		    if(checkBox5.checked == true){
		    	$('#top-port-pie-widget').removeClass('hidden');
		    } else{
		    	$('#top-port-pie-widget').addClass('hidden');
		    }

		    if(checkBox6.checked == true){
		    	$('#top-malware-pie-widget').removeClass('hidden');
		    } else{
		    	$('#top-malware-pie-widget').addClass('hidden');
		    }

		    if(checkBox7.checked == true){
		    	$('#live-tick-widget').removeClass('hidden');
		    } else{
		    	$('#live-tick-widget').addClass('hidden');
		    }

		    if(checkBox8.checked == true){
		    	$('#world-map-widget').removeClass('hidden');
		    } else{
		    	$('#world-map-widget').addClass('hidden');
		    }

		    if(checkBox9.checked == true){
		    	$('#indonesia-map-widget').removeClass('hidden');
		    } else{
		    	$('#indonesia-map-widget').addClass('hidden');
		    }


		    		}

		function manageWidget(){
		// $('#top-malware-widget').addClass('hidden');
		// $('#top-port-widget').addClass('hidden');
		// $('#top-country-widget').addClass('hidden');
		// $('#live-tick-widget').addClass('hidden');
		// $('#world-map-widget').addClass('hidden');
		// $('#indonesia-map-widget').addClass('hidden');

		$('#top-country-donut-widget').removeClass('hidden');
		$('#top-port-donut-widget').removeClass('hidden');
		$('#top-malware-donut-widget').removeClass('hidden');
		$('#top-country-pie-widget').removeClass('hidden');
		$('#top-port-pie-widget').removeClass('hidden');
		$('#top-malware-pie-widget').removeClass('hidden');


		$('#live-tick-widget').removeClass('hidden');
		$('#world-map-widget').removeClass('hidden');
		$('#indonesia-map-widget').removeClass('hidden');
		}

		function hideWidgetCountryDonut(){
			$('#top-country-donut-widget').addClass('hidden');
		}
		function hideWidgetPortDonut(){
			$('#top-port-donut-widget').addClass('hidden');
		}
		function hideWidgetMalwareDonut(){
			$('#top-malware-donut-widget').addClass('hidden');
		}
		function hideWidgetCountryPie(){
			$('#top-country-pie-widget').addClass('hidden');
		}
		function hideWidgetPortPie(){
			$('#top-port-pie-widget').addClass('hidden');
		}
		function hideWidgetMalwarePie(){
			$('#top-malware-pie-widget').addClass('hidden');
		}
		function hideWidgetLive(){
			$('#live-tick-widget').addClass('hidden');
		}
		function hideWidgetWorld(){
			$('#world-map-widget').addClass('hidden');
		}
		function hideWidgetIndonesia(){
			$('#indonesia-map-widget').addClass('hidden');
		}
		
		

		axios.get(baseurl + "/api/widget/total-attack").then(response => {
			$('#total-attack-value').text(numberWithCommas(response.data.data.count));
			$('#total-attack-value').removeClass('hidden');
			$('#total-attack-loader').addClass('hidden');
			//console.log("cekcekekcek = " + response.data.data.count);
			console.log('Widget totalAttack Mounted');
		});

		axios.get(baseurl + "/api/malware/total-malware-attack").then(response => {
			$('#total-malware-attack-value').text(numberWithCommas(response.data.data.count));
			$('#total-malware-attack-value').removeClass('hidden');
			$('#total-malware-attack-loader').addClass('hidden');
			console.log('Widget totalMalwareAttack Mounted');
		});

		axios.get(baseurl + "/api/widget/total-unique-attack").then(response => {
			$('#unique-attack-value').text(numberWithCommas(response.data.data.count));
			$('#unique-attack-value').removeClass('hidden');
			$('#unique-attack-loader').addClass('hidden');
			console.log('Widget Unique Attack Mounted');
		});

		axios.get(baseurl + "/api/widget/total-country").then(response => {
			$('#total-country-value').text(numberWithCommas(response.data.data.count));
			$('#total-country-value').removeClass('hidden');
			$('#total-country-loader').addClass('hidden');
			console.log('Widget totalCountry Mounted');
		});
		localStorage.clear();
		// axios.get("{{route('api.country.top-five.get')}}").then(response => {
		// 	this.topCountries = response.data
		// 	this.isLoading = true;
		// });
		// axios.get("/api/country/country-list").then(response => {
		// 	this.countries = response.data
		// 	this.isCountryLoading=false;
		// 	console.log('Country Mounted');
		// });

		axios.get(baseurl + "/api/country/top-five-country").then(response => {
			var country_array = new Array( );  
			var country_name = new Array();
		    var i = 0;
		    var color_array = new Array( );
		    color_array[0] = 'brand' ;
		    color_array[1] = 'accent';
		    color_array[2] = 'danger';
		    color_array[3] = 'success';
		    color_array[4] = 'warning';

			//console.log('Top Five Country:');
			//console.log(response.data);
			$.each(response.data.data,function(key,value){
				selectCode = "<div class='m-widget14__legend'>"
				selectCode += "<span class='m-widget14__legend-bullet m--bg-"+color_array[i]+" '>"+"	</span>"
				selectCode += "<span class='m-widget14__legend-text'>"+numberWithCommas(value.count)+" "+value.countryName+"</span>"
				selectCode += "</div>"		
				$('#top-five-country').append(selectCode);
				//console.log("negara " + value.countryName);
				country_name[i] = value.countryName;	
				country_array[i] = value.count;

		       // console.log("chart negara =" + country_array[0]);
		        i++;
		    });

		        Morris.Donut({

			        element: 'donutchart_country',
			        data: [
			            {label: country_name[0], value: country_array[0]},
			            {label: country_name[1], value: country_array[1]},
			            {label: country_name[2], value: country_array[2]},
			            {label: country_name[3], value: country_array[3]},
			            {label: country_name[4], value: country_array[4]}

			        ],
			        colors: [
			            mUtil.getColor('brand'),
			            mUtil.getColor('accent'),
			            mUtil.getColor('danger'),
			            mUtil.getColor('success'),
			            mUtil.getColor('warning')
			        ],
			    });


			


    		$('#top-five-country').removeClass('hidden');
			// $('#countrySelect').prop('disabled',false);
			//console.log('IP Mounted');
		});


		axios.get(baseurl +"/api/port/top-five-port").then(response => {
			var port_array = new Array( );  
	        var portname_array = new Array();
		    var i = 0;
		    var color_array = new Array( );
		    color_array[0] = 'brand' ;
		    color_array[1] = 'accent';
		    color_array[2] = 'danger';
		    color_array[3] = 'success';
		    color_array[4] = 'warning';
			
			//console.log('Top Five:');
			//console.log(response.data);
			$.each(response.data.data,function(key,value){
				selectCode = "<div class='m-widget14__legend'>"
				selectCode += "<span class='m-widget14__legend-bullet m--bg-"+color_array[i]+"'>"+"	</span>"
				selectCode += "<span class='m-widget14__legend-text'>"+numberWithCommas(value.countPort)+" "+value.service+"</span>"
				selectCode += "</div>"		
				$('#top-five-port').append(selectCode);
				//console.log('port terbaik = ' + value.service);


				port_array[i] = value.countPort;
		       	portname_array[i] = value.service;
	        	//console.log("ini nge load : " + port_array[0]);
	        	i++;
	        });

	        	Morris.Donut({

			        element: 'donutchart_port',
			        data: [
			            {label: portname_array[0], value: port_array[0]},
			            {label: portname_array[1], value: port_array[1]},
			            {label: portname_array[2], value: port_array[2]},
			            {label: portname_array[3], value: port_array[3]},
			            {label: portname_array[4], value: port_array[4]}
			                

			        ],
			        colors: [
			            mUtil.getColor('brand'),
			            mUtil.getColor('accent'),
			            mUtil.getColor('danger'),
			            mUtil.getColor('success'),
			            mUtil.getColor('warning')
			        ],
			    });

			
    		
    		$('#top-five-port').removeClass('hidden');
			// $('#countrySelect').prop('disabled',false);


			
		});

		axios.get(baseurl +"/api/malware/top-five-malware").then(response => {
			var port_array = new Array( );  
	        var portname_array = new Array();
		    var i = 0;
		    var color_array = new Array( );
		    color_array[0] = 'brand' ;
		    color_array[1] = 'accent';
		    color_array[2] = 'danger';
		    color_array[3] = 'success';
		    color_array[4] = 'warning';
			
			//console.log('Top Five:');
			//console.log(response.data);
			$.each(response.data.data,function(key,value){
				selectCode = "<div class='m-widget14__legend'>"
				selectCode += "<span class='m-widget14__legend-bullet m--bg-"+color_array[i]+"'>"+"	</span>"
				selectCode += "<span class='m-widget14__legend-text'>"+numberWithCommas(value.count)+" "+value.virustotalscan_result+"</span>"
				selectCode += "</div>"		
				$('#top-five-port').append(selectCode);
				//console.log('port terbaik = ' + value.service);


				port_array[i] = value.count;
		       	portname_array[i] = value.virustotalscan_result;
	        	//console.log("ini nge load : " + port_array[0]);
	        	i++;
	        });

	        	Morris.Donut({

			        element: 'donutchart_malware',
			        data: [
			            {label: portname_array[0], value: port_array[0]},
			            {label: portname_array[1], value: port_array[1]},
			            {label: portname_array[2], value: port_array[2]},
			            {label: portname_array[3], value: port_array[3]},
			            {label: portname_array[4], value: port_array[4]}
			                

			        ],
			        colors: [
			            mUtil.getColor('brand'),
			            mUtil.getColor('accent'),
			            mUtil.getColor('danger'),
			            mUtil.getColor('success'),
			            mUtil.getColor('warning')
			        ],
			    });

			
    		
    		$('#top-five-malware').removeClass('hidden');
			// $('#countrySelect').prop('disabled',false);


			
		});



	axios.get(baseurl + "/api/country/top-five-country").then(response => {

		var total_array = new Array();  
	    var countryname_array = new Array();
	    var color_array = new Array();
	    var i = 0;

		$.each(response.data.data,function(key,value){
		selectCode = "<div class='m-widget14__legend'>"
		selectCode += "<span class='m-widget14__legend-bullet m--bg-"+color_array[i]+" '>"+"	</span>"
		selectCode += "<span class='m-widget14__legend-text'>"+numberWithCommas(value.count)+" "+value.countryName+"</span>"
		selectCode += "</div>"		
		$('#top-five-malware').append(selectCode);
		//console.log("negara " + value.countryName);
		countryname_array[i] = value.countryName;	
		total_array[i] = value.count;

		// console.log("chart negara =" + country_array[0]);
		i++;


			google.charts.load('current', {'packages':['corechart']});
		    google.charts.setOnLoadCallback(drawChart);

		     function drawChart() {

		    	var data = google.visualization.arrayToDataTable([
		          ['Country', 'Attack'],
		          [countryname_array[0],total_array[0]],
		          [countryname_array[1],total_array[1]],
		          [countryname_array[2],total_array[2]],
		          [countryname_array[3],total_array[3]],
		          [countryname_array[4],total_array[4]]
		        ]);

		        var options = {
		          title: ''
		        };

		        var chart = new google.visualization.PieChart(document.getElementById('piechart_country'));

		        chart.draw(data);
		      }

			



		});


	});


	axios.get(baseurl + "/api/port/top-five-port").then(response => {

		var total_array = new Array();  
	    var portname_array = new Array();
	    var color_array = new Array();
	    var i = 0;

		$.each(response.data.data,function(key,value){
		selectCode = "<div class='m-widget14__legend'>"
		selectCode += "<span class='m-widget14__legend-bullet m--bg-"+color_array[i]+" '>"+"	</span>"
		selectCode += "<span class='m-widget14__legend-text'>"+numberWithCommas(value.countPort)+" "+value.service+"</span>"
		selectCode += "</div>"		
		$('#top-five-malware').append(selectCode);
		//console.log("negara " + value.countryName);
		portname_array[i] = value.service;	
		total_array[i] = value.countPort;

		// console.log("chart negara =" + country_array[0]);
		i++;


			google.charts.load('current', {'packages':['corechart']});
		    google.charts.setOnLoadCallback(drawChart);

		     function drawChart() {

		    	var data = google.visualization.arrayToDataTable([
		          ['Country', 'Attack'],
		          [portname_array[0],total_array[0]],
		          [portname_array[1],total_array[1]],
		          [portname_array[2],total_array[2]],
		          [portname_array[3],total_array[3]],
		          [portname_array[4],total_array[4]]
		        ]);

		        var options = {
		          title: ''
		        };

		        var chart = new google.visualization.PieChart(document.getElementById('piechart_port'));

		        chart.draw(data);
		      }

			



		});


	});



	axios.get(baseurl + "/api/malware/top-five-malware").then(response => {

		var total_array = new Array();  
	    var malwarename_array = new Array();
	    var color_array = new Array();
	    var i = 0;

		$.each(response.data.data,function(key,value){
		selectCode = "<div class='m-widget14__legend'>"
		selectCode += "<span class='m-widget14__legend-bullet m--bg-"+color_array[i]+" '>"+"	</span>"
		selectCode += "<span class='m-widget14__legend-text'>"+numberWithCommas(value.count)+" "+value.virustotalscan_result+"</span>"
		selectCode += "</div>"		
		$('#top-five-malware').append(selectCode);
		//console.log("negara " + value.countryName);
		malwarename_array[i] = value.virustotalscan_result;	
		total_array[i] = value.count;

		// console.log("chart negara =" + country_array[0]);
		i++;


			google.charts.load('current', {'packages':['corechart']});
		    google.charts.setOnLoadCallback(drawChart);

		     function drawChart() {

		    	var data = google.visualization.arrayToDataTable([
		          ['Malware', 'Attack'],
		          [malwarename_array[0],total_array[0]],
		          [malwarename_array[1],total_array[1]],
		          [malwarename_array[2],total_array[2]],
		          [malwarename_array[3],total_array[3]],
		          [malwarename_array[4],total_array[4]]
		        ]);

		        var options = {
		          title: ''
		        };

		        var chart = new google.visualization.PieChart(document.getElementById('piechart_malware'));

		        chart.draw(data);
		      }

			



		});


	});



	axios.get(baseurl + "/api/map/load-world-map").then(response => {

		var jQVMapDemo = function() {

		    var sample_data = {
		        "af": "16.63",
		        "al": "11.58",
		        "dz": "158.97",
		        "ao": "85.81",
		        "ag": "1.1",
		        "ar": "351.02",
		        "am": "8.83",
		        "au": "1219.72",
		        "at": "366.26",
		        "az": "52.17",
		        "bs": "7.54",
		        "bh": "21.73",
		        "bd": "105.4",
		        "bb": "3.96",
		        "by": "52.89",
		        "be": "461.33",
		        "bz": "1.43",
		        "bj": "6.49",
		        "bt": "1.4",
		        "bo": "19.18",
		        "ba": "16.2",
		        "bw": "12.5",
		        "br": "2023.53",
		        "bn": "11.96",
		        "bg": "44.84",
		        "bf": "8.67",
		        "bi": "1.47",
		        "kh": "11.36",
		        "cm": "21.88",
		        "ca": "1563.66",
		        "cv": "1.57",
		        "cf": "2.11",
		        "td": "7.59",
		        "cl": "199.18",
		        "cn": "5745.13",
		        "co": "283.11",
		        "km": "0.56",
		        "cd": "12.6",
		        "cg": "11.88",
		        "cr": "35.02",
		        "ci": "22.38",
		        "hr": "59.92",
		        "cy": "22.75",
		        "cz": "195.23",
		        "dk": "304.56",
		        "dj": "1.14",
		        "dm": "0.38",
		        "do": "50.87",
		        "ec": "61.49",
		        "eg": "216.83",
		        "sv": "21.8",
		        "gq": "14.55",
		        "er": "2.25",
		        "ee": "19.22",
		        "et": "30.94",
		        "fj": "3.15",
		        "fi": "231.98",
		        "fr": "2555.44",
		        "ga": "12.56",
		        "gm": "1.04",
		        "ge": "11.23",
		        "de": "3305.9",
		        "gh": "18.06",
		        "gr": "305.01",
		        "gd": "0.65",
		        "gt": "40.77",
		        "gn": "4.34",
		        "gw": "0.83",
		        "gy": "2.2",
		        "ht": "6.5",
		        "hn": "15.34",
		        "hk": "226.49",
		        "hu": "132.28",
		        "is": "12.77",
		        "in": "1430.02",
		        "id": "695.06",
		        "ir": "337.9",
		        "iq": "84.14",
		        "ie": "204.14",
		        "il": "201.25",
		        "it": "2036.69",
		        "jm": "13.74",
		        "jp": "5390.9",
		        "jo": "27.13",
		        "kz": "129.76",
		        "ke": "32.42",
		        "ki": "0.15",
		        "kr": "986.26",
		        "undefined": "5.73",
		        "kw": "117.32",
		        "kg": "4.44",
		        "la": "6.34",
		        "lv": "23.39",
		        "lb": "39.15",
		        "ls": "1.8",
		        "lr": "0.98",
		        "ly": "77.91",
		        "lt": "35.73",
		        "lu": "52.43",
		        "mk": "9.58",
		        "mg": "8.33",
		        "mw": "5.04",
		        "my": "218.95",
		        "mv": "1.43",
		        "ml": "9.08",
		        "mt": "7.8",
		        "mr": "3.49",
		        "mu": "9.43",
		        "mx": "1004.04",
		        "md": "5.36",
		        "mn": "5.81",
		        "me": "3.88",
		        "ma": "91.7",
		        "mz": "10.21",
		        "mm": "35.65",
		        "na": "11.45",
		        "np": "15.11",
		        "nl": "770.31",
		        "nz": "138",
		        "ni": "6.38",
		        "ne": "5.6",
		        "ng": "206.66",
		        "no": "413.51",
		        "om": "53.78",
		        "pk": "174.79",
		        "pa": "27.2",
		        "pg": "8.81",
		        "py": "17.17",
		        "pe": "153.55",
		        "ph": "189.06",
		        "pl": "438.88",
		        "pt": "223.7",
		        "qa": "126.52",
		        "ro": "158.39",
		        "ru": "1476.91",
		        "rw": "5.69",
		        "ws": "0.55",
		        "st": "0.19",
		        "sa": "434.44",
		        "sn": "12.66",
		        "rs": "38.92",
		        "sc": "0.92",
		        "sl": "1.9",
		        "sg": "217.38",
		        "sk": "86.26",
		        "si": "46.44",
		        "sb": "0.67",
		        "za": "354.41",
		        "es": "1374.78",
		        "lk": "48.24",
		        "kn": "0.56",
		        "lc": "1",
		        "vc": "0.58",
		        "sd": "65.93",
		        "sr": "3.3",
		        "sz": "3.17",
		        "se": "444.59",
		        "ch": "522.44",
		        "sy": "59.63",
		        "tw": "426.98",
		        "tj": "5.58",
		        "tz": "22.43",
		        "th": "312.61",
		        "tl": "0.62",
		        "tg": "3.07",
		        "to": "0.3",
		        "tt": "21.2",
		        "tn": "43.86",
		        "tr": "729.05",
		        "tm": 0,
		        "ug": "17.12",
		        "ua": "136.56",
		        "ae": "239.65",
		        "gb": "2258.57",
		        "us": "14624.18",
		        "uy": "40.71",
		        "uz": "37.72",
		        "vu": "0.72",
		        "ve": "285.21",
		        "vn": "101.99",
		        "ye": "30.02",
		        "zm": "15.69",
		        "zw": "5.57"
		    };




		    //== Private functions
		    console.log("cek cek " + response.data.data.countryName);
		    var setupMap = function(name) {
		    	
		        var data = {
		            map: 'world_en',
		            backgroundColor: null,
		            color: '#ffffff',
		            hoverOpacity: 0.4,
		            selectedColor: '#666666',
		            enableZoom: true,
		            showTooltip: true,
		            values: sample_data,
		            scaleColors: ['#C8EEFF', '#006491'],
		            normalizeFunction: 'polynomial',

		            onLabelShow: function(event, label, code)
				    {
				    	$.each(response.data.data,function(key,value){
			                if (code.toUpperCase() == value.countryCode) {
			             		 //console.log( value.countryName  + "(" + value.count + ")");
			             		 label.text(value.countryName  + "(" + numberWithCommas(value.count) + ")");
			                }
			            });
				    },


		            onRegionOver: function(event, code) {

		             //    $.each(response.data.data,function(key,value){
			            //     if (code.toUpperCase() == value.countryCode) {
			             		 
			            //     }
			            // });
		            },
		            onRegionClick: function(element, code, region) {
		                //sample to interact with map
		                $.each(response.data.data,function(key,value){
		                	// console.log("masuk load map : " + value.countryCode);
		                	// console.log("coba liat code : " + code.toUpperCase());
			                if(code.toUpperCase() == value.countryCode){
			                	console.log(value.countryName);
				                var message = 'You clicked "' + region + '" which has the total attack: ' + numberWithCommas(value.count) ;
				                alert(message);
				            }
				        });
		            }
		        };

		        data.map = name + '_en';

		        var map = jQuery('#m_jqvmap_' + name);

		        map.width(map.parent().width());
		        map.vectorMap(data);
		    }

		    var setupMaps = function() {
		        setupMap("world");
		        setupMap("usa");
		        setupMap("europe");
		        setupMap("russia");
		        setupMap("germany");
		        
		    }

		    return {
		        // public functions
		        init: function() {
		            // default charts
		            setupMaps();

		            mUtil.addResizeHandler(function() {
		                setupMaps();
		            });
		        }
		    };
		}();

		jQuery(document).ready(function() {
		    jQVMapDemo.init();
		});
	});



	</script>


	<script>

		axios.get(baseurl + "/api/map/load-indonesia-map").then(response => {

			
		      jQuery(document).ready(function () {
		        jQuery('#indo_vmap').vectorMap({
		          map: 'indonesia_id',
		          backgroundColor: null,
		          color: '#aaaaaa',
		          hoverOpacity: 0.4,
		          selectedColor: '#666666',
		          enableZoom: true,
		          showTooltip: true,

		          onLabelShow: function(event, label, code)
				    {
				    	$.each(response.data.data,function(key,value){
			                if (code.toUpperCase() == value.province_iso_id) {
			             		 console.log( value.province_name  + "(" + value.count + ")");
			             		 label.text(value.province_name  + "(" + numberWithCommas(value.count) + ")");
			                }
			            });


			      	console.log(code);
				    },

		          onRegionClick: function(event, code, region){

		          	$.each(response.data.data,function(key,value){

		          		console.log("coba liat code : ");
		            	
		            });
		          }
		        });
		      });

		});

    </script>


	<script>
		  $( function() {
		    $( "#sortable" ).sortable({
		      revert: true
		    });
		    $( "#draggable" ).draggable({
		      connectToSortable: "#sortable",
		      helper: "clone",
		      revert: "invalid"
		    });
		    $( "ul, li" ).disableSelection();
		  } );
	</script>
	<!--end::Base Scripts -->
	<script src="/assets/vendors/custom/jqvmap/jqvmap.bundle.js" type="text/javascript"></script>
	<script src="/js/jquery.vmap.indonesia.js" type="text/javascript"></script>
	<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>


	
	<!-- <script src="/js/jquery-jvectormap-indonesia.js" type="text/javascript"></script> -->
	<!--end::Page Vendors -->
	<!--begin::Page Resources -->
	<!-- 	<script src="/assets/demo/default/custom/components/maps/jqvmap.js" type="text/javascript"></script> -->
		<!--end::Base Scripts -->
	<!--begin::Page Vendors -->
	<script src="//www.google.com/jsapi" type="text/javascript"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<!--end::Page Vendors -->
	<!--begin::Page Resources -->
	<script src="/backend-assets/demo/default/custom/components/charts/google-charts.js" type="text/javascript"></script>
	<!--end::Page Resources -->

@endsection