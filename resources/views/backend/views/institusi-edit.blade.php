@extends('backend.layouts.default')
@section('page-title','Edit Institusi')


@section('content')
	<!--end:: Widgets/Stats-->
	<div class="row">
		<div class="col-md-12">
			<!--begin::Portlet-->
			<div class="m-portlet m-portlet--tab">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Form Edit Institusi
							</h3>
						</div>
					</div>
				</div>
				<!--begin::Form-->
				<form class="m-form m-form--fit m-form--label-align-right" action="{{route('api.backend.editUser')}}" method="POST">					
					<div class="m-portlet__body">
						<div class="row">
							<div class="col-md-6">
								@foreach($institute as $institusi)
								<div class="form-group m-form__group">
									<label for="institute_name">
										 Nama Institusi :
									</label>
									<input type="text" class="form-control m-input" id="institute_name" name="institute_name" value="{{ $institusi->instituteName }}" >
								</div>
								<div class="form-group m-form__group">
									<label for="institute_address">
										Alamat Institusi :
									</label>
									<input type="text" class="form-control m-input" id="institute_address" name="institute_address" value="{{ $institusi->sensorIP }}">
								</div>
								@endforeach
								<div class="form-group m-form__group">
									<label for="user-institution-category">
										Kategori :
									</label>
									<input type="text" class="form-control m-input" id="institute-category" name="institute-category" value="{{ $institusi->sensorName }}">
								</div>
								<div class="form-group m-form__group">
									<label for="user-role">
										Provinsi :
									</label>
									
									<select class="form-control select2" id="user-role" name="user-role">
										<option value="0">Pilih Province</option>
										@foreach($province as $province)
										<option value="{{ $province->provinceMendagriId }}">{{ $province->provinceName }}</option>
										@endforeach
									</select>
								</div>
								
							</div>
							<div class="col-md-6">
							</div>
						</div>
					</div>
					<div class="m-portlet__foot m-portlet__foot--fit">
						<div class="m-form__actions">
							<button type="submit" class="btn btn-primary pull-right">
								Edit User
							</button>
						</div>
					</div>
				</form>
				<!--end::Form-->


				




			</div>
			<!--end::Portlet-->
		</div>
	</div>
@endsection

@section('style')
@endsection
@section('head-script')
@endsection

@section('script')

	<script>


	jQuery(document).ready(function() {    
	    console.log('Select2 Created');
	});

	</script>

@endsection