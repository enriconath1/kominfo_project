<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Daftar IP Penyerang</title>
    <style>
      table, th, td {
          border: 1px solid black;
          text-align:center;
      }
      table{
        width: 100%;
      }
    </style>
  </head>
  <body>
    <h1>Daftar IP Penyerang</h1>
    <table>
        <thead>
          <tr>
            <th>No.</th>
            <th>IP</th>
            <th>Jumlah Serangan</th>
          </tr>
        </thead>
        @foreach ($res as $key => $data)
        <tbody>
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $data->attackerIP }}</td>
                <td>{{ $data->count }}</td>        
                
            </tr>
        </tbody>
        @endforeach

        <!-- <tbody>
            <tr>
                <td>1</td>
                <td>test</td>
                <td>testing</td>        
                
            </tr>
        </tbody> -->

    </table>
  </body>
</html>
