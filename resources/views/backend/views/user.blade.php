@extends('backend.layouts.default')
@section('page-title','Daftar User')


@section('content')
	<div class="row">
		<div class="col-lg-12">
			<!--begin:: Widgets/Stats-->
			<div class="m-portlet">
				<div class="m-portlet__body  m-portlet__body--no-padding">
					<div class="row m-row--no-padding m-row--col-separator-xl">
						<div class="col-md-12 col-lg-6 col-xl-3">
							<!--begin::Total Profit-->
							<div class="m-widget24" style="padding-bottom: 30px;">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										Jumlah Serangan
									</h4>
									<br>
									<br>
									<span class="m-widget24__stats m--font-info">
										<div id="total-attack-loader">
											<div class='m-loader m-loader--brand' style="width: 30px; display: inline-block;"></div>
										</div>
										<span id="total-attack-value"></span>
									</span>
									<div class="m--space-10"></div>
								</div>
							</div>
							<!--end::Total Profit-->
						</div>
						<div class="col-md-12 col-lg-6 col-xl-3">
							<!--begin::Total Profit-->
							<div class="m-widget24" style="padding-bottom: 30px;">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										IP Yang Terdeteksi
									</h4>
									<br>
									<br>
									<span class="m-widget24__stats m--font-brand">
										<div id="unique-attack-loader">
											<div class='m-loader m-loader--brand' style="width: 30px; display: inline-block;"></div>
										</div>
										<span id="unique-attack-value"></span>
									</span>
									<div class="m--space-10"></div>
								</div>
							</div>
							<!--end::Total Profit-->
						</div>
						<div class="col-md-12 col-lg-6 col-xl-3">
							<!--begin::Total Profit-->
							<div class="m-widget24" style="padding-bottom: 30px;">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										Serangan Malware
									</h4>
									<br>
									<br>
									<span class="m-widget24__stats m--font-danger">
										<div id="total-malware-attack-loader">
											<div class='m-loader m-loader--brand' style="width: 30px; display: inline-block;"></div>
										</div>
										<span id="total-malware-attack-value"></span>
									</span>
									<div class="m--space-10"></div>
								</div>
							</div>
							<!--end::Total Profit-->
						</div>
						<div class="col-md-12 col-lg-6 col-xl-3">
							<!--begin::Total Profit-->
							<div class="m-widget24" style="padding-bottom: 30px;">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										Malware Tidak Terdeteksi
									</h4>
									<br>
									<br>
									<span class="m-widget24__stats m--font-success">
										<div id="count-malware-not-detected-loader">
											<div class='m-loader m-loader--brand' style="width: 30px; display: inline-block;"></div>
										</div>
										<span id="count-malware-not-detected-value"></span>
									</span>
									<div class="m--space-10"></div>
								</div>
							</div>
							<!--end::Total Profit-->
						</div>
					</div>
				</div>
			</div>
			<!--end:: Widgets/Stats-->
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="m-portlet">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										Daftar User
									</h3>
								</div>
							</div>
						</div>
						<div class="m-portlet__body">
							<!--begin::Section-->
							<div class="m-section">
								<div class="m-section__content">
									<div id="loading-table">
										<div class='m-loader m-loader--brand'></div>
									</div>
									<table class="table hidden" id="user-table">
										<thead>
											<tr>
												<th> Nama </th>
												<th> Institusi </th>
												<th> Email </th>
												<th> Peran </th>
												<th> Ubah </th>
											</tr>
										</thead>
										<tbody style="overflow: scroll; height: 500px;">
											<tr style="display:none;">
												<td colspan="5">
													<button class="btn btn-primary" style="width:100%;" id="load-more">Load More</button>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!--end::Section-->
						</div>
						<!--end::Section-->
					</div>
					<!--end::Portlet-->
				</div>
			</div>
		</div>

		<!--begin::Modal-->
		<div class="modal fade" id="modal-user-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">
							Detail User
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">
								&times;
							</span>
						</button>
					</div>
					<div class="modal-body">
						<ul class="nav nav-tabs  m-tabs-line m-tabs-line--success" role="tablist">
							<li class="nav-item m-tabs__item">
								<a class="nav-link m-tabs__link" id="user-bio-tab-button" data-toggle="tab" href="#user-bio-tabs" role="tab">
									<i class="la la-puzzle-piece"></i>
									Data User
								</a>
							</li>
						</ul>
						<div class="tab-content">
							
							<div class="tab-pane" id="user-bio-tabs" role="tabpanel">
								<div id="user-bio-loading" style="margin-top:20px">
									<div class='m-loader m-loader--brand'></div>
								</div>
								<div id="user-bio">
									<table class="table m-table m-table--head-bg-brand" id="user-bio-table">
										<thead>
											<tr>
												<th>Nama</th>
		                                        <th>No. Telp</th>
												<th>Email</th>
												<th>Institusi</th>
		                                        <th>Aktivitas Terakhir</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">
							Close
						</button>
					</div>
				</div>
			</div>
		</div>
		<!--end::Modal-->
@endsection

@section('style')
	<!--begin::Page Vendors -->
	<!-- <link href="/assets/vendors/custom/jqvmap/jqvmap.bundle.css" rel="stylesheet" type="text/css" /> -->
	<!-- <link href="/css/scrolltable.css" rel="stylesheet" type="text/css" /> -->
	<!--end::Page Vendors -->
@endsection
@section('head-script')
@endsection

@section('script')

	<!--begin::Page Vendors -->
	<!-- <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script> -->

	<!-- <script src="/assets/vendors/custom/jqvmap/jqvmap.bundle.js" type="text/javascript"></script> -->
	<!-- <script src="/js/dashboard.js" type="text/javascript"></script> -->
	<!--end::Page Vendors -->
	<script>

	</script>
	
	<!--begin::Page Resources -->
	<!-- <script src="/assets/demo/default/custom/components/maps/jqvmap.js" type="text/javascript"></script> -->
	<!--end::Page Resources -->
	<!--begin::Page Resources -->
	<!-- <script src="/assets/demo/default/custom/components/forms/widgets/select2.js" type="text/javascript"></script> -->
	<!--end::Page Resources -->

	<script>	
	var baseUrl="{{ env('APP_API_URL') }}";

	var limit=100;
	var offset=0;
	var limitDetailPort=10;
	var offsetDetailPort=0;
	var limitDetailMalware=10;
	var offsetDetailMalware=0;

	
	

	function userDetail(id){
		$('#modal-user-detail').modal('show'); 
		// tabs
		
		$('#user-bio-tab-button').addClass("active");

		
		$('#user-bio-tabs').addClass("active");
		// $('#who-is-tab-button').attr("aria-expanded","true");
		// $('#target-port-tab-button').attr("aria-expanded","false");
		// $('#malware-tab-button').attr("aria-expanded","false");

		//value.lastActivity
       
        $('#user-bio-loading').removeClass('hidden');
		$("#user-bio-table").addClass("hidden");
    	console.log("load IP Detail "+id);
		console.log("session :" + "{{$sessionKey}}" );
		

		axios.get(baseUrl + "/api/user/user-biodata/"+id+ "/{{$sessionKey}}").then(response => {
			console.log('Port List Data:');
			offsetDetailPort+=limitDetailPort;
	        console.log(response.data.data);
	        if(jQuery.isEmptyObject(response.data.data)){
				$('#user-bio-table tbody').append("<tr><td colspan='2' style='text-align:center;'>Tidak ada data.</td></tr>");
	        } else {
	        	$('#user-bio-table tbody').html('');
	        	console.log('dalem');
		        $.each(response.data.data, function(key, value){

		   		date= new Date(value.lastActivity);
		   		console.log(date);

					selectCode = "<tr>";
					selectCode+= "<td>"+value.name+"</td>";
					selectCode+= "<td>"+value.telp+"</td>";
					selectCode+= "<td>"+value.email+"</td>";
					selectCode+= "<td>"+value.institutionName+"</td>";
					selectCode+= "<td>"+date+"</td>";
					selectCode+="</tr>";		
					$('#user-bio-table tbody').append(selectCode);
		        });
	        }
	        $('#user-bio-loading').addClass('hidden');
            $("#user-bio-table").removeClass("hidden");
			console.log('Detail User List Mounted');
		});

	}


	jQuery(document).ready(function() {    
	        // enable clear button 
	        $('.datepicker').datepicker({
	            todayBtn: "linked",
	            clearBtn: true,
	            todayHighlight: true,
	            format : "dd-mm-yyyy",
	            templates: {
	                leftArrow: '<i class="la la-angle-left"></i>',
	                rightArrow: '<i class="la la-angle-right"></i>'
	            }
	        });
	        console.log('DatePicker Created');

			// axios.get(baseUrl + "/api/country/country-list").then(response => {
			// 	$.each(response.data.data,function(key,value){
			// 		selectCode = "<option value='"+value.countryCode+"''>"+value.countryName+"</option>";		
			// 		$('#countrySelect').append(selectCode);
			// 	});
			// 	$('#countrySelect').prop('disabled',false);
			// 	console.log('Country Mounted');
			// });
    		$('#countrySelect').select2();
	        console.log('Select2 Created');

			axios.get(baseUrl + "/api/user/user-list/"+offset+"/"+limit+"/{{$sessionKey}}").then(response => {
				offset+=limit;
				console.log('User List Data:');
				console.log(response.data);

				console.log("session :" + "{{$sessionKey}}" );
				$.each(response.data.data,function(key,value){
					selectCode = "<tr>";
					selectCode+= "<td onClick=userDetail('"+value.id+"') style='cursor:pointer;'>"+value.name+"</td>";
					selectCode+= "<td>"+value.institutionName+"</td>";
					selectCode+= "<td>"+value.email+"</td>";
					selectCode+= "<td>"+value.roleName+"</td>";
					selectCode+= "<td> <a href='/user/edit-user/"+value.id+"' class='btn btn-primary'><i class='la la-edit'></i></a></td>";
					selectCode+="</tr>";		
					$('#user-table tbody tr:last').before(selectCode);
				});
    			$('#loading-table').addClass('hidden');
    			$('#user-table').removeClass('hidden');
				// $('#countrySelect').prop('disabled',false);
				console.log('User Mounted');
			});
			
			axios.get(baseUrl + "/api/widget/total-attack").then(response => {
				$('#total-attack-value').text(numberWithCommas(response.data.data.count));
				$('#total-attack-value').removeClass('hidden');
				$('#total-attack-loader').addClass('hidden');
				console.log('Widget totalAttack Mounted');
			});

			axios.get(baseUrl + "/api/malware/total-malware-attack").then(response => {
				$('#total-malware-attack-value').text(numberWithCommas(response.data.data.count));
				$('#total-malware-attack-value').removeClass('hidden');
				$('#total-malware-attack-loader').addClass('hidden');
				console.log('Widget totalMalwareAttack Mounted');
			});

			axios.get(baseUrl + "/api/widget/total-unique-attack").then(response => {
				$('#unique-attack-value').text(numberWithCommas(response.data.data.count));
				$('#unique-attack-value').removeClass('hidden');
				$('#unique-attack-loader').addClass('hidden');
				console.log('Widget Unique Attack Mounted');
			});

			axios.get(baseUrl + "/api/widget/total-country").then(response => {
				$('#total-country-value').text(numberWithCommas(response.data.data.count));
				$('#total-country-value').removeClass('hidden');
				$('#total-country-loader').addClass('hidden');
				console.log('Widget totalCountry Mounted');
			});

			axios.get(baseUrl + "/api/malware/count-malware-not-detected").then(response => {
				$('#count-malware-not-detected-value').text(numberWithCommas(response.data.data.count));
				$('#count-malware-not-detected-value').removeClass('hidden');
				$('#count-malware-not-detected-loader').addClass('hidden');
				console.log('Widget Count Malware Not Detected Mounted');
			});

			
			$('#load-more').click(function(){
				$('#load-more').addClass('m-loader');
				$('#load-more').addClass('m-loader--light');
				$('#load-more').addClass('m-loader--left');
				axios.get(baseUrl + "/api/user/user-list/"+offset+"/"+limit+"/$sessionKey").then(response => {
					offset+=10;
					console.log('User List Load More Data:');
					$.each(response.data.data,function(key,value){
						selectCode = "<tr onClick=ipDetail('"+value.attackerUser+"') style='cursor:pointer;' >";
						selectCode+= "<td>"+value.attackerUser+"</td>";
						selectCode+= "<td>"+numberWithCommas(value.count)+"</td>";
						selectCode+="</tr>";		
						$('#user-table tbody tr:last').before(selectCode);
					});
	    			$('#loading-table').addClass('hidden');
	    			$('#user-table').removeClass('hidden');
					$('#load-more').removeClass('m-loader');
					$('#load-more').removeClass('m-loader--light');
					$('#load-more').removeClass('m-loader--left');
					// $('#countrySelect').prop('disabled',false);
					console.log('User Load More');
				});
			});
		});
	</script>

@endsection