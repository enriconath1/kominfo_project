@extends('backend.layouts.default')
@section('page-title','Ubah kata sandi')


@section('content')
	<!--end:: Widgets/Stats-->
	<div class="row">
		<div class="col-md-12">
			<!--begin::Portlet-->
			<div class="m-portlet m-portlet--tab">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Form Ganti Password
							</h3>
						</div>
					</div>
				</div>
				<!--begin::Form-->
				<form class="m-form m-form--fit m-form--label-align-right">
					<div class="m-portlet__body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group m-form__group">
									<label for="user-name">
										Password :
									</label>
									<input type="text" class="form-control m-input" id="user-name" placeholder="Masukkan Password">
								</div>
								<div class="form-group m-form__group">
									<label for="user-phone">
										Konfirmasi Password :
									</label>
									<input type="text" class="form-control m-input" id="user-phone" placeholder="Masukkan Konfirmasi Password">
								</div>
							</div>
						</div>
					</div>
					<div class="m-portlet__foot m-portlet__foot--fit">
						<div class="m-form__actions">
							<button type="reset" class="btn btn-primary">
								Ganti Password
							</button>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
			<!--end::Portlet-->
		</div>
	</div>
@endsection

@section('style')
@endsection
@section('head-script')
@endsection

@section('script')

	<script>


	jQuery(document).ready(function() {    
	    console.log('Select2 Created');
	});

	</script>

@endsection