@extends('frontend.layouts.old')

@section('content')
<div class="row">
    <div id="hmp" class="col-md-12">

        <div id="left-sidebar" class="col-md-2">
            <div class="row">
                <div class="col-md-12">
                    <div class="logo">
                        <img src="{{ asset('assets/old/img/kominfo-logo-honeynet.png') }}" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="title" id="title-table-peringkat-serangan">Peringkat Serangan</div>
                    <div id="top-infections" class="box-1">
                        <div id="top-attacker-load"></div>
                        <table id="top-attacker" class="table-responsive table">
                            <thead>
                            <th>#</th>
                            <th><i class="fa fa-flag"></i></th>
                            <th class="text">Jumlah Serangan</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="title" id="title-table-live-feed">Live Feed</div>
                    <div id="live-ticker">
                        <table id="live" class="table-responsive table" style="color: #FFF; font-size: 10pt;">
                            <thead>
                            <th>Waktu</th>
                            <th>Sumber</th>
                            <th>Port</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> 

        <div id="middle" class="col-md-8">
            <div class="main-title" id="map-title">Peta Serangan Siber Dunia</div>
            <div class="sub-main-title" id="sub-map-title">(<span id="target-source">Sumber Serangan</span> - <span id="tanggal-serangan">Akumulasi Data</span>)</div>
            <div id="switch-map">
                <div class="btn-group" role="group">
                    <button type="button" class="btn map-switch" id="world-map">Peta Dunia</button>
                    <button type="button" class="btn map-switch" id="indonesia-map">Peta Indonesia</button>
                </div>
            </div>
            <div id="map-container">
                <div id="world-map-container"></div>
                <!--<div id="map-highstock"></div>-->
            </div>
            <div id="chart-container">
                <div id="chart" style="height:130px; margin-top: 20px;"></div>
            </div>
        </div> 

        <div id="right-sidebar" class="col-md-2">
            <div class="row">
                <div class="col-md-12">
                    <div class="title text">Waktu Sekarang</div>
                    <div id="digital-date" class="text"></div>
                    <div id="clock" class="text"></div>
                </div>
            </div>
            <div class="row">
                <div class="sub-main-title" id="string-total-sensor">Dikumpulkan dari <span id="total-sensor"></span> sensor</div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="title text">Tren Malware</div>
                    <div id="carousel">
                        <div class="btn-bar">
                            <div id="buttons" style="display:none;"><a id="prev" href="#"><</a><a id="next" href="#">></a> </div>
                        </div>
                        <div id="slides" style="background-color:rgba(200,200,200,0.2);">
                            <ul>
                                <li class="slide" style="height:100px;  ">
                                    <div class="row" style="padding: 0px; margin:0px;">
                                        <div class="col-md-4" style="height:100px; padding: 0px; margin:0px;">
                                            <center><h1 style="color:white;  line-height: 100px; padding: 0px; margin: 0px; width:100%; text-align: center;">1</h1></center>
                                        </div>
                                        <div class="col-md-8" style="height:100px; padding: 10px 20px; margin:0px;">
                                            <div style="color:white; font-size: 8pt; margin-bottom:10px; word-wrap: break-word;">Worm/Win32.Conficker</div>
                                            <div style="color:white; font-size: 8pt;">Total Attack : 1.000</div>
                                        </div>
                                    </div>
                                </li>
                                <li class="slide" style="height:100px;  ">
                                    <div class="row" style="padding: 0px; margin:0px;">
                                        <div class="col-md-4" style="height:100px; padding: 0px; margin:0px;">
                                            <center><h1 style="color:white;  line-height: 100px; padding: 0px; margin: 0px; width:100%; text-align: center;">2</h1></center>
                                        </div>
                                        <div class="col-md-8" style="height:100px; padding: 10px 20px; margin:0px;">
                                            <div style="color:white; font-size: 8pt; margin-bottom:10px; word-wrap: break-word;">Worm/Win32.Conficker</div>
                                            <div style="color:white; font-size: 8pt;">Total Attack : 850</div>
                                        </div>
                                    </div>
                                </li>
                                <li class="slide" style="height:100px;  ">
                                    <div class="row" style="padding: 0px; margin:0px;">
                                        <div class="col-md-4" style="height:100px; padding: 0px; margin:0px;">
                                            <center><h1 style="color:white;  line-height: 100px; padding: 0px; margin: 0px; width:100%; text-align: center;">3</h1></center>
                                        </div>
                                        <div class="col-md-8" style="height:100px; padding: 10px 20px; margin:0px;">
                                            <div style="color:white; font-size: 8pt; margin-bottom:10px; word-wrap: break-word;">Worm/Win32.Conficker</div>
                                            <div style="color:white; font-size: 8pt;">Total Attack : 700</div>
                                        </div>
                                    </div>
                                </li>
                                <li class="slide" style="height:100px;  ">
                                    <div class="row" style="padding: 0px; margin:0px;">
                                        <div class="col-md-4" style="height:100px; padding: 0px; margin:0px;">
                                            <center><h1 style="color:white;  line-height: 100px; padding: 0px; margin: 0px; width:100%; text-align: center;">4</h1></center>
                                        </div>
                                        <div class="col-md-8" style=" height:100px; padding: 10px 20px; margin:0px;">
                                            <div style="color:white; font-size: 8pt; margin-bottom:10px; word-wrap: break-word;">Worm/Win32.Conficker</div>
                                            <div style="color:white; font-size: 8pt;">Total Attack : 500</div>
                                        </div>
                                    </div>
                                </li>
                                <li class="slide" style="height:100px;  ">
                                    <div class="row" style="padding: 0px; margin:0px;">
                                        <div class="col-md-4" style="height:100px; padding: 0px; margin:0px;">
                                            <center><h1 style="color:white;  line-height: 100px; padding: 0px; margin: 0px; width:100%; text-align: center;">5</h1></center>
                                        </div>
                                        <div class="col-md-8" style="height:100px; padding: 10px 20px; margin:0px;">
                                            <div style="color:white; font-size: 8pt; margin-bottom:10px; word-wrap: break-word;">Worm/Win32.Conficker</div>
                                            <div style="color:white; font-size: 8pt;">Total Attack : 300</div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="logo" style="padding-left:10px; margin-top:15px;">
                        <span class="support">Didukung oleh</span>
                        <a href="http://honeynet.or.id"><img style="height: 50px; width: auto" src="{{ asset('/img/hp2_logo.png') }}" /></a>
                    </div>
                </div>
            </div>

    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span id="modalTitle">Modal title</span> (<span id="modalTitleDate"></span>)</h4>
                <h4 class="modal-title modalSeranganMasuk">Jumlah Serangan: <span id="modalJumlahSerangan"></span> kali</h4>
                <h4 class="modal-title modalSeranganMasuk">Total Serangan Dunia: <span id="modalJumlahSeranganDunia"></span> kali</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6" style="margin-bottom: 20px;">
                        <div class="sub-title">IP Penyerang</div>
                        <div id="attacker-ip" class="chart-modal">
                        </div>
                    </div>
                    <div class="col-md-6" style="margin-bottom: 20px;">
                        <div class="sub-title">Port Sasaran</div>
                        <div id="attacked-port" class="chart-modal">

                        </div>
                    </div>
                    <div class="col-md-6" >
                        <div class="sub-title">Malware</div>
                        <div id="malware-by-country" class="chart-modal">

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="sub-title">Propinsi Sasaran</div>
                        <div id="top-target" class="chart-modal">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="indoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span id="modalTitleIndo">Modal title</span> (<span id="modalTitleDateIndo"></span>)</h4>
                <h4 class="modal-title modalSeranganMasuk" id="modalSeranganMasukIndo">Jumlah Serangan: <span id="modalJumlahSeranganIndo"></span> kali</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6" style="margin-bottom: 20px;">
                        <div class="sub-title">IP Penyerang</div>
                        <div id="attacker-ip-indo" class="chart-modal">
                        </div>
                    </div>
                    <div class="col-md-6" style="margin-bottom: 20px;">
                        <div class="sub-title">Port Sasaran</div>
                        <div id="attacked-port-indo" class="chart-modal">

                        </div>
                    </div>
                    <div class="col-md-6" >
                        <div class="sub-title">Malware</div>
                        <div id="malware-by-country-indo" class="chart-modal">

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="sub-title">Negara Penyerang</div>
                        <div id="top-country-attacker" class="chart-modal">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="malwareModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span id="modalTitleMalware">Malware Populer per Periode</span> (<span id="modalTitleDateMalware"></span>)</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="sub-title">Grafik Malware Terpopuler</div>
                        <div id="top-malware-chart" style="height: 400px">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="sub-title">Tabel Malware Terpopuler</div>
                        <table id="top-malware" class="table table-responsive" style="color: #FFF; font-size: 100%;">
                            <thead>
                            <th>#</th>
                            <th>Nama Malware</th>
                            <th>Jumlah Serangan</th>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('page-specific-scripts')
<style>
#carousel {
position: relative;
width:100%;
margin:0 auto;
}

#slides {
    overflow: hidden;
    position: relative;
    width: 100%;
    height: 100px;
}

#slides ul {
list-style: none;
width:100%;
height:100px;
margin: 0;
padding: 0;
position: relative;
}

 #slides li {
width:100%;
height:100px;
float:left;
text-align: center;
    display: block;
font-family:lato, sans-serif;
}
/* Styling for prev and next buttons */
.btn-bar{
    max-width: 500px;
    margin: 0 auto;
    display: block;
    position: relative;
    top: 40px;
    width: 100%;
}

 #buttons {
padding:0 0 5px 0;
float:right;
}

#buttons a {
text-align:center;
display:block;
font-size:50px;
float:left;
outline:0;
margin:0 60px;
color:#b14943;
text-decoration:none;
display:block;
padding:9px;
width:35px;
}

a#prev:hover, a#next:hover {
color:#FFF;
text-shadow:.5px 0px #b14943;  
}
</style>
<script>
    function startTime() {
        var today = new Date();
        var y = today.getFullYear();
        var mo = today.getMonth();
        var d = today.getDate();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        var monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
            "Juli", "Agustus", "September", "Oktober", "November", "Desember"
        ];
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('clock').innerHTML = h + ":" + m + ":" + s;
        document.getElementById('digital-date').innerHTML = d + " " + monthNames[mo] + " " + y;
        var t = setTimeout(function () {
            startTime()
        }, 500);
    }

    function checkTime(i) {
        if (i < 10) {
            i = "0" + i
        }
        ;  // add zero in front of numbers < 10
        return i;
    }
    $(document).ready(function () {
        //rotation speed and timer
        var speed = 3000;
        
        var run = setInterval(rotate, speed);
        var slides = $('.slide');
        var container = $('#slides ul');
        var elm = container.find(':first-child').prop("tagName");
        var item_width = container.height();
        var previous = 'prev'; //id of previous button
        var next = 'next'; //id of next button
        slides.height(item_width); //set the slides to the correct pixel width
        container.parent().height(item_width);
        container.height(slides.length * item_width); //set the slides container to the correct total width
        container.find(elm + ':first').before(container.find(elm + ':last'));
        resetSlides();
        
        
        //if user clicked on prev button
        
        $('#buttons a').click(function (e) {
            //slide the item
            
            if (container.is(':animated')) {
                return false;
            }
            if (e.target.id == previous) {
                container.stop().animate({
                    'bottom': 0
                }, 1500, function () {
                    container.find(elm + ':first').before(container.find(elm + ':last'));
                    resetSlides();
                });
            }
            
            if (e.target.id == next) {
                container.stop().animate({
                    'top': item_width * -2
                }, 1500, function () {
                    container.find(elm + ':last').after(container.find(elm + ':first'));
                    resetSlides();
                });
            }
            
            //cancel the link behavior            
            return false;
            
        });
        
        //if mouse hover, pause the auto rotation, otherwise rotate it    
        container.parent().mouseenter(function () {
            clearInterval(run);
        }).mouseleave(function () {
            run = setInterval(rotate, speed);
        });
        
        
        function resetSlides() {
            //and adjust the container so current is in the frame
            container.css({
                'top': -1 * item_width
            });
        }
        
    });
    //a simple function to click next link
    //a timer will call this function, and the rotation will begin

    function rotate(e) {
        $('#next').click();
    }
</script>
<script src="{{URL::asset('assets/old/js/app.js')}}"></script>
<script src="{{URL::asset('assets/old/js/countryList.js')}}"></script>
<!--<script src="{{URL::asset('js/app-highstock.js')}}"></script>-->
@stop
