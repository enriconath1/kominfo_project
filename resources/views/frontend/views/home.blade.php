@extends('frontend.layouts.default')


@section('content')
	<div id="honeynet-indonesia">
		<div id="left-column">
			<div id="logo">
				<img src="/img/logo-honey.png" style="width:80%;">
			</div>
			<div class="content-div">
				<div class="content-div__header">
					<h2>PERINGKAT SERANGAN</h2>
				</div>
				<div class="content-div__body">
					<table>
						<thead>
							<th width="20px" style="text-align: left;">#</th>
							<th width="50px" style="padding:0px 10px; text-align: center;">SUMBER</th>
							<th style="text-align: center;">JUMLAH SERANGAN</th>
						</thead>
						<tbody>
							@for($i=1; $i < 6; $i++)
							<tr>
								<td style="text-align: left;">{{$i}}</td>
								<td style="text-align: center;"><img src="/img/country/{{$faker->countryCode}}.png"></td>
								<td style="color:#A5A5A5;text-align: center;">{{number_format($faker->numberBetween(100000,1000000))}}</td>
							</tr>
							@endfor
						</tbody>
					</table>
				</div>
			</div>
			<div class="content-div">
				<div class="content-div__header">
					<h2>LIVE FEED</h2>
				</div>
				<div class="content-div__body">
					<table>
						<thead>
							<th width="20px" style="text-align: left;">#</th>
							<th width="50px" style="padding:0px 10px; text-align: center;">SUMBER</th>
							<th style="text-align: center;">PORT</th>
						</thead>
						<tbody>
							@for($i=1; $i < 6; $i++)
							<tr>
								<td style="text-align: left;">{{$faker->time($format = 'H:i:s', $max = 'now')}}</td>
								<td style="text-align: center;"><img src="/img/country/{{$faker->countryCode}}.png"></td>
								<td style="color:#A5A5A5;text-align: center;">{{$faker->numberBetween(80,10000)}}</td>
							</tr>
							@endfor
						</tbody>
					</table>
				</div>
			</div>
			<div>
				<h3>Didukung oleh:</h3>
				<div>
					<img src="/img/honeynet-logo.png" style="width:200px;">
				</div>
			</div>
		</div>
		<div id="middle-column">
			<div id="honeynet-indonesia--header">
				<h1 class="honeynet--indonesia--title">PETA PENYERANGAN SIBER INDONESIA</h1>
				<h4 class="honeynet--indonesia--subtitle">(SUMBER SERANGAN - 13 MARET 2017 S/D 12 SEPTEMBER 2017)</h4>
			</div>
			<div id="jqvmap--world" class="m-jqvmap"style="display: block; height: 300px;"></div>

			<div id="container" style="height: 400px; min-width: 310px"></div>
		</div>
		<div id="right-column">
			<div class="time">
				<div class="time--title">
					<h4>WAKTU SEKARANG</h4>
				</div>
				<div class="time--date">
					{{\Carbon\Carbon::now()->format('d M Y')}}
				</div>
				<div class="time--time">
					{{\Carbon\Carbon::now()->format('H:i:s')}}
				</div>
			</div>

			<div class="menu">
				<a href="" class="btn btn-honey-active">Sumber Serangan</a>
				<a href="" class="btn btn-honey">Target Serangan</a>
				<div class="menu--attacker-source">
					
				</div>
				<div class="menu--attacker-destination"></div>
			</div>

			<div id="jumlah-sensor">
				<h2>JUMLAH SENSOR : <strong style="color:#f0f0f0">21<strong></h2>
			</div>

			<div class="content-div">
				<div class="content-div__header">
					<h2>TREN MALWARE</h2>
				</div>
				<div class="content-div__body" style="height:300px;">
					@foreach($trendMalwares as $trendMalware)
					<div class="carousel-item">
					  <div class="carousel-caption d-none d-md-block">
					    <h3>{{$trendMalware}}</h3>
					    <p>{{$faker->md5}}</p>
					  </div>
					</div>
					@endforeach
				</div>
			</div>
			<div id="malware--trend">
				<div id="malware--trend__header"></div>
				<div id="malware--body">
					@foreach($trendMalwares as $trendMalware)
					<div class="carousel-item">
					  <div class="carousel-caption d-none d-md-block">
					    <h3>{{$trendMalware}}</h3>
					    <p>{{$faker->md5}}</p>
					  </div>
					</div>
					@endforeach
				</div>
			</div>
			<div id="qr">
				<!-- Trigger the Modal -->
				<img id="myImg" src="/img/qr-icon.png" alt="Snow" style="width:100%;max-width:225px">

				<!-- The Modal -->
				<div id="myModal" class="modal">

				  <!-- The Close Button -->
				  <span class="close">&times;</span>

				  <!-- Modal Content (The Image) -->
				  <img class="modal-content" id="img01">

				  <!-- Modal Caption (Image Text) -->
				  <div id="caption"></div>
				</div>
				<img src="">
			</div>
		</div>
	</div>
@endsection

@section('style')
	<!--begin::Page Vendors -->
	<link href="/css/honeynet-indonesia.css" rel="stylesheet" type="text/css" />
	<!--end::Page Vendors -->
	<style type="text/css">
		#qr{
			/* Style the Image Used to Trigger the Modal */
			#myImg {
			  border-radius: 5px;
			  cursor: pointer;
			  transition: 0.3s;
			}

			#myImg:hover {opacity: 0.7;}

			/* The Modal (background) */
			.modal {
			  display: none; /* Hidden by default */
			  position: fixed; /* Stay in place */
			  z-index: 1; /* Sit on top */
			  padding-top: 100px; /* Location of the box */
			  left: 0;
			  top: 0;
			  width: 100%; /* Full width */
			  height: 100%; /* Full height */
			  overflow: auto; /* Enable scroll if needed */
			  background-color: rgb(0,0,0); /* Fallback color */
			  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
			}

			/* Modal Content (Image) */
			.modal-content {
			  margin: auto;
			  display: block;
			  width: 80%;
			  max-width: 700px;
			}

			/* Caption of Modal Image (Image Text) - Same Width as the Image */
			#caption {
			  margin: auto;
			  display: block;
			  width: 80%;
			  max-width: 700px;
			  text-align: center;
			  color: #ccc;
			  padding: 10px 0;
			  height: 150px;
			}

			/* Add Animation - Zoom in the Modal */
			.modal-content, #caption {
			  animation-name: zoom;
			  animation-duration: 0.6s;
			}

			@keyframes zoom {
			  from {transform:scale(0)}
			  to {transform:scale(1)}
			}

			/* The Close Button */
			.close {
			  position: absolute;
			  top: 15px;
			  right: 35px;
			  color: #f1f1f1;
			  font-size: 40px;
			  font-weight: bold;
			  transition: 0.3s;
			}

			.close:hover,
			.close:focus {
			  color: #bbb;
			  text-decoration: none;
			  cursor: pointer;
			}

			/* 100% Image Width on Smaller Screens */
			@media only screen and (max-width: 700px){
			  .modal-content {
			    width: 100%;
			  }
			}
		}
	</style>
@endsection

@section('script')
	<!--begin::Page Vendors -->
	<script src="/assets/vendors/custom/jqvmap/jqvmap.bundle.js" type="text/javascript"></script>
	<!--end::Page Vendors -->
	<script src="https://code.highcharts.com/stock/highstock.js"></script>
	<script type="text/javascript" src="/plugins/smartwizard/js/jquery.smartWizard.min.js"></script>
<script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
	<script src="/js/honeynet-indonesia.js" type="text/javascript"></script>
	<script type="text/javascript">
		// Get the modal
		var modal = document.getElementById("myModal");

		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img = document.getElementById("myImg");
		var modalImg = document.getElementById("img01");
		var captionText = document.getElementById("caption");
		img.onclick = function(){
		  modal.style.display = "block";
		  modalImg.src = this.src;
		  captionText.innerHTML = this.alt;
		}

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("close")[0];

		// When the user clicks on <span> (x), close the modal
		span.onclick = function() {
		  modal.style.display = "none";
		}
	</script>

@endsection