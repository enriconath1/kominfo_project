<html>
    <head>
        @include('frontend.partials.head')
    </head>
    <body onload="startTime()">
        @yield('content')
        
        @yield('page-specific-scripts')
    </body>
    
</html>
