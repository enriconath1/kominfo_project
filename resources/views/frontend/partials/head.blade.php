<base target="_top" />
<title>HoneyNet Indonesia</title>
<link href="{{URL::asset('assets/old/css/bootstrap.css')}}" media="all" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('assets/old/css/map.css')}}" media="all" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{URL::asset('assets/old/css/font-awesome.min.css')}}" type="text/css" media="screen"/>
<link rel="stylesheet" href="{{URL::asset('assets/old/css/flag-icon.min.css')}}" type="text/css" media="screen"/>
<link rel="stylesheet" href="{{URL::asset('assets/old/css/jquery-jvectormap-2.0.1.css')}}" type="text/css" media="screen"/>
<link rel="stylesheet" href="{{URL::asset('assets/old/css/animate.css')}}" type="text/css" media="screen"/>

<script src="{{URL::asset('assets/old/js/jquery-2.1.3.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('assets/old/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('assets/old/js/jquery.nicescroll.min.js')}}" type="text/javascript"></script>

<!--<script src="{{URL::asset('coffee/honeymap.js')}}"></script>-->
<script src="{{URL::asset('assets/old/js/flot/jquery.flot.min.js')}}"></script>
<script src="{{URL::asset('assets/old/js/flot/jquery.flot.time.min.js')}}"></script>
<script src="{{URL::asset('assets/old/js/flot/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{URL::asset('assets/old/js/flot/jquery.flot.pie.min.js')}}"></script>
<script src="{{URL::asset('assets/old/js/jquery-jvectormap-2.0.1.min.js')}}"></script>
<script src="{{URL::asset('assets/old/js/jquery-jvectormap-world-mill-en.js')}}"></script>
<script src="{{URL::asset('assets/old/js/jquery-jvectormap-indonesia.js')}}"></script>
<script src="{{URL::asset('assets/old/js/jquery.qtip.js')}}"></script>

<script src="{{URL::asset('assets/old/js/modules/highstock.js')}}"></script>
<script src="{{URL::asset('assets/old/js/modules/map.js')}}"></script>
<script src="{{URL::asset('assets/old/js/modules/id-all.js')}}"></script>

<!--<script src="{{URL::asset('assets/old/js/charts.js')}}"></script>-->
<script src="{{URL::asset('assets/old/js/wordcloud2.js')}}"></script>
<script src="{{URL::asset('assets/old/js/detailedChart.js')}}"></script>
<script src="{{URL::asset('assets/old/js/liveticker.js')}}"></script>
