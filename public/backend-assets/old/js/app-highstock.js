$(document).ready(function () {
    $("#map-highstock").highcharts('Map', {
        title: {
            text: "Hello World"
        },
        series: [{
                mapData: Highcharts.maps('countries/id/id-all'),
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            },
        ]
    })
});

