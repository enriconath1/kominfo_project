$(function() {
    //loadWorldMap();
    $("#malware-list").niceScroll({cursorcolor: "#00F"});

    $(document).on('click', '#trigger', function() {
        $('#tray').animate({
            top: "0%",
        }, 2000);
    });



    $(document).on('click', '.map-switch', function() {
        var id = $(this).attr('id');
        if (id == 'world-map') {
            $('#map-container').html('<div id="world-map" style="height:520px"></div>');
            loadWorldMap();
        } else {
            $('#map-container').html('<div id="map" style="height:520px"></div>');
            loadIndonesiaMap();
//            $("#map").on("petaklik", function(event, idwilayah) {
//
//                $('#myModal').modal('show');
//                $('#modalTitle').html(idwilayah);
//            });
        }
    });

});

var indoMap;

function loadIndonesiaMap() {
    $("#map-title").html("Indonesia Map");
    var markerIndex = 0,
            markersCoords = {};

    indoMap = new jvm.Map({
        map: 'id_mill_en',
        backgroundColor: 'transparent',
        container: $('#map'),
        series: {
            regions: [{
                    attribute: 'fill'
                }]
        },
        onRegionOver: function(e, code) {
            $(this).on('click', function() {
                var map = $('#map').vectorMap('get', 'mapObject');
                var regionName = map.getRegionName(code); 
                $("#modalTitle").html(regionName);
                $("#myModal").modal('show');
            });
        },
        onRegionOut: function(e, code) {
            $(this).unbind('click');
        },
		zoomOnScroll: false,
		zoomButtons: false,
    });

    var pallete = ["#99B2FF", "#CCD8FF", "#0047B2"]

    generateColor = function() {
        var colors = {},
                key;

        for (key in indoMap.regions) {
            colors[key] = pallete[Math.floor(Math.random() * pallete.length)];
        }
        return colors;

    }

    indoMap.series.regions[0].setValues(generateColor())
}

var worldMap;

function loadWorldMap() {
    $("#map-title").html("World Map");
    var markerIndex = 0,
            markersCoords = {};

    worldMap = new jvm.Map({
        map: 'world_mill_en',
        backgroundColor: 'transparent',
        markerStyle: {
            initial: {
                fill: 'red'
            }
        },
        container: $('#world-map'),
        onMarkerTipShow: function(e, label, code) {
            worldMap.tip.text(markersCoords[code].lat.toFixed(2) + ', ' + markersCoords[code].lng.toFixed(2));
        },
        series: {
            regions: [{
                    attribute: 'fill'
                }]
        },
        onRegionOver: function(e, code) {
            $(this).on('click', function() {
                var map = $('#world-map').vectorMap('get', 'mapObject');
                var regionName = map.getRegionName(code);
                var regionCode = map.getSelectedRegions();
                $("#modalTitle").html(regionName+"-"+regionCode);
                $("#myModal").modal('show');
            });
        },
        onRegionOut: function(e, code) {
            $(this).unbind('click');
        },
		zoomOnScroll: false,
		zoomButtons: false,
    });

//    var jvmCountries = {"countries": [
//            {"countryCode": "AF", "name": "Afghanistan", "coords": [33, 65]},
//            {"countryCode": "AL", "name": "Albania", "coords": [41, 20]},
//            {"countryCode": "DZ", "name": "Algeria", "coords": [28, 3]},
//            {"countryCode": "AO", "name": "Angola", "coords": [-12.5, 18.5]},
//            {"countryCode": "AR", "name": "Argentina", "coords": [-34, -64]},
//            {"countryCode": "AM", "name": "Armenia", "coords": [40, 45]},
//            {"countryCode": "AU", "name": "Australia", "coords": [-27, 133]},
//            {"countryCode": "AT", "name": "Austria", "coords": [47.3333, 13.3333]},
//            {"countryCode": "AZ", "name": "Azerbaijan", "coords": [40.5, 47.5]},
//            {"countryCode": "BS", "name": "Bahamas", "coords": [24.25, -76]},
//            {"countryCode": "BD", "name": "Bangladesh", "coords": [24, 90]},
//            {"countryCode": "BY", "name": "Belarus", "coords": [53, 28]},
//            {"countryCode": "BE", "name": "Belgium", "coords": [50.8333, 4]},
//            {"countryCode": "BZ", "name": "Belize", "coords": [17.25, -88.75]},
//            {"countryCode": "BJ", "name": "Benin", "coords": [9.5, 2.25]},
//            {"countryCode": "BT", "name": "Bhutan", "coords": [27.5, 90.5]},
//            {"countryCode": "BO", "name": "Bolivia", "coords": [-17, -65]},
//            {"countryCode": "BA", "name": "Bosnia and Herz.", "coords": [44, 18]},
//            {"countryCode": "BW", "name": "Botswana", "coords": [-22, 24]},
//            {"countryCode": "BR", "name": "Brazil", "coords": [-10, -55]},
//            {"countryCode": "BN", "name": "Brunei", "coords": [4.5, 114.6667]},
//            {"countryCode": "BG", "name": "Bulgaria", "coords": [43, 25]},
//            {"countryCode": "BF", "name": "Burkina Faso", "coords": [13, -2]},
//            {"countryCode": "BI", "name": "Burundi", "coords": [-3.5, 30]},
//            {"countryCode": "KH", "name": "Cambodia", "coords": [13, 105]},
//            {"countryCode": "CM", "name": "Cameroon", "coords": [6, 12]},
//            {"countryCode": "CA", "name": "Canada", "coords": [60, -95]},
//            {"countryCode": "CF", "name": "Central African Rep.", "coords": [7, 21]},
//            {"countryCode": "TD", "name": "Chad", "coords": [15, 19]},
//            {"countryCode": "CL", "name": "Chile", "coords": [-30, -71]},
//            {"countryCode": "CN", "name": "China", "coords": [35, 105]},
//            {"countryCode": "CO", "name": "Colombia", "coords": [4, -72]},
//            {"countryCode": "CG", "name": "Congo", "coords": [-1, 15]},
//            {"countryCode": "CR", "name": "Costa Rica", "coords": [10, -84]},
//            {"countryCode": "HR", "name": "Croatia", "coords": [45.1667, 15.5]},
//            {"countryCode": "CU", "name": "Cuba", "coords": [21.5, -80]},
//            {"countryCode": "CY", "name": "Cyprus", "coords": [35, 33]},
//            {"countryCode": "CZ", "name": "Czech Rep.", "coords": [49.75, 15.5]},
//            {"countryCode": "CI", "name": "Côte d'Ivoire", "coords": [8, -5]},
//            {"countryCode": "CD", "name": "Dem. Rep. Congo", "coords": [0, 25]},
//            {"countryCode": "KP", "name": "Dem. Rep. Korea", "coords": [40, 127]},
//            {"countryCode": "DK", "name": "Denmark", "coords": [56, 10]},
//            {"countryCode": "DJ", "name": "Djibouti", "coords": [11.5, 43]},
//            {"countryCode": "DO", "name": "Dominican Rep.", "coords": [19, -70.6667]},
//            {"countryCode": "EC", "name": "Ecuador", "coords": [-2, -77.5]},
//            {"countryCode": "EG", "name": "Egypt", "coords": [27, 30]},
//            {"countryCode": "SV", "name": "El Salvador", "coords": [13.8333, -88.9167]},
//            {"countryCode": "GQ", "name": "Eq. Guinea", "coords": [2, 10]},
//            {"countryCode": "ER", "name": "Eritrea", "coords": [15, 39]},
//            {"countryCode": "EE", "name": "Estonia", "coords": [59, 26]},
//            {"countryCode": "ET", "name": "Ethiopia", "coords": [8, 38]},
//            {"countryCode": "FK", "name": "Falkland Is.", "coords": [-51.75, -59]},
//            {"countryCode": "FJ", "name": "Fiji", "coords": [-18, 175]},
//            {"countryCode": "FI", "name": "Finland", "coords": [64, 26]},
//            {"countryCode": "TF", "name": "Fr. S. Antarctic Lands", "coords": [-43, 67]},
//            {"countryCode": "FR", "name": "France", "coords": [46, 2]},
//            {"countryCode": "GA", "name": "Gabon", "coords": [-1, 11.75]},
//            {"countryCode": "GM", "name": "Gambia", "coords": [13.4667, -16.5667]},
//            {"countryCode": "GE", "name": "Georgia", "coords": [42, 43.5]},
//            {"countryCode": "DE", "name": "Germany", "coords": [51, 9]},
//            {"countryCode": "GH", "name": "Ghana", "coords": [8, -2]},
//            {"countryCode": "GR", "name": "Greece", "coords": [39, 22]},
//            {"countryCode": "GL", "name": "Greenland", "coords": [72, -40]},
//            {"countryCode": "GT", "name": "Guatemala", "coords": [15.5, -90.25]},
//            {"countryCode": "GN", "name": "Guinea", "coords": [11, -10]},
//            {"countryCode": "GW", "name": "Guinea-Bissau", "coords": [12, -15]},
//            {"countryCode": "GY", "name": "Guyana", "coords": [5, -59]},
//            {"countryCode": "HT", "name": "Haiti", "coords": [19, -72.4167]},
//            {"countryCode": "HN", "name": "Honduras", "coords": [15, -86.5]},
//            {"countryCode": "HU", "name": "Hungary", "coords": [47, 20]},
//            {"countryCode": "IS", "name": "Iceland", "coords": [65, -18]},
//            {"countryCode": "IN", "name": "India", "coords": [20, 77]},
//            {"countryCode": "ID", "name": "Indonesia", "coords": [-5, 120]},
//            {"countryCode": "IR", "name": "Iran", "coords": [32, 53]},
//            {"countryCode": "IQ", "name": "Iraq", "coords": [33, 44]},
//            {"countryCode": "IE", "name": "Ireland", "coords": [53, -8]},
//            {"countryCode": "IL", "name": "Israel", "coords": [31.5, 34.75]},
//            {"countryCode": "IT", "name": "Italy", "coords": [42.8333, 12.8333]},
//            {"countryCode": "JM", "name": "Jamaica", "coords": [18.25, -77.5]},
//            {"countryCode": "JP", "name": "Japan", "coords": [36, 138]},
//            {"countryCode": "JO", "name": "Jordan", "coords": [31, 36]},
//            {"countryCode": "KZ", "name": "Kazakhstan", "coords": [48, 68]},
//            {"countryCode": "KE", "name": "Kenya", "coords": [1, 38]},
//            {"countryCode": "KR", "name": "Korea", "coords": [37, 127.5]},
//            {"countryCode": "KW", "name": "Kuwait", "coords": [29.3375, 47.6581]},
//            {"countryCode": "KG", "name": "Kyrgyzstan", "coords": [41, 75]},
//            {"countryCode": "LA", "name": "Lao PDR", "coords": [18, 105]},
//            {"countryCode": "LV", "name": "Latvia", "coords": [57, 25]},
//            {"countryCode": "LB", "name": "Lebanon", "coords": [33.8333, 35.8333]},
//            {"countryCode": "LS", "name": "Lesotho", "coords": [-29.5, 28.5]},
//            {"countryCode": "LR", "name": "Liberia", "coords": [6.5, -9.5]},
//            {"countryCode": "LY", "name": "Libya", "coords": [25, 17]},
//            {"countryCode": "LT", "name": "Lithuania", "coords": [56, 24]},
//            {"countryCode": "LU", "name": "Luxembourg", "coords": [49.75, 6.1667]},
//            {"countryCode": "MK", "name": "Macedonia", "coords": [41.8333, 22]},
//            {"countryCode": "MG", "name": "Madagascar", "coords": [-20, 47]},
//            {"countryCode": "MW", "name": "Malawi", "coords": [-13.5, 34]},
//            {"countryCode": "MY", "name": "Malaysia", "coords": [2.5, 112.5]},
//            {"countryCode": "ML", "name": "Mali", "coords": [17, -4]},
//            {"countryCode": "MR", "name": "Mauritania", "coords": [20, -12]},
//            {"countryCode": "MX", "name": "Mexico", "coords": [23, -102]},
//            {"countryCode": "MD", "name": "Moldova", "coords": [47, 29]},
//            {"countryCode": "MN", "name": "Mongolia", "coords": [46, 105]},
//            {"countryCode": "ME", "name": "Montenegro", "coords": [42, 19]},
//            {"countryCode": "MA", "name": "Morocco", "coords": [32, -5]},
//            {"countryCode": "MZ", "name": "Mozamb", "coords": [-18.25, 35]},
//            {"countryCode": "MM", "name": "Myanmar", "coords": [22, 98]},
//            {"countryCode": "NA", "name": "Namibia", "coords": [-22, 17]},
//            {"countryCode": "NP", "name": "Nepal", "coords": [28, 84]},
//            {"countryCode": "NL", "name": "Netherlands", "coords": [52.5, 5.75]},
//            {"countryCode": "NC", "name": "New Caledonia", "coords": [-21.5, 165.5]},
//            {"countryCode": "NZ", "name": "New Zealand", "coords": [-41, 174]},
//            {"countryCode": "NI", "name": "Nicaragua", "coords": [13, -85]},
//            {"countryCode": "NE", "name": "Niger", "coords": [16, 8]},
//            {"countryCode": "NG", "name": "Nigeria", "coords": [10, 8]},
//            {"countryCode": "NO", "name": "Norway", "coords": [62, 10]},
//            {"countryCode": "OM", "name": "Oman", "coords": [21, 57]},
//            {"countryCode": "PK", "name": "Pakistan", "coords": [30, 70]},
//            {"countryCode": "PS", "name": "Palestine", "coords": [32, 35.25]},
//            {"countryCode": "PA", "name": "Panama", "coords": [9, -80]},
//            {"countryCode": "PG", "name": "Papua New Guinea", "coords": [-6, 147]},
//            {"countryCode": "PY", "name": "Paraguay", "coords": [-23, -58]},
//            {"countryCode": "PE", "name": "Peru", "coords": [-10, -76]},
//            {"countryCode": "PH", "name": "Philippines", "coords": [13, 122]},
//            {"countryCode": "PL", "name": "Poland", "coords": [52, 20]},
//            {"countryCode": "PT", "name": "Portugal", "coords": [39.5, -8]},
//            {"countryCode": "PR", "name": "Puerto Rico", "coords": [18.25, -66.5]},
//            {"countryCode": "QA", "name": "Qatar", "coords": [25.5, 51.25]},
//            {"countryCode": "RO", "name": "Romania", "coords": [46, 25]},
//            {"countryCode": "RU", "name": "Russia", "coords": [60, 100]},
//            {"countryCode": "RW", "name": "Rwanda", "coords": [-2, 30]},
//            {"countryCode": "SS",
//                "name": "S. Sudan"
//            },
//            {"countryCode": "SA", "name": "Saudi Arabia", "coords": [25, 45]},
//            {"countryCode": "SN", "name": "Senegal", "coords": [14, -14]},
//            {"countryCode": "RS", "name": "Serbia", "coords": [44, 21]},
//            {"countryCode": "SL", "name": "Sierra Leone", "coords": [8.5, -11.5]},
//            {"countryCode": "SK", "name": "Slovakia", "coords": [48.6667, 19.5]},
//            {"countryCode": "SI", "name": "Slovenia", "coords": [46, 15]},
//            {"countryCode": "SB", "name": "Solomon Is.", "coords": [-8, 159]},
//            {"countryCode": "SO", "name": "Somalia", "coords": [10, 49]},
//            {"countryCode": "ZA", "name": "South Africa", "coords": [-29, 24]},
//            {"countryCode": "ES", "name": "Spain", "coords": [40, -4]},
//            {"countryCode": "LK", "name": "Sri Lanka", "coords": [7, 81]},
//            {"countryCode": "SD", "name": "Sudan", "coords": [15, 30]},
//            {"countryCode": "SR", "name": "Suriname", "coords": [4, -56]},
//            {"countryCode": "SZ", "name": "Swaziland", "coords": [-26.5, 31.5]},
//            {"countryCode": "SE", "name": "Sweden", "coords": [62, 15]},
//            {"countryCode": "CH", "name": "Switzerland", "coords": [47, 8]},
//            {"countryCode": "SY", "name": "Syria", "coords": [35, 38]},
//            {"countryCode": "TW", "name": "Taiwan", "coords": [23.5, 121]},
//            {"countryCode": "TJ", "name": "Tajikistan", "coords": [39, 71]},
//            {"countryCode": "TZ", "name": "Tanzania", "coords": [-6, 35]},
//            {"countryCode": "TH", "name": "Thailand", "coords": [15, 100]},
//            {"countryCode": "TL",
//                "name": "Timor-Leste"
//            },
//            {"countryCode": "TG", "name": "Togo", "coords": [8, 1.1667]},
//            {"countryCode": "TT", "name": "Trinidad and Tobago", "coords": [11, -61]},
//            {"countryCode": "TN", "name": "Tunisia", "coords": [34, 9]},
//            {"countryCode": "TR", "name": "Turkey", "coords": [39, 35]},
//            {"countryCode": "TM", "name": "Turkmenistan", "coords": [40, 60]},
//            {"countryCode": "UG", "name": "Uganda", "coords": [1, 32]},
//            {"countryCode": "UA", "name": "Ukraine", "coords": [49, 32]},
//            {"countryCode": "AE", "name": "United Arab Emirates", "coords": [24, 54]},
//            {"countryCode": "GB", "name": "United Kingdom", "coords": [54, -2]},
//            {"countryCode": "US", "name": "United States", "coords": [38, -97]},
//            {"countryCode": "UY", "name": "Uruguay", "coords": [-33, -56]},
//            {"countryCode": "UZ", "name": "Uzbekistan", "coords": [41, 64]},
//            {"countryCode": "VU", "name": "Vanuatu", "coords": [-16, 167]},
//            {"countryCode": "VE", "name": "Venezuela", "coords": [8, -66]},
//            {"countryCode": "VN", "name": "Vietnam", "coords": [16, 106]},
//            {"countryCode": "EH", "name": "W. Sahara", "coords": [24.5, -13]},
//            {"countryCode": "YE", "name": "Yemen", "coords": [15, 48]},
//            {"countryCode": "ZM", "name": "Zambia", "coords": [-15, 30]},
//            {"countryCode": "ZW", "name": "Zimbabwe", "coords": [-20, 30]}
//        ]}
//
//    var countCountry = 0;
//
//    var refresh = setInterval(function() {
//        worldMap.addMarker(markerIndex, {latLng: jvmCountries.countries[countCountry].coords, name: jvmCountries.countries[countCountry].name});
//
//        markerIndex += 1;
//        countCountry += 1;
//    }, 5000)
//
    var pallete = ["#99B2FF", "#CCD8FF", "#0047B2"]

    generateColor = function() {
        var colors = {},
                key;

        for (key in worldMap.regions) {
            colors[key] = pallete[Math.floor(Math.random() * pallete.length)];
        }
        return colors;

    }

    worldMap.series.regions[0].setValues(generateColor())
//
//    Log = (function() {
//        function Log(config) {
//            this.elem = jQuery("#log");
//            this.max = config.markersMaxVisible;
//            this.fitSize();
//        }
//
//        Log.prototype.fitSize = function() {
//            this.elem.width(0.3 * jQuery(document).width());
//            this.elem.css("margin-top", 0.01 * jQuery(document).height());
//            return this.elem.height(0.10 * jQuery(document).height());
//        };
//
//        Log.prototype.clearOld = function() {
//            var entries;
//            entries = this.elem.find("div.log_entry");
//            if (entries.length >= this.max) {
//                entries.slice(0, entries.length / 2).remove();
//                return this.elem.find("br").nextUntil('div.log_entry', 'br').remove();
//            }
//        };
//
//        Log.prototype.add = function(msg) {
//            var scroll;
//            this.clearOld();
//            scroll = this.elem.scrollTop() + this.elem.innerHeight() === this.elem[0].scrollHeight;
//            this.elem.append('<div class="log_entry">' + msg + '</div><br/>');
//            if (scroll) {
//                return this.elem.scrollTop(this.elem[0].scrollHeight);
//            }
//        };
//
//        return Log;
//
//    })();
//
//    config = {
//        markersMaxVisible: 5,
//        colors: {
//            src: {
//                stroke: 'darkred',
//                fill: 'red'
//            },
//            dst: {
//                stroke: '#383F47',
//                fill: '#F8E23B'
//            },
//            scale: ['#FFFFFF', '#0071A4']
//        }
//    };
//
//    jQuery(document).ready(function() {
//        var log;
//        log = new Log(config);
//        jQuery(window).resize(function() {
//            return log.fitSize();
//        });
//        log.add("<b>Welcome to HoneyMap. This is a BETA version! Bug reports welcome :-)</b>");
//        log.add("Note that this is not <b>all</b> honeypots of the Honeynet Project,");
//        log.add("only those who voluntarily publish their captures to hpfeeds!");
//        log.add("");
//        var currentDate = new Date();
//        var country = 0;
//        setInterval(function() {
//            log.add("<span class='log_txt'>" + currentDate.getDate() + "/" + (currentDate.getMonth() + 1) + "/" + currentDate.getFullYear() + " "
//                    + currentDate.getHours() + ":" + currentDate.getMinutes() + " - "
//                    + "Attack from " + jvmCountries.countries[country].name + " to Port " + Math.floor(Math.random() * 999) + 1 + "</span>")
//            country += 1;
//        }, 5000)
//
//        return new Feed(honeymap, log, "geoloc.events");
//    });
//
//    Feed = (function() {
//        function Feed(map, log, instance) {
//            this.handler = __bind(this.handler, this);
//            var transport;
//            this.map = map;
//            this.log = log;
//            transport = new Transport(instance, this.handler, log);
//        }
//
//        Feed.prototype.handler = function(data) {
//            var dst, lat1, lat2, lng1, lng2, src;
//            lat1 = data.latitude;
//            lng1 = data.longitude;
//            if (!(lat1 && lng1)) {
//                return;
//            }
//            src = new Marker(this.map, lat1, lng1, data.type, "src", data.countrycode, data.city);
//            if (src.x === 0 && src.y === 0) {
//                return;
//            }
//            lat2 = data.latitude2;
//            lng2 = data.longitude2;
//            if (lat2 && lng2) {
//                dst = new Marker(this.map, lat2, lng2, data.type, "dst", data.countrycode2, data.city2);
//                if (dst.x === 0 && dst.y === 0) {
//                    dst = null;
//                }
//            }
//            this.addLog(src, dst, data.md5);
//            this.map.addMarker(src);
//            if (dst) {
//                return this.map.addMarker(dst);
//            }
//        };
//
//        Feed.prototype.addLog = function(src, dst, md5) {
//            var attacktype, logstr, timestamp;
//            if (!src.regionName()) {
//                return;
//            }
//            timestamp = new Date().toTimeString().substring(0, 8);
//            attacktype = src.eventName === "thug.events" ? "scan" : "attack";
//            logstr = "<div class=\"log_timestamp\">" + timestamp + "</div> \n<div class=\"log_bracket\">&lt;</div>" + src.eventName + "<div class=\"log_bracket\">&gt;</div> \nNew " + attacktype + " from <div class=\"log_country\">" + (src.location()) + "</div> \n<small>" + (src.name()) + "</small>";
//            if (dst && dst.regionName()) {
//                logstr += " to <div class=\"log_country\">" + (dst.location()) + "</div> <small>" + (dst.name()) + "</small>";
//            }
//            if (md5) {
//                logstr += " <div class=\"log_bracket2\">[</div>" + ("<div class=\"log_info\"><a href=\"http://www.virustotal.com/search/?query=" + md5 + "\">" + (md5.substr(0, 9)) + "</a></div>") + "<div class=\"log_bracket2\">]</div>";
//            }
//            return this.log.add(logstr);
//        };
//
//        return Feed;
//
//
//
//    })();

}