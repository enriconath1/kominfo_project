var connection;
var username;
var markerIndex = 0;
var arrayMarkers = [];

function connect() {
    connection = new WebSocket('wss://webservice-beta.honeynet.id/websocket/liveTicker');

    connection.onopen = function () {
        $('table#live > tbody:last').empty();

    };

    connection.onerror = function (error) {
        console.log('WebSocket Error ' + error);
    };

    connection.onmessage = function (msg) {
        var isi = JSON.parse(msg.data);
        var sourceCountry = isi.sourceCountryCode;
        var portDest = isi.destinationPort;
        var myDate = new Date(isi.timestamp);
        var hour;
        var minute;
        var second;
        var mapWD = $('#world-map-container').vectorMap('get', 'mapObject');

        $.each(countryList.countries, function (i, v) {
            if (v.countryCode == sourceCountry) {
                for (var i = 0; i < arrayMarkers.length; i++) {
                    if (arrayMarkers[i].codeOfCountry == v.countryCode) {
                        mapWD.removeMarkers([arrayMarkers[i].idMarkers]);
                        arrayMarkers.splice(i, 1);
                    }
                }
                
                var point = mapWD.latLngToPoint(v.coords[0], v.coords[1]);
                var pointX = parseFloat(point.x);
                var pointY = parseFloat(point.y);

                var indoPoint = mapWD.latLngToPoint(-5,120);
                var indoX = parseFloat(indoPoint.x);
                var indoY = parseFloat(indoPoint.y);
                var curvedLine = document.createElementNS('http://www.w3.org/2000/svg', 'path');
                curvedLine.setAttribute('id', 'curvedLine'+markerIndex);
                curvedLine.setAttribute('class', 'curved-line-attack');
                curvedLine.setAttribute('d', 'M'+pointX+" "+pointY+" C "+(pointX+5)+" "+(pointY-60)+" "+(indoX-10)+" "+(indoY-60)+" "+indoX+" "+indoY);
                curvedLine.setAttribute('stroke', 'red');
                curvedLine.setAttribute('stroke-width', 5);
                curvedLine.setAttribute('fill', 'transparent');
                $('.jvectormap-container svg').append(curvedLine);
                // var kotak = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
                // kotak.setAttribute('x',pointX);
                // kotak.setAttribute('y',pointY);
                // kotak.setAttribute('width',150);
                // kotak.setAttribute('height',150);
                // $('.jvectormap-container svg').append(kotak);
                var countryLatLang = v.coords;
                mapWD.addMarker('targetMarker'+markerIndex, {latLng: [-5,120], style: {fill: 'rgba(255,255,0,1)'}});
                mapWD.addMarker(markerIndex, {latLng: countryLatLang});

                arrayMarkers.push(
                        {
                            idMarkers: markerIndex,
                            codeOfCountry: v.countryCode
                        }
                );
                markerIndex += 1;
                return;
            }
        });


        if (myDate.getHours() < 10) {
            hour = "0" + myDate.getHours();
        }
        else {
            hour = myDate.getHours();
        }

        if (myDate.getMinutes() < 10) {
            minute = "0" + myDate.getMinutes();
        }
        else {
            minute = myDate.getMinutes();
        }

        if (myDate.getSeconds() < 10) {
            second = "0" + myDate.getSeconds();
        }
        else {
            second = myDate.getSeconds();
        }
        
        if(sourceCountry == "None"){
            
        }
        else {
            $('table#live').prepend('<tr style="text-align: center"><td>' + hour + ":" + minute + ":" + second + '</td><td>' + sourceCountry + '</td><td>' + portDest + '</td></tr>')
            var rowCount = $('table#live tr').length;
            if (rowCount > 4) {
                $('table#live tr:last').remove;
            }
        }
//        if (sourceCountry != "NONE" || sourceCountry !=  "None") {
//            console.log(sourceCountry);
//            $('table#live').prepend('<tr style="text-align: center"><td>' + hour + ":" + minute + ":" + second + '</td><td>' + sourceCountry + '</td><td>' + portDest + '</td></tr>')
//            var rowCount = $('table#live tr').length;
//            if (rowCount > 4) {
//                $('table#live tr:last').remove;
//            }
//        }


        //document.getElementById('msgBox').innerHTML += myDate.getHours()+":"+ myDate.getMinutes()+ ":" + myDate.getSeconds() +" Attack to port "+ portDest + " from " + sourceCountry +'<br>';
    };
}

function disconnect() {
    connection.close()
}

connect();

