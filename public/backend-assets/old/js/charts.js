$(document).ready(function() {

    var d1 = [];
    
    //January
    for(var i=1;i<32;i++){
        var date = new Date("January "+i+", 2014 00:00:00");
        var epoch = date.getTime();
        d1.push([epoch, Math.floor((Math.random() * 90000) + 10000)]);
    }
    
    //February
    for(var i=1;i<29;i++){
        var date = new Date("February "+i+", 2014 00:00:00");
        var epoch = date.getTime();
        d1.push([epoch, Math.floor((Math.random() * 90000) + 10000)]);
    }
    
    //March
    for(var i=1;i<32;i++){
        var date = new Date("March "+i+", 2014 00:00:00");
        var epoch = date.getTime();
        d1.push([epoch, Math.floor((Math.random() * 90000) + 10000)]);
    }
    
    //April
    for(var i=1;i<31;i++){
        var date = new Date("April "+i+", 2014 00:00:00");
        var epoch = date.getTime();
        d1.push([epoch, Math.floor((Math.random() * 90000) + 10000)]);
    }
    
    //May
    for(var i=1;i<32;i++){
        var date = new Date("May "+i+", 2014 00:00:00");
        var epoch = date.getTime();
        d1.push([epoch, Math.floor((Math.random() * 90000) + 10000)]);
    }
    
    //June
    for(var i=1;i<31;i++){
        var date = new Date("June "+i+", 2014 00:00:00");
        var epoch = date.getTime();
        d1.push([epoch, Math.floor((Math.random() * 90000) + 10000)]);
    }
    
    //July
    for(var i=1;i<32;i++){
        var date = new Date("July "+i+", 2014 00:00:00");
        var epoch = date.getTime();
        d1.push([epoch, Math.floor((Math.random() * 90000) + 10000)]);
    }
    
    //August
    for(var i=1;i<32;i++){
        var date = new Date("August "+i+", 2014 00:00:00");
        var epoch = date.getTime();
        d1.push([epoch, Math.floor((Math.random() * 90000) + 10000)]);
    }
    
    //September
    for(var i=1;i<31;i++){
        var date = new Date("September "+i+", 2014 00:00:00");
        var epoch = date.getTime();
        d1.push([epoch, Math.floor((Math.random() * 90000) + 10000)]);
    }
    
    //October
    for(var i=1;i<32;i++){
        var date = new Date("October "+i+", 2014 00:00:00");
        var epoch = date.getTime();
        d1.push([epoch, Math.floor((Math.random() * 90000) + 10000)]);
    }
    
    //November
    for(var i=1;i<31;i++){
        var date = new Date("November "+i+", 2014 00:00:00");
        var epoch = date.getTime();
        d1.push([epoch, Math.floor((Math.random() * 90000) + 10000)]);
    }
    
    //December
    for(var i=1;i<32;i++){
        var date = new Date("December "+i+", 2014 00:00:00");
        var epoch = date.getTime();
        d1.push([epoch, Math.floor((Math.random() * 90000) + 10000)]);
    }


    var data1 = [
        {
            data: d1,
            points: {fillColor: "#4572A7", size: 5},
            color: "#4572A7"
        }
    ]
    
    var pallete = ["#99B2FF", "#CCD8FF", "#0047B2"]
    
    generateColor = function(){
        var colors = {},
            key;
    
        for(key in worldMap.regions){
            colors[key] = pallete[Math.floor(Math.random()*pallete.length)];
        }
        return colors;
        
    }

    $.plot($("#chart"), data1, {
        xaxis: {
            min: (new Date(2013, 12, 1)).getTime(),
            max: (new Date(2014, 12, 1)).getTime(),
            mode: "time",
            tickSize: [1, "month"],
            monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            tickLength: 0,
            axisLabel: 'Month',
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
            axisLabelPadding: 5
        },
        yaxis: {
            axisLabel: 'Amount',
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
            axisLabelPadding: 5,
			tickLength: 0
        },
        series: {
            lines: {
                show: true,
                fill: true
            },
            points: {
                show: false
            },
        },
        grid: {
            borderWidth: 0,
            hoverable: true,
            clickable: true
        },
        tooltip: true,
        tooltipOpts: {
            content: '%x - %y<br>2014',
            shifts: {
                x: -60,
                y: 25
            },
            xDateFormat: "%b %d"
        }
    });
    
//    worldMap.series.regions[0].setValues(generateColor())
    
    var pallete = ["#99B2FF", "#CCD8FF", "#0047B2"]
    var regionStyle = {
                    fill: pallete[Math.floor(Math.random() * pallete.length)]
                }
    
    $("#chart").bind("plotclick",function(event, pos, item){
        if(item){
//            worldMap.series.regions[0].setValues(generateColor())
        }
    });
});


