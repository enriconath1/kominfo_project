$(document).ready(function() {
    var d1 = [];
    var d2 = [];
    var d3 = [];
    
    for(var i=0;i<13;i++){
        var date = new Date(2014, i, 1, 00, 00, 00);
        var epoch = date.getTime();
        d1.push([epoch, Math.floor((Math.random() * 9899) + 100)]);
    }
    
    for(var i=0;i<13;i++){
        var date = new Date(2014, i, 1, 00, 00, 00);
        var epoch = date.getTime();
        d2.push([epoch, Math.floor((Math.random() * 9899) + 100)]);
    }
    
    for(var i=0;i<13;i++){
        var date = new Date(2014, i, 1, 00, 00, 00);
        var epoch = date.getTime();
        d3.push([epoch, Math.floor((Math.random() * 99) + 1)]);
    }
    


    var data1 = [
        {
            data: d1,
            points: {fillColor: "#4572A7", size: 5},
            color: "#4572A7"
        }
    ]
    
    var data2 = [
        {
            data: d2,
            points: {fillColor: "#4572A7", size: 5},
            color: "#FF6666"
        }
    ]
    
    var data3 = [
        {
            data: d3,
            points: {fillColor: "#4572A7", size: 5},
            color: "#5CE68A"
        }
    ]
    
    

//    $.plot($("#attacked"), data1, {
//        xaxis: {
//            min: (new Date(2013, 12, 1)).getTime(),
//            max: (new Date(2014, 12, 1)).getTime(),
//            mode: "time",
//            tickSize: [1, "month"],
//            monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
//            tickLength: 0,
//            axisLabel: 'Month',
//            axisLabelUseCanvas: true,
//            axisLabelFontSizePixels: 12,
//            axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
//            axisLabelPadding: 5
//        },
//        yaxis: {
//            axisLabel: 'Amount',
//            axisLabelUseCanvas: true,
//            axisLabelFontSizePixels: 12,
//            axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
//            axisLabelPadding: 5
//        },
//        series: {
//            lines: {
//                show: true,
//                fill: true
//            },
//            points: {
//                show: false
//            },
//        },
//        grid: {
//            borderWidth: 0,
//            hoverable: true
//        },
//        tooltip: true,
//        tooltipOpts: {
//            content: '%x - %y<br>2014',
//            shifts: {
//                x: -60,
//                y: 25
//            },
//            xDateFormat: "%b %d"
//        }
//    });
    
//    $.plot($("#attacking"), data2, {
//        xaxis: {
//            min: (new Date(2013, 12, 1)).getTime(),
//            max: (new Date(2014, 12, 1)).getTime(),
//            mode: "time",
//            tickSize: [1, "month"],
//            monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
//            tickLength: 0,
//            axisLabel: 'Month',
//            axisLabelUseCanvas: true,
//            axisLabelFontSizePixels: 12,
//            axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
//            axisLabelPadding: 5
//        },
//        yaxis: {
//            axisLabel: 'Amount',
//            axisLabelUseCanvas: true,
//            axisLabelFontSizePixels: 12,
//            axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
//            axisLabelPadding: 5
//        },
//        series: {
//            lines: {
//                show: true,
//                fill: true
//            },
//            points: {
//                show: false
//            },
//        },
//        grid: {
//            borderWidth: 0,
//            hoverable: true
//        },
//        tooltip: true,
//        tooltipOpts: {
//            content: '%x - %y<br>2014',
//            shifts: {
//                x: -60,
//                y: 25
//            },
//            xDateFormat: "%b %d"
//        }
//    });
    
//    $.plot($("#totalMalware"), data3, {
//        xaxis: {
//            min: (new Date(2013, 12, 1)).getTime(),
//            max: (new Date(2014, 12, 1)).getTime(),
//            mode: "time",
//            tickSize: [1, "month"],
//            monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
//            tickLength: 0,
//            axisLabel: 'Month',
//            axisLabelUseCanvas: true,
//            axisLabelFontSizePixels: 12,
//            axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
//            axisLabelPadding: 5
//        },
//        yaxis: {
//            axisLabel: 'Amount',
//            axisLabelUseCanvas: true,
//            axisLabelFontSizePixels: 12,
//            axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
//            axisLabelPadding: 5
//        },
//        series: {
//            lines: {
//                show: true,
//                fill: true
//            },
//            points: {
//                show: false
//            },
//        },
//        grid: {
//            borderWidth: 0,
//            hoverable: true
//        },
//        tooltip: true,
//        tooltipOpts: {
//            content: '%x - %y<br>2014',
//            shifts: {
//                x: -60,
//                y: 25
//            },
//            xDateFormat: "%b %d"
//        }
//    });
})