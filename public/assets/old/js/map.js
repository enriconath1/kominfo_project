$(function() {
    loadWorldMap();
    $("#malware-list").niceScroll({cursorcolor: "#00F"});

    $(document).on('click', '#trigger', function() {
        $('#tray').animate({
            top: "0%",
        }, 2000);
    });



    $(document).on('click', '.map-switch', function() {
        var id = $(this).attr('id');
        if (id == 'world-map') {
            $('#map-container').html('<div id="world-map" style="height:520px"></div>');
            loadWorldMap();
        } else {
            $('#map-container').html('<div id="map" style="height:520px"></div>');
            loadIndonesiaMap();
//            $("#map").on("petaklik", function(event, idwilayah) {
//
//                $('#myModal').modal('show');
//                $('#modalTitle').html(idwilayah);
//            });
        }
    });

});

var indoMap;

function loadIndonesiaMap() {
    $("#map-title").html("Indonesia Map");
    var markerIndex = 0,
            markersCoords = {};

    indoMap = new jvm.Map({
        map: 'id_mill_en',
        backgroundColor: 'transparent',
        container: $('#map'),
        series: {
            regions: [{
                    attribute: 'fill'
                }]
        },
        onRegionOver: function(e, code) {
            $(this).on('click', function() {
                var map = $('#map').vectorMap('get', 'mapObject');
                var regionName = map.getRegionName(code); 
                $("#modalTitle").html(regionName);
                $("#myModal").modal('show');
            });
        },
        onRegionOut: function(e, code) {
            $(this).unbind('click');
        },
		zoomOnScroll: false,
		zoomButtons: false,
    });

    var pallete = ["#99B2FF", "#CCD8FF", "#0047B2"]

    generateColor = function() {
        var colors = {},
                key;

        for (key in indoMap.regions) {
            colors[key] = pallete[Math.floor(Math.random() * pallete.length)];
        }
        return colors;

    }

    indoMap.series.regions[0].setValues(generateColor())
}

var worldMap;

function loadWorldMap() {
    $("#map-title").html("World Map");
    var markerIndex = 0,
            markersCoords = {};

    worldMap = new jvm.Map({
        map: 'world_mill_en',
        backgroundColor: 'transparent',
        series: {
            regions: [{
                    attribute: 'fill'
                }]
        },
        markerStyle: {
            initial: {
                fill: 'red'
            }
        },
        container: $('#world-map'),
        onMarkerTipShow: function(e, label, code) {
            worldMap.tip.text(markersCoords[code].lat.toFixed(2) + ', ' + markersCoords[code].lng.toFixed(2));
        },
        series: {
            regions: [{
                    attribute: 'fill'
                }]
        },
        onRegionOver: function(e, code) {
            $(this).on('click', function() {
                var map = $('#world-map').vectorMap('get', 'mapObject');
                var regionName = map.getRegionName(code);
                var regionCode = map.getSelectedRegions();
                $("#modalTitle").html(regionName+"-"+regionCode);
                $("#myModal").modal('show');
            });
        },
        onRegionOut: function(e, code) {
            $(this).unbind('click');
        },
		zoomOnScroll: false,
		zoomButtons: false,
    });
    
    var pallete = ["#99B2FF", "#CCD8FF", "#0047B2"]

    generateColor = function() {
        var colors = {},
                key;

        for (key in worldMap.regions) {
            colors[key] = pallete[Math.floor(Math.random() * pallete.length)];
        }
        return colors;

    }

    worldMap.series.regions[0].setValues(generateColor())
//
//    Log = (function() {
//        function Log(config) {
//            this.elem = jQuery("#log");
//            this.max = config.markersMaxVisible;
//            this.fitSize();
//        }
//
//        Log.prototype.fitSize = function() {
//            this.elem.width(0.3 * jQuery(document).width());
//            this.elem.css("margin-top", 0.01 * jQuery(document).height());
//            return this.elem.height(0.10 * jQuery(document).height());
//        };
//
//        Log.prototype.clearOld = function() {
//            var entries;
//            entries = this.elem.find("div.log_entry");
//            if (entries.length >= this.max) {
//                entries.slice(0, entries.length / 2).remove();
//                return this.elem.find("br").nextUntil('div.log_entry', 'br').remove();
//            }
//        };
//
//        Log.prototype.add = function(msg) {
//            var scroll;
//            this.clearOld();
//            scroll = this.elem.scrollTop() + this.elem.innerHeight() === this.elem[0].scrollHeight;
//            this.elem.append('<div class="log_entry">' + msg + '</div><br/>');
//            if (scroll) {
//                return this.elem.scrollTop(this.elem[0].scrollHeight);
//            }
//        };
//
//        return Log;
//
//    })();
//
//    config = {
//        markersMaxVisible: 5,
//        colors: {
//            src: {
//                stroke: 'darkred',
//                fill: 'red'
//            },
//            dst: {
//                stroke: '#383F47',
//                fill: '#F8E23B'
//            },
//            scale: ['#FFFFFF', '#0071A4']
//        }
//    };
//
//    jQuery(document).ready(function() {
//        var log;
//        log = new Log(config);
//        jQuery(window).resize(function() {
//            return log.fitSize();
//        });
//        log.add("<b>Welcome to HoneyMap. This is a BETA version! Bug reports welcome :-)</b>");
//        log.add("Note that this is not <b>all</b> honeypots of the Honeynet Project,");
//        log.add("only those who voluntarily publish their captures to hpfeeds!");
//        log.add("");
//        var currentDate = new Date();
//        var country = 0;
//        setInterval(function() {
//            log.add("<span class='log_txt'>" + currentDate.getDate() + "/" + (currentDate.getMonth() + 1) + "/" + currentDate.getFullYear() + " "
//                    + currentDate.getHours() + ":" + currentDate.getMinutes() + " - "
//                    + "Attack from " + jvmCountries.countries[country].name + " to Port " + Math.floor(Math.random() * 999) + 1 + "</span>")
//            country += 1;
//        }, 5000)
//
//        return new Feed(honeymap, log, "geoloc.events");
//    });
//
//    Feed = (function() {
//        function Feed(map, log, instance) {
//            this.handler = __bind(this.handler, this);
//            var transport;
//            this.map = map;
//            this.log = log;
//            transport = new Transport(instance, this.handler, log);
//        }
//
//        Feed.prototype.handler = function(data) {
//            var dst, lat1, lat2, lng1, lng2, src;
//            lat1 = data.latitude;
//            lng1 = data.longitude;
//            if (!(lat1 && lng1)) {
//                return;
//            }
//            src = new Marker(this.map, lat1, lng1, data.type, "src", data.countrycode, data.city);
//            if (src.x === 0 && src.y === 0) {
//                return;
//            }
//            lat2 = data.latitude2;
//            lng2 = data.longitude2;
//            if (lat2 && lng2) {
//                dst = new Marker(this.map, lat2, lng2, data.type, "dst", data.countrycode2, data.city2);
//                if (dst.x === 0 && dst.y === 0) {
//                    dst = null;
//                }
//            }
//            this.addLog(src, dst, data.md5);
//            this.map.addMarker(src);
//            if (dst) {
//                return this.map.addMarker(dst);
//            }
//        };
//
//        Feed.prototype.addLog = function(src, dst, md5) {
//            var attacktype, logstr, timestamp;
//            if (!src.regionName()) {
//                return;
//            }
//            timestamp = new Date().toTimeString().substring(0, 8);
//            attacktype = src.eventName === "thug.events" ? "scan" : "attack";
//            logstr = "<div class=\"log_timestamp\">" + timestamp + "</div> \n<div class=\"log_bracket\">&lt;</div>" + src.eventName + "<div class=\"log_bracket\">&gt;</div> \nNew " + attacktype + " from <div class=\"log_country\">" + (src.location()) + "</div> \n<small>" + (src.name()) + "</small>";
//            if (dst && dst.regionName()) {
//                logstr += " to <div class=\"log_country\">" + (dst.location()) + "</div> <small>" + (dst.name()) + "</small>";
//            }
//            if (md5) {
//                logstr += " <div class=\"log_bracket2\">[</div>" + ("<div class=\"log_info\"><a href=\"http://www.virustotal.com/search/?query=" + md5 + "\">" + (md5.substr(0, 9)) + "</a></div>") + "<div class=\"log_bracket2\">]</div>";
//            }
//            return this.log.add(logstr);
//        };
//
//        return Feed;
//
//
//
//    })();

}