var chartData = [];

$(document).ready(function () {
    var pathArray = window.location.href.split('/');
    var protocol = pathArray[0];
    var host = pathArray[2];
    var baseurl = 'http://public.honeynet.id/api';

    var malwareWordcloudData = []

    Highcharts.setOptions({// This is for all plots, change Date axis to local timezone
        global: {
            timezoneOffset: -7 * 60
        }
    });

    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/malware/top-malware/AhnLab-V3/0/10",
        data: "",
        success: function (msg) {
            console.log('API: malware-top-malware:23 called.');
            console.log(msg);

            var count = 10;
            $.each(msg, function (i, data) {
                malwareWordcloudData.push([data.virustotalscan_result, count]);
                count--;
            })
            console.log(malwareWordcloudData);

            wordCloudMalware(malwareWordcloudData);
        }
    })

    //Function when wordcloud-container is clicked
    $(document).on('click', '#wordcloud-container', function () {
        var tanggalAwal = $('#firstDateSub').html()
        var tanggalAkhir = $('#lastDateSub').html()
        var splitTanggalAwal = tanggalAwal.split(" ");
        if (splitTanggalAwal[1] == 'Mei')
            splitTanggalAwal[1] = 'May'
        else if (splitTanggalAwal[1] == 'Agustus')
            splitTanggalAwal[1] = 'August'
        else if (splitTanggalAwal[1] == 'Oktober')
            splitTanggalAwal[1] = 'October'
        else if (splitTanggalAwal[1] == 'Desember')
            splitTanggalAwal[1] = 'December'

        var splitTanggalAkhir = tanggalAkhir.split(" ");
        if (splitTanggalAkhir[1] == 'Mei')
            splitTanggalAkhir[1] = 'May'
        else if (splitTanggalAkhir[1] == 'Agustus')
            splitTanggalAkhir[1] = 'August'
        else if (splitTanggalAkhir[1] == 'Oktober')
            splitTanggalAkhir[1] = 'October'
        else if (splitTanggalAkhir[1] == 'Desember')
            splitTanggalAkhir[1] = 'December'

        tanggalAwal = splitTanggalAwal.join(" ")
        tanggalAkhir = splitTanggalAkhir.join(" ")
        var dateFirst = new Date(tanggalAwal);
        var dateLast = new Date(tanggalAkhir)
        var dateEpochFirst = dateFirst.getTime() / 1000;
        var dateEpochLast = dateLast.getTime() / 1000;
        
        $("#modalTitleDateMalware").html(checkTime(splitTanggalAwal[0]) + " " + checkTime(splitTanggalAwal[1]) + " " + splitTanggalAwal[2] + " s/d " + checkTime(splitTanggalAkhir[0]) + " " + checkTime(splitTanggalAkhir[1]) + " " + splitTanggalAkhir[2])
        $("#malwareModal").modal('show');
        $.ajax({
            type: "GET",
            dataType: 'json',
            url: baseurl + "/top-malware-by-time/AhnLab-V3/" + dateEpochFirst + "/" + dateEpochLast + "/0/5",
            data: "",
            beforeSend: function () {
                console.log('Function: top-malware-by-time called.');
                $("#top-malware-chart").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>')
            },
            success: function (msg) {
                $("#top-malware-chart").html('');
                $("#top-malware > tbody").html('');
                var counter = 1;
                var d1 = [];
                $.each(msg, function (i, data) {
                    $('#top-malware > tbody:last').append("<tr><td>" + counter + "</td><td>"+data.virustotalscan_result+"</td><td class='text'>" + numberWithCommas(data.count) + "</td></tr>");
                    counter++;
                    d1[i] = {label: data.virustotalscan_result, data: Math.floor(data.count)};
                });

                //chartnya
                if ($("#top-malware-chart").length)
                {
                    $.plot($("#top-malware-chart"), d1,
                            {
                                series: {
                                    pie: {
                                        show: true,
                                        tilt: 0.5,
                                    }
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true
                                },
                                legend: {
                                    backgroundColor: "transparent",
                                    show: true
                                }
                            });


                    function pieHover(event, pos, obj)
                    {
                        if (!obj)
                            return;
                        percent = parseFloat(obj.series.percent).toFixed(2);
                        $("#hover").html('<span style="font-weight: bold; color: black">' + obj.series.label + ' (' + percent + '%)</span>');
                    }
                    $("#attacked-port").bind("plothover", pieHover);
                }
                
            }
        })
    })


    $("#world-map").addClass('active');

    $(document).on('click', '.map-switch', function () {
        var id = $(this).attr('id');
        if (id == 'world-map') {
            $(".jvectormap-tip").remove();
            $('#map-container').html('<div id="world-map-container"></div>');
            $("#world-map").addClass('active');
            $("#indonesia-map").removeClass('active');
            $("#map-title").html("Peta Serangan Siber Dunia");
            $('#top-attacker > tbody').html('');
            $('#target-source').html('Sumber Serangan')
            if ($('#tanggal-serangan').html() == 'Akumulasi Data') {
                getMapData(baseurl)
                getTopAttacker(baseurl)
            }
            else {
                var tanggalAwal = $('#firstDateSub').html()
                var tanggalAkhir = $('#lastDateSub').html()
                var splitTanggalAwal = tanggalAwal.split(" ");
                if (splitTanggalAwal[1] == 'Mei')
                    splitTanggalAwal[1] = 'May'
                else if (splitTanggalAwal[1] == 'Agustus')
                    splitTanggalAwal[1] = 'August'
                else if (splitTanggalAwal[1] == 'Oktober')
                    splitTanggalAwal[1] = 'October'
                else if (splitTanggalAwal[1] == 'Desember')
                    splitTanggalAwal[1] = 'December'

                var splitTanggalAkhir = tanggalAkhir.split(" ");
                if (splitTanggalAkhir[1] == 'Mei')
                    splitTanggalAkhir[1] = 'May'
                else if (splitTanggalAkhir[1] == 'Agustus')
                    splitTanggalAkhir[1] = 'August'
                else if (splitTanggalAkhir[1] == 'Oktober')
                    splitTanggalAkhir[1] = 'October'
                else if (splitTanggalAkhir[1] == 'Desember')
                    splitTanggalAkhir[1] = 'December'

                tanggalAwal = splitTanggalAwal.join(" ")
                tanggalAkhir = splitTanggalAkhir.join(" ")
                var dateFirst = new Date(tanggalAwal);
                var dateLast = new Date(tanggalAkhir)
                var dateEpochFirst = dateFirst.getTime() / 1000;
                var dateEpochLast = dateLast.getTime() / 1000;
                filteringMapsAll(baseurl, dateEpochFirst, dateEpochLast)
                filteringTopAttacker(baseurl, dateEpochFirst, dateEpochLast)
            }

        }
        else {
            $(".jvectormap-tip").remove();
            $('#map-container').html('<div id="indo-map"></div>');
            $("#map-title").html("Peta Serangan Siber Ke Indonesia");
            $("#world-map").removeClass('active');
            $("#indonesia-map").addClass('active');
            $('#top-attacker > tbody').html('');
            $('#target-source').html('Provinsi Sasaran')

            if ($('#tanggal-serangan').html() == 'Akumulasi Data') {
                getIndonesiaMapData(baseurl)
                getTopAttacker(baseurl)
            }
            else {
                var tanggalAwal = $('#firstDateSub').html()
                var tanggalAkhir = $('#lastDateSub').html()
                var splitTanggalAwal = tanggalAwal.split(" ");
                if (splitTanggalAwal[1] == 'Mei')
                    splitTanggalAwal[1] = 'May'
                else if (splitTanggalAwal[1] == 'Agustus')
                    splitTanggalAwal[1] = 'August'
                else if (splitTanggalAwal[1] == 'Oktober')
                    splitTanggalAwal[1] = 'October'
                else if (splitTanggalAwal[1] == 'Desember')
                    splitTanggalAwal[1] = 'December'

                var splitTanggalAkhir = tanggalAkhir.split(" ");
                if (splitTanggalAkhir[1] == 'Mei')
                    splitTanggalAkhir[1] = 'May'
                else if (splitTanggalAkhir[1] == 'Agustus')
                    splitTanggalAkhir[1] = 'August'
                else if (splitTanggalAkhir[1] == 'Oktober')
                    splitTanggalAkhir[1] = 'October'
                else if (splitTanggalAkhir[1] == 'Desember')
                    splitTanggalAkhir[1] = 'December'

                tanggalAwal = splitTanggalAwal.join(" ")
                tanggalAkhir = splitTanggalAkhir.join(" ")
                var dateFirst = new Date(tanggalAwal);
                var dateLast = new Date(tanggalAkhir)
                var dateEpochFirst = dateFirst.getTime() / 1000;
                var dateEpochLast = dateLast.getTime() / 1000;
                filteringIndoMapsAll(baseurl, dateEpochFirst, dateEpochLast)
                filteringTopAttacker(baseurl, dateEpochFirst, dateEpochLast)
            }


        }
    })
    $("#map-title").html("Peta Serangan Siber Dunia");

    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/widget/total-attack",
        data: "",
        cache: false,
        success: function (msg) {
            console.log('Function: widget-total-attack called.');
            console.log(msg);
            $.each(msg.data, function (i, data) {
                $("#modalJumlahSeranganDunia").html(numberWithCommas(data.count));
            })
        }});

    var monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
        "Juli", "Agustus", "September", "Oktober", "November", "Desember"
    ];
    var now = new Date();
    var startOfDay = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    var timestamp = startOfDay / 1000;

    filteringTopAttacker(baseurl, +timestamp - (2629743), timestamp);
    filteringMapsAll(baseurl, +timestamp - (2629743), timestamp);
    //getTopAttacker(baseurl);
    //getMapData(baseurl);
    getSensorCount(baseurl);

    //clickableCharts(baseurl);
    //saveToArray(baseurl);
    applyWhenDeferredYear(baseurl);
    getArrayData(baseurl, 1351728000, 1443503478);
});



function wordCloudMalware(malwareData) {
    var container = document.getElementById("wordcloud-container");
    var canvas = document.getElementById("malware-wordcloud");
    canvas.height = container.offsetHeight;
    canvas.width = container.offsetWidth;

    var options = {
        list: malwareData,
        backgroundColor: 'rgba(0,255,255,0.2)',
        color: 'random-light',
        shape: 'diamond',
//        gridSize: Math.round(4 * canvas.width / 64),
        weightFactor: 1.2,
//        color: '#12ace0',
//        rotateRatio: 0,
//        minSize: 2,
    }

    WordCloud(canvas, options);
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function getDeferredYear(baseurl) {
    var deferredYear = [];

    deferredYear.push(
            $.ajax({
                type: "GET",
                dataType: 'json',
                url: baseurl + "/getYears"
            })
            )

    return deferredYear;
}

function applyWhenDeferredYear(baseurl) {
    var deferredYear = getDeferredYear(baseurl);
    var years = [];
    var deferredMonth = [];
    var months = [];
    var deferredDate = [];

    $.when.apply(null, deferredYear).done(function () {
        $.each(deferredYear, function (i, data) {
            years = data.responseJSON;
        })
    })

    $.each(years, function (yearIndex, year) {
        deferredMonth.push(
                $.ajax({
                    type: "GET",
                    dataType: 'json',
                    url: baseurl + "/getMonthByYear/" + year
                })
                )
    })

    return deferredMonth;

//    $.when.apply(null, deferredMonth).done(function () {
//        $.each(deferredMonth, function (i, data) {
//            months = data.responseJSON;
//        })
//    })

//    $.each(years, function (yearIndex, year) {
//        $.each(months, function (monthIndex, month) {
//            deferredDate.push(
//                $.ajax({
//                        type: "GET",
//                        dataType: 'json',
//                        url: baseurl + "/getTotalPerDay/" + month + "/" + year,
//                    })
//                )
//        })
//    })
//    
//    $.when.apply(null, deferredDate).done(function(){
//        $.each(deferredDate, function(i, data){
//            alert("CountryCode: "+data.countryCode)
//        })
//    })

}

function loadIndonesiaMap() {
    var markerIndex = 0,
            markersCoords = {};

    var indoMap = new jvm.Map({
        map: 'id_mill_en',
        backgroundColor: 'transparent',
        container: $('#indo-map'),
        series: {
            regions: [{
                    attribute: 'fill'
                }]
        },
        onRegionOver: function (e, code) {
            $(this).on('click', function () {
                var map = $('#map').vectorMap('get', 'mapObject');
                var regionName = map.getRegionName(code);
                $("#modalTitle").html(regionName);
                $("#myModal").modal('show');
            });
        },
        onRegionOut: function (e, code) {
            $(this).unbind('click');
        },
        zoomOnScroll: false,
        zoomButtons: false,
    });

    var pallete = ["#801515", "#FFE200", "#0047B2"]

    generateColor = function () {
        var colors = {},
                key;

        for (key in indoMap.regions) {
            colors[key] = pallete[Math.floor(Math.random() * pallete.length)];
        }
        return colors;

    }

    indoMap.series.regions[0].setValues(generateColor())
}

function saveToArray(baseurl) {
    var years = [];
    var months = [];

    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getYears",
        data: "",
        success: function (msg) {
            $.each(msg, function (i, data) {
                years.push(data); //get Available Years
            })
            $.each(years, function (yearIndex, year) {
                $.ajax({
                    type: "GET",
                    dataType: 'json',
                    url: baseurl + "/getMonthByYear/" + year,
                    data: "",
                    success: function (msg2) {
                        $.each(msg2, function (j, data2) {
                            months.push(data2); //get Available Months
                        });
                        $.each(months, function (monthIndex, month) {
                            $.ajax({
                                type: "GET",
                                dataType: 'json',
                                url: baseurl + "/getTotalPerDay/" + month + "/" + year,
                                data: "",
                                success: function (msg3) {
                                    $.each(msg3, function (k, data3) {
                                        chartData.push([Date.UTC(year, month - 1, data3.timeUnit), data3.value]);
                                    });
                                },
                            })
                        })
                    }
                })
            })
        },
        complete: function (x) {
            $(document).ajaxStop(function () {
                alert("All AJAX requests completed");
                console.log("AJAX REQUEST COMPLETE!")
            });
        }
    })

//    $.ajax({
//        type: "GET",
//        dataType: 'json',
//        url: baseurl + "/getYears",
//        data: "",
//        success: function (msg) {
//            $.each(msg, function (i, data) {
//                years.push(data); //get Available Years
//            });
//            years.forEach(function (year) {
//                $.ajax({
//                    type: "GET",
//                    dataType: 'json',
//                    url: baseurl + "/getMonthByYear/" + year,
//                    data: "",
//                    success: function (msg2) {
//                        $.each(msg2, function (j, data2) {
//                            months.push(data2); //get Available Months
//                        });
//                        $.when(
//                                months.forEach(function (month) {
//                                    $.ajax({
//                                        type: "GET",
//                                        dataType: 'json',
//                                        url: baseurl + "/getTotalPerDay/" + month + "/" + year,
//                                        data: "",
//                                        success: function (msg3) {
//                                            $.each(msg3, function (k, data3) {
//                                                chartData.push([Date.UTC(year, month - 1, data3.timeUnit), data3.value]);
//                                            });
//                                        }})
//
//                                })).then(function (finish) {
//                            alert("Finished");
//                            getArrayData();
//                        })
//                    }
//                })
//
//            })
//        }})
}

function getArrayData(baseurl, firstDayEpoch, lastDayEpoch) {
//    var sortedData = chartData.sort();
//    var json = JSON.stringify(sortedData);
//    console.log(json);
//
    var chartDataHighstock = [];

    $.getJSON(baseurl + "/getAttackOnRange", function (data) {
        $.each(data, function (i, data2) {
            chartDataHighstock.push([data2.timestamp, data2.count])
        })
    }).done(function () {
        console.log(chartDataHighstock);

        $('#chart-container').highcharts('StockChart', {
            chart: {
                height: 250,
                width: 900,
                backgroundColor: 'transparent',
                type: 'area',
            },
            rangeSelector: {
                selected: 0,
            },
            title: {
                text: 'Rentang Waktu',
                style: {
                    color: '#12ace0',
                }
            },
            xAxis: {
                labels: {
                    style: {
                        color: '#FFF',
                        font: 'Jura, sans-serif'
                    }
                },
                events: {
                    setExtremes: function (e) {
                        var now = new Date();
                        var startOfDay = new Date(now.getFullYear(), now.getMonth(), now.getDate());
                        var startOfYear = new Date(now.getFullYear(), 0, 1);
                        var ytd = startOfYear / 1000;
                        var timestamp = startOfDay / 1000;
                        if (typeof (e.rangeSelectorButton) !== 'undefined')
                        {
                            if (e.rangeSelectorButton.count == 1 && e.rangeSelectorButton.type == 'month') {
                                if ($('#map-container').find('#world-map-container').length > 0) {

                                    filteringMapsAll(baseurl, +timestamp - (2629743), timestamp);
                                }
                                else if ($('#map-container').find('#indo-map').length > 0) {

                                    filteringIndoMapsAll(baseurl, +timestamp - (2629743), timestamp);
                                }
                                filteringTopAttacker(baseurl, +timestamp - (2629743), timestamp);
                            }

                            else if (e.rangeSelectorButton.count == 3 && e.rangeSelectorButton.type == 'month') {
                                if ($('#map-container').find('#world-map-container').length > 0) {

                                    filteringMapsAll(baseurl, +timestamp - (2629743 * 3), timestamp);
                                }
                                else if ($('#map-container').find('#indo-map').length > 0) {

                                    filteringIndoMapsAll(baseurl, +timestamp - (2629743 * 3), timestamp);
                                }
                                filteringTopAttacker(baseurl, +timestamp - (2629743 * 3), timestamp);
                            }

                            else if (e.rangeSelectorButton.count == 6 && e.rangeSelectorButton.type == 'month') {
                                if ($('#map-container').find('#world-map-container').length > 0) {

                                    filteringMapsAll(baseurl, +timestamp - (2629743 * 6), timestamp);
                                }
                                else if ($('#map-container').find('#indo-map').length > 0) {

                                    filteringIndoMapsAll(baseurl, +timestamp - (2629743 * 6), timestamp);
                                }
                                filteringTopAttacker(baseurl, +timestamp - (2629743 * 6), timestamp);
                            }

                            else if (e.rangeSelectorButton.type == 'ytd') {
                                if ($('#map-container').find('#world-map-container').length > 0) {

                                    filteringMapsAll(baseurl, ytd, timestamp);
                                }
                                else if ($('#map-container').find('#indo-map').length > 0) {

                                    filteringIndoMapsAll(baseurl, ytd, timestamp);
                                }
                                filteringTopAttacker(baseurl, ytd, timestamp);
                            }

                            else if (e.rangeSelectorButton.count == 1 && e.rangeSelectorButton.type == 'year') {
                                if ($('#map-container').find('#world-map-container').length > 0) {

                                    filteringMapsAll(baseurl, +timestamp - 31556926, timestamp);
                                }
                                else if ($('#map-container').find('#indo-map').length > 0) {

                                    filteringIndoMapsAll(baseurl, +timestamp - 31556926, timestamp);
                                }
                                filteringTopAttacker(baseurl, +timestamp - 31556926, timestamp);
                            }

                            else if (e.rangeSelectorButton.type == 'all') {
                                if ($('#map-container').find('#world-map-container').length > 0) {
                                    getMapData(baseurl);
                                }
                                else if ($('#map-container').find('#indo-map').length > 0) {
                                    getIndonesiaMapData(baseurl);
                                }
                                getTopAttacker(baseurl);
                            }

                        }
                    }
                }
            },
            yAxis: {
                labels: {
                    style: {
                        color: '#FFF',
                        font: 'Jura, sans-serif'
                    }
                }
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                if ($('#map-container').find('#world-map-container').length > 0) {

                                    filteringMapsAll(baseurl, +this.category / 1000, +this.category / 1000 + 86399);
                                }
                                else if ($('#map-container').find('#indo-map').length > 0) {

                                    filteringIndoMapsAll(baseurl, +this.category / 1000, +this.category / 1000 + 86399);
                                }
                                filteringTopAttacker(baseurl, +this.category / 1000, +this.category / 1000 + 86399);
                            }
                        }
                    }
                }
            },
            series: [{
                    name: 'ATTACK',
                    data: chartDataHighstock,
                    tooltip: {
                        valueDecimals: 0
                    }
                }]
        });
    })


}

function getTopAttacker(baseurl) {
    console.log('Function: getTopAttacker loaded.');

    $.ajax({
        type: "GET",
        url: baseurl + "/country/top-five-country",
        data: "",
        cache: false,
        beforeSend: function () {
            console.log('Load Top Five Country : ');
            $("#top-attacker").css('display', 'none')
            $("#top-attacker-load").html('<center><img src="img/ajax-loader-top-country.gif" /></center>');
            $("#top-attacker-load").css('margin', '10px');
        },
        success: function (msg) {
            console.log('Data Top Five Country : ');
            console.log(msg);
//            $("#attacker-by-country").html('');
//            $("#sumAtt").removeAttr('style');
            $("#top-attacker-load").html('')
            $("#top-attacker-load").removeAttr("style")
            $("#top-attacker").removeAttr("style")
            $('#top-attacker > tbody').html('');
            var d1 = [];
            var counter = 1;
            $.each(msg, function (i, data) {
                $('#top-attacker > tbody:last').append("<tr><td>" + counter + "</td><td><span data-toggle='tooltip' title=\"" + data.countryName + "\" class='flag-icon flag-icon-" + data.countryCode.toLowerCase() + "'></span></td><td class='text'>" + numberWithCommas(data.count) + "</td></tr>");
                counter++;
                d1[i] = {label: data.countrySourceName, data: Math.floor(data.countryCount)};
            });
        }});

}

function getMapData(baseurl) {
    console.log('Function: getMapData called.');
    $('#tanggal-serangan').html('Akumulasi Data');

    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getTotalAttack",
        data: "",
        cache: false,
        beforeSend: function () {
            console.log('API: getTotalAttack called.');
        },
        success: function (msg) {
                console.log(msg);
            $.each(msg, function (i, data) {
                $("#modalJumlahSeranganDunia").html(numberWithCommas(data.count));
            })
        }});

    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getMapData/" + 0 + "/" + 250,
        data: "",
        cache: false,
        beforeSend: function () {
            console.log('API: getMapData called.');
            $("#world-map-container").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>');
        },
        success: function (msg) {
            console.log('test');
            console.log(msg);
            $("#world-map-container").html('');
            $('#world-map-container').vectorMap({
                backgroundColor: 'transparent',
//                zoomButtons: false,
//                zoomOnScroll: false,
                map: 'world_mill_en',
                markerStyle: {
                    initial: {
                        fill: '#FF2310',
                        stroke: '#383f47'
                    }
                },
                focusOn: {
                    x: 0.5,
                    y: 0.5,
                    scale: 0.2
                },
                series: {
                    regions: [{
//                            scale: ['#99B2FF', '#0047B2'],
                            scale: ['#FFFFFF', '#0047B2'],
                            normalizeFunction: 'linear',
                            values: msg,
                            min: 0,
                            max: 1000000,
                            legend: {
                                vertical: true
                            }
                        }, {
                            legend: {
                                horizontal: true,
                                cssClass: 'jvectormap-legend-icons',
                            }
                        }]
                },
//                onMarkerTipShow: function (e, el, code) {
//                    el.html(el.html() + ' (Attack Hit - ' + msg[code] + ')');
//                },
                onRegionTipShow: function (e, el, code) {
                    if (typeof msg[count] === 'undefined') {
                        el.html(el.html() + '<br>Jumlah Serangan: 0)');
                    }
                    else {
                        el.html(el.html() + '<br>Jumlah Serangan: ' + numberWithCommas(msg[count]));
                    }

                },
//                onRegionLabelShow: function (e, el, code) {
//                    el.html(el.html() + ' (Attack Hit - ' + msg[code] + ')');
//                },
                onRegionOver: function (e, code) {
                    $(this).on('click', function () {

                        var map = $('#world-map-container').vectorMap('get', 'mapObject');
                        var regionName = map.getRegionName(code);
                        $("#modalTitle").html(regionName);
                        $("#modalTitleDate").html("Akumulasi Data");
                        if (msg[code] === undefined) {
                            $("#modalJumlahSerangan").html('0');
                        }
                        else {
                            $("#modalJumlahSerangan").html(numberWithCommas(msg[count]));
                        }
                        getTopFiveAttackerIP(baseurl, code)
                        getTopFiveAttackedPort(baseurl, code)
                        getTopFiveMalware(baseurl, code)
                        getTopTarget(baseurl, code)
                        $("#myModal").modal('show');

                    });
                },
                onRegionOut: function (e, code) {
                    $(this).unbind('click');
                },
            });

        }});
}

function getIndonesiaMapData(baseurl) {
    console.log('Function: getIndonesiaMapData called.');

    $('#tanggal-serangan').html('Akumulasi Data');

    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getIndonesiaMapData",
        data: "",
        cache: false,
        beforeSend: function () {
            $("#indo-map").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>');
        },
        success: function (msg) {
            console.log(msg);
            $("#indo-map").html('');
            $('#indo-map').vectorMap({
                backgroundColor: 'transparent',
//                zoomButtons: false,
//                zoomOnScroll: false,
                map: 'id_mill_en',
                focusOn: {
                    x: 0.5,
                    y: 0.5,
                    scale: 0.5
                },
                series: {
                    regions: [{
//                            scale: ['#99B2FF', '#0047B2'],
                            scale: ['#99B2FF', '#0047B2'],
                            normalizeFunction: 'linear',
                            values: msg,
                            min: 0,
                            max: 1000000,
                            legend: {
                                vertical: true
                            }
                        }, {
                            legend: {
                                horizontal: true,
                                cssClass: 'jvectormap-legend-icons',
                                title: 'Business type'
                            }
                        }]
                },
                onRegionLabelShow: function (e, el, code) {
                    el.html(el.html() + ' (Attack Hit - ' + msg[code] + ')');
                },
                onRegionTipShow: function (e, el, code) {
                    if (typeof msg[code] === 'undefined') {
                        el.html(el.html() + '<br>Jumlah Serangan: 0)');
                    }
                    else {
                        el.html(el.html() + '<br>Jumlah Serangan: ' + numberWithCommas(msg[code]));
                    }

                },
                onRegionOver: function (e, code) {
                    $(this).on('click', function () {

                        var map = $('#indo-map').vectorMap('get', 'mapObject');
                        var regionName = map.getRegionName(code);
                        $("#modalTitleIndo").html(regionName);
                        $("#modalTitleDateIndo").html("Total Data");
                        if (msg[code] === undefined) {
                            $("#modalJumlahSeranganIndo").html('0');
                        }
                        else {
                            $("#modalJumlahSeranganIndo").html(numberWithCommas(msg[code]));
                        }
                        getTopFiveAttackerIPProvince(baseurl, code)
                        getTopFiveAttackedPortProvince(baseurl, code)
                        getTopFiveMalwareProvince(baseurl, code)
                        getTopFiveAttackerCountryProvince(baseurl, code)
                        $("#indoModal").modal('show');

                    });
                },
                onRegionOut: function (e, code) {
                    $(this).unbind('click');
                },
            });

        }});
}



function getSensorCount(baseurl) {
    console.log('Function: getSensorCount called.');
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getSensorCount",
        data: "",
        cache: false,
        success: function (msg) {
            $.each(msg, function (i, data) {
                $("#total-sensor").html(data.count);
            })
        }
    })
}

function getTopFiveAttackerIP(baseurl, countryCode) {
    console.log('Function: getTopFiveAttackerIP called.');
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getTopFiveAttackerIP/" + countryCode + "/" + 0 + "/" + 5,
        data: "",
        cache: false,
        beforeSend: function () {
            $("#attacker-ip").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>')
        },
        error: function (e) {
        },
        success: function (msg) {
            if (msg == '') {
                $("#attacker-ip").html("<span class='not-found'>IP Penyerang Tidak Ditemukan</span>");
            }
            else {
                $("#attacker-ip").html('');
                var d1 = [];
                $.each(msg, function (i, data) {
                    d1[i] = {label: data.attackerIP, data: Math.floor(data.count)};

                    //$("#topAttacker ul").append("<li><a href='#'><span class='red'>"+data.a+"</span>"+data.b+"</li>");
                });

                //chartnya
                if ($("#attacker-ip").length)
                {
                    $.plot($("#attacker-ip"), d1,
                            {
                                series: {
                                    pie: {
                                        show: true,
                                    }
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true
                                },
                                legend: {
                                    backgroundColor: "transparent",
                                    show: true
                                }
                            });


                    function pieHover(event, pos, obj)
                    {
                        if (!obj)
                            return;
                        percent = parseFloat(obj.series.percent).toFixed(2);
                        $("#hover").html('<span style="font-weight: bold; color: black">' + obj.series.label + ' (' + percent + '%)</span>');
                    }
                    $("#attacker-ip").bind("plothover", pieHover);
                }
            }
        }});
}

function getTopFiveAttackerIPByTime(baseurl, countryCode, from, till) {
    console.log('Function: getTopFiveAttackerIPByTime called.');
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getTopFiveAttackerIPByTime/" + from + "/" + till + "/" + countryCode + "/" + 0 + "/" + 5,
        data: "",
        cache: false,
        beforeSend: function () {
            $("#attacker-ip").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>')
        },
        error: function (e) {
        },
        success: function (msg) {
            if (msg == '') {
                $("#attacker-ip").html("<span class='not-found'>IP Penyerang Tidak Ditemukan</span>");
            }
            else {
                $("#attacker-ip").html('');
                var d1 = [];
                $.each(msg, function (i, data) {
                    d1[i] = {label: data.attackerIP, data: Math.floor(data.count)};

                    //$("#topAttacker ul").append("<li><a href='#'><span class='red'>"+data.a+"</span>"+data.b+"</li>");
                });

                //chartnya
                if ($("#attacker-ip").length)
                {
                    $.plot($("#attacker-ip"), d1,
                            {
                                series: {
                                    pie: {
                                        show: true,
                                    }
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true
                                },
                                legend: {
                                    backgroundColor: "transparent",
                                    show: true
                                }
                            });


                    function pieHover(event, pos, obj)
                    {
                        if (!obj)
                            return;
                        percent = parseFloat(obj.series.percent).toFixed(2);
                        $("#hover").html('<span style="font-weight: bold; color: black">' + obj.series.label + ' (' + percent + '%)</span>');
                    }
                    $("#attacker-ip").bind("plothover", pieHover);
                }
            }
        }});
}

function getTopFiveAttackedPort(baseurl, countryCode) {
    console.log('Function: getTopFiveAttackedPort called.');
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getTopFiveAttackedPort/" + countryCode + "/" + 0 + "/" + 5,
        data: "",
        cache: false,
        beforeSend: function () {
            $("#attacked-port").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>')
        },
        error: function (e) {
        },
        success: function (msg) {
            if (msg == '') {
                $("#attacked-port").html("<span class='not-found'>Port Tidak Ditemukan</span>")
            }
            else {
                $("#attacked-port").html('');
                var d1 = [];
                $.each(msg, function (i, data) {
                    d1[i] = {label: data.portDestination + " (" + data.service + ")", data: Math.floor(data.countPort)};

                    //$("#topAttacker ul").append("<li><a href='#'><span class='red'>"+data.a+"</span>"+data.b+"</li>");
                });

                //chartnya
                if ($("#attacked-port").length)
                {
                    $.plot($("#attacked-port"), d1,
                            {
                                series: {
                                    pie: {
                                        show: true,
                                    }
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true
                                },
                                legend: {
                                    backgroundColor: "transparent",
                                    show: true
                                }
                            });


                    function pieHover(event, pos, obj)
                    {
                        if (!obj)
                            return;
                        percent = parseFloat(obj.series.percent).toFixed(2);
                        $("#hover").html('<span style="font-weight: bold; color: black">' + obj.series.label + ' (' + percent + '%)</span>');
                    }
                    $("#attacked-port").bind("plothover", pieHover);
                }
            }
        }});
}

function getTopFiveAttackedPortByTime(baseurl, countryCode, from, till) {
    console.log('Function: getTopFiveAttackedPortByTime called.');
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getTopFiveAttackedPortByTime/" + from + "/" + till + "/" + countryCode + "/" + 0 + "/" + 5,
        data: "",
        cache: false,
        beforeSend: function () {
            $("#attacked-port").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>')
        },
        error: function (e) {
        },
        success: function (msg) {
            if (msg == '') {
                $("#attacked-port").html("<span class='not-found'>Port Tidak Ditemukan</span>")
            }
            else {
                $("#attacked-port").html('');
                var d1 = [];
                $.each(msg, function (i, data) {
                    d1[i] = {label: data.portDestination + " (" + data.service + ")", data: Math.floor(data.countPort)};

                    //$("#topAttacker ul").append("<li><a href='#'><span class='red'>"+data.a+"</span>"+data.b+"</li>");
                });

                //chartnya
                if ($("#attacked-port").length)
                {
                    $.plot($("#attacked-port"), d1,
                            {
                                series: {
                                    pie: {
                                        show: true,
                                    }
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true
                                },
                                legend: {
                                    backgroundColor: "transparent",
                                    show: true
                                }
                            });


                    function pieHover(event, pos, obj)
                    {
                        if (!obj)
                            return;
                        percent = parseFloat(obj.series.percent).toFixed(2);
                        $("#hover").html('<span style="font-weight: bold; color: black">' + obj.series.label + ' (' + percent + '%)</span>');
                    }
                    $("#attacked-port").bind("plothover", pieHover);
                }
            }
        }});
}

function getTopFiveMalware(baseurl, countryCode) {
    console.log('Function: getTopFiveMalware called.');
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getTopFiveMalware/" + countryCode + "/" + 0 + "/" + 5,
        data: "",
        cache: false,
        beforeSend: function () {
            $("#malware-by-country").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>')
        },
        error: function (e) {
        },
        success: function (msg) {
            if (msg == '') {
                $("#malware-by-country").html("<span class='not-found'>Malware Tidak Ditemukan</span>");
            }
            else {
                $("#malware-by-country").html('');
                var d1 = [];
                $.each(msg, function (i, data) {
                    d1[i] = {label: data.vitolResult, data: Math.floor(data.count)};

                    //$("#topAttacker ul").append("<li><a href='#'><span class='red'>"+data.a+"</span>"+data.b+"</li>");
                });

                //chartnya
                if ($("#malware-by-country").length)
                {
                    $.plot($("#malware-by-country"), d1,
                            {
                                series: {
                                    pie: {
                                        show: true,
                                    }
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true
                                },
                                legend: {
                                    backgroundColor: "transparent",
                                    show: true
                                }
                            });


                    function pieHover(event, pos, obj)
                    {
                        if (!obj)
                            return;
                        percent = parseFloat(obj.series.percent).toFixed(2);
                        $("#hover").html('<span style="font-weight: bold; color: black">' + obj.series.label + ' (' + percent + '%)</span>');
                    }
                    $("#malware-by-country").bind("plothover", pieHover);
                }
            }
        }});
}

function getTopFiveMalwareByTime(baseurl, countryCode, from, till) {
    console.log('Function: getTopFiveMalwareByTime called.');
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getTopFiveMalwareByTime/" + countryCode + "/" + from + "/" + till + "/" + 0 + "/" + 5,
        data: "",
        cache: false,
        beforeSend: function () {
            $("#malware-by-country").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>')
        },
        error: function (e) {
        },
        success: function (msg) {
            if (msg == '') {
                $("#malware-by-country").html("<span class='not-found'>Malware Tidak Ditemukan</span>");
            }
            else {
                $("#malware-by-country").html('');
                var d1 = [];
                $.each(msg, function (i, data) {
                    d1[i] = {label: data.vitolResult, data: Math.floor(data.count)};

                    //$("#topAttacker ul").append("<li><a href='#'><span class='red'>"+data.a+"</span>"+data.b+"</li>");
                });

                //chartnya
                if ($("#malware-by-country").length)
                {
                    $.plot($("#malware-by-country"), d1,
                            {
                                series: {
                                    pie: {
                                        show: true,
                                    }
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true
                                },
                                legend: {
                                    backgroundColor: "transparent",
                                    show: true
                                }
                            });


                    function pieHover(event, pos, obj)
                    {
                        if (!obj)
                            return;
                        percent = parseFloat(obj.series.percent).toFixed(2);
                        $("#hover").html('<span style="font-weight: bold; color: black">' + obj.series.label + ' (' + percent + '%)</span>');
                    }
                    $("#malware-by-country").bind("plothover", pieHover);
                }
            }
        }});
}

function getTopTarget(baseurl, countryCode) {
    console.log('Function: getTopTarget called.');
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getTopAttackerProvincePerCountry/" + countryCode + "/" + 5 + "/" + 0,
        data: "",
        cache: false,
        beforeSend: function () {
            $("#top-target").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>')
        },
        error: function (e) {
        },
        success: function (msg) {
            if (msg == '') {
                $("#top-target").html("<span class='not-found'>Target Provinsi Tidak Ditemukan</span>");
            }
            else {
                $("#top-target").html('');
                var d1 = [];
                $.each(msg, function (i, data) {
                    d1[i] = {label: data.province_name, data: Math.floor(data.count)};

                    //$("#topAttacker ul").append("<li><a href='#'><span class='red'>"+data.a+"</span>"+data.b+"</li>");
                });

                //chartnya
                if ($("#top-target").length)
                {
                    $.plot($("#top-target"), d1,
                            {
                                series: {
                                    pie: {
                                        show: true,
                                    }
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true
                                },
                                legend: {
                                    backgroundColor: "transparent",
                                    show: true
                                }
                            });


                    function pieHover(event, pos, obj)
                    {
                        if (!obj)
                            return;
                        percent = parseFloat(obj.series.percent).toFixed(2);
                        $("#hover").html('<span style="font-weight: bold; color: black">' + obj.series.label + ' (' + percent + '%)</span>');
                    }
                    $("#top-target").bind("plothover", pieHover);
                }
            }
        }});
}

function getTopTargetByTime(baseurl, countryCode, from, till) {
    console.log('Function: getTopTargetByTime called.');
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getTopAttackerProvincePerCountryByTime/" + countryCode + "/" + from + "/" + till + "/" + 5 + "/" + 0,
        data: "",
        cache: false,
        beforeSend: function () {
            $("#top-target").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>')
        },
        error: function (e) {
        },
        success: function (msg) {
            if (msg == '') {
                $("#top-target").html("<span class='not-found'>Target Provinsi Tidak Ditemukan</span>");
            }
            else {
                $("#top-target").html('');
                var d1 = [];
                $.each(msg, function (i, data) {
                    d1[i] = {label: data.province_name, data: Math.floor(data.count)};

                    //$("#topAttacker ul").append("<li><a href='#'><span class='red'>"+data.a+"</span>"+data.b+"</li>");
                });

                //chartnya
                if ($("#top-target").length)
                {
                    $.plot($("#top-target"), d1,
                            {
                                series: {
                                    pie: {
                                        show: true,
                                    }
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true
                                },
                                legend: {
                                    backgroundColor: "transparent",
                                    show: true
                                }
                            });


                    function pieHover(event, pos, obj)
                    {
                        if (!obj)
                            return;
                        percent = parseFloat(obj.series.percent).toFixed(2);
                        $("#hover").html('<span style="font-weight: bold; color: black">' + obj.series.label + ' (' + percent + '%)</span>');
                    }
                    $("#top-target").bind("plothover", pieHover);
                }
            }
        }});
}

function redrawMap(baseurl, msg, firstDayEpoch, lastDayEpoch) {
    var array = {};
    $.each(msg, function (i, data) {
        var code = data.countryCode;
        var count = data.count;
        array[code] = count;
    });
    var jsonMAP = JSON.stringify(array);
    var jsonOBJ = JSON.parse(jsonMAP);
    console.log('value');
    console.log(jsonOBJ);
    $('#world-map-container').vectorMap({
        backgroundColor: 'transparent',
//        zoomButtons: false,
//        zoomOnScroll: false,
        map: 'world_mill_en',
        markerStyle: {
            initial: {
                fill: '#FF0000',
                stroke: '#383f47'
            }
        },
        focusOn: {
            x: 0.5,
            y: 0.5,
            scale: 0.5
        },
        series: {
            regions: [{
                    scale: ['#ffffff','#f1c40f','#c0392b'],
                    normalizeFunction: 'linear',
                    values: jsonOBJ,
                    min: 0,
                    legend: {
                        vertical: true
                    }
                }, {
                    legend: {
                        horizontal: true,
                        cssClass: 'jvectormap-legend-icons',
                        title: 'Business type'
                    }
                }]
        },
        onRegionLabelShow: function (e, el, code) {
            el.html(el.html() + ' (Attack Hit - ' + msg[code] + ')');
        },
        onRegionTipShow: function (e, el, code) {
            if (typeof jsonOBJ[code] === 'undefined') {
                el.html(el.html() + '<br>Jumlah Serangan: 0');
            }
            else {
                el.html(el.html() + '<br>Jumlah Serangan: ' + numberWithCommas(jsonOBJ[code]));
            }

        },
        onRegionOver: function (e, code) {
            $(this).on('click', function () {
                var selectedDate = new Date(firstDayEpoch * 1000);
                var day = selectedDate.getDate();
                var month = selectedDate.getMonth() + 1;
                var year = selectedDate.getFullYear();

                var lastDate = new Date(lastDayEpoch * 1000);
                var fDay = lastDate.getDate();
                var fMonth = lastDate.getMonth() + 1;
                var fYear = lastDate.getFullYear();

                var map = $('#world-map-container').vectorMap('get', 'mapObject');
                var regionName = map.getRegionName(code);
                $("#modalTitle").html(regionName);
                $("#modalTitleDate").html(checkTime(day) + "-" + checkTime(month) + "-" + year + " s/d " + checkTime(fDay) + "-" + checkTime(fMonth) + "-" + checkTime(fYear));
                if (jsonOBJ[code] === undefined) {
                    $("#modalJumlahSerangan").html('0');
                }
                else {
                    $("#modalJumlahSerangan").html(jsonOBJ[code]);
                }
                getTopFiveAttackerIPByTime(baseurl, code, firstDayEpoch, lastDayEpoch)
                getTopFiveAttackedPortByTime(baseurl, code, firstDayEpoch, lastDayEpoch)
                getTopFiveMalwareByTime(baseurl, code, firstDayEpoch, lastDayEpoch)
                getTopTargetByTime(baseurl, code, firstDayEpoch, lastDayEpoch)
                $("#myModal").modal('show');

            });
        },
        onRegionOut: function (e, code) {
            $(this).unbind('click');
        },
    });
}

function redrawIndoMap(baseurl, msg, firstDayEpoch, lastDayEpoch) {
    console.log('Function: redrawIndoMap called.');
    var array = {};
    $.each(msg, function (i, data) {
        var code = data.province_iso_id;
        var count = data.count;
        array[code] = count;
    });
    var jsonMAP = JSON.stringify(array);
    var jsonOBJ = JSON.parse(jsonMAP);
    console.log(jsonOBJ);
    $('#indo-map').vectorMap({
        backgroundColor: 'transparent',
//        zoomButtons: false,
//        zoomOnScroll: false,
        map: 'id_mill_en',
        focusOn: {
            x: 0.5,
            y: 0.5,
            scale: 0.5
        },
        series: {
            regions: [{
                    scale: ['#ffffff','#f1c40f', '#c0392b'],
                    // scale: ['#FFFFFF', '#0047B2'],
                    normalizeFunction: 'linear',
                    values: jsonOBJ,
                    min: 0,
                    legend: {
                        vertical: true
                    }
                }, {
                    legend: {
                        horizontal: true,
                        cssClass: 'jvectormap-legend-icons',
                        title: 'Business type'
                    }
                }]
        },
        onRegionLabelShow: function (e, el, code) {
            el.html(el.html() + ' (Attack Hit - ' + msg[code] + ')');
        },
        onRegionTipShow: function (e, el, code) {
            if (typeof jsonOBJ[code] === 'undefined') {
                el.html(el.html() + '<br>Jumlah Serangan: 0');
            }
            else {
                el.html(el.html() + '<br>Jumlah Serangan: ' + numberWithCommas(jsonOBJ[code]));
            }

        },
        onRegionOver: function (e, code) {
            $(this).on('click', function () {
                var selectedDate = new Date(firstDayEpoch * 1000);
                var day = selectedDate.getDate();
                var month = selectedDate.getMonth() + 1;
                var year = selectedDate.getFullYear();

                var lastDate = new Date(lastDayEpoch * 1000);
                var fDay = lastDate.getDate();
                var fMonth = lastDate.getMonth() + 1;
                var fYear = lastDate.getFullYear();

                var map = $('#indo-map').vectorMap('get', 'mapObject');
                var regionName = map.getRegionName(code);
                $("#modalTitleIndo").html(regionName);
                $("#modalTitleDateIndo").html(checkTime(day) + "-" + checkTime(month) + "-" + year + " s/d " + checkTime(fDay) + "-" + checkTime(fMonth) + "-" + checkTime(fYear));
                if (jsonOBJ[code] === undefined) {
                    $("#modalJumlahSeranganIndo").html('0');
                }
                else {
                    $("#modalJumlahSeranganIndo").html(jsonOBJ[code]);
                }
                getTopFiveAttackerIPProvinceByTime(baseurl, code, firstDayEpoch, lastDayEpoch)
                getTopFiveAttackedPortProvinceByTime(baseurl, code, firstDayEpoch, lastDayEpoch)
                getTopFiveMalwareProvinceByTime(baseurl, code, firstDayEpoch, lastDayEpoch)
                getTopFiveAttackerCountryProvinceByTime(baseurl, code, firstDayEpoch, lastDayEpoch)
                $("#indoModal").modal('show');

            });
        },
        onRegionOut: function (e, code) {
            $(this).unbind('click');
        },
    });

}

function filteringMapsAll(baseurl, firstDayEpoch, lastDayEpoch) {
    console.log('Function: filteringMapsAll called.');
    var monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
        "Juli", "Agustus", "September", "Oktober", "November", "Desember"
    ];

    $.ajax({
        type: "GET",
        dataType: "json",
        url: baseurl + "/country/getDataByTime/" + firstDayEpoch + "/" + lastDayEpoch,
        beforeSend: function () {
//            $("#topAttacker").html('<center><img src="public/assets/img/ajax-loader-1.gif" /></center>');
            $("#world-map-container").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>');
//            $("#sumAtt").find('tbody').html('<center><img src="public/assets/img/ajax-loader-1.gif" /></center>');
        },
        cache: false,
        error: function (response) {
            console.log('Error: API: filteringMapsAll');
            console.log(response);
        },
        success: function (msg) {
            console.log('API: firstDayEpoch called.');
            console.log(msg.data);

            var selectedDate = new Date(firstDayEpoch * 1000);
            var day = selectedDate.getDate();
            var month = monthNames[selectedDate.getMonth()];
            var year = selectedDate.getFullYear();

            var finalDate = new Date(lastDayEpoch * 1000);
            var fDay = finalDate.getDate();
            var fMonth = monthNames[finalDate.getMonth()];
            var fYear = finalDate.getFullYear();
            $("#tanggal-serangan").html('<span id="firstDateSub">' + day + " " + month + " " + year + '</span>' + " s/d " + '<span id="lastDateSub">' + fDay + " " + fMonth + " " + fYear + '</span>');
            $(".jvectormap-tip").remove();
            $("#world-map-container").html('');
            redrawMap(baseurl, msg.data, firstDayEpoch, lastDayEpoch);
        }});
}

function filteringIndoMapsAll(baseurl, firstDayEpoch, lastDayEpoch) {
    console.log('Function: filteringIndoMapsAll called.');
    var monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
        "Juli", "Agustus", "September", "Oktober", "November", "Desember"
    ];

    $.ajax({
        type: "GET",
        dataType: "json",
        url: baseurl + "/getProvinceDataByTime/" + firstDayEpoch + "/" + lastDayEpoch,
        beforeSend: function () {
//            $("#topAttacker").html('<center><img src="public/assets/img/ajax-loader-1.gif" /></center>');
            $("#indo-map").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>');
//            $("#sumAtt").find('tbody').html('<center><img src="public/assets/img/ajax-loader-1.gif" /></center>');
        },
        cache: false,
        error: function (response) {
            console.log(response);
        },
        success: function (msg) {

            var selectedDate = new Date(firstDayEpoch * 1000);
            var day = selectedDate.getDate();
            var month = monthNames[selectedDate.getMonth()];
            var year = selectedDate.getFullYear();

            var finalDate = new Date(lastDayEpoch * 1000);
            var fDay = finalDate.getDate();
            var fMonth = monthNames[finalDate.getMonth()];
            var fYear = finalDate.getFullYear();
            $("#tanggal-serangan").html('<span id="firstDateSub">' + day + " " + month + " " + year + '</span>' + " s/d " + '<span id="lastDateSub">' + fDay + " " + fMonth + " " + fYear + '</span>');
            $(".jvectormap-tip").remove();
            $("#indo-map").html('');
            redrawIndoMap(baseurl, msg, firstDayEpoch, lastDayEpoch);
        }});
}

function filteringTopAttacker(baseurl, firstDayEpoch, lastDayEpoch) {
    console.log('Function: filteringTopAttacker called.');
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/country/getAttackerByCountryByTime/" + firstDayEpoch + "/" + lastDayEpoch + "/" + 0 + "/" + 5,
        data: "",
        cache: false,
        beforeSend: function () {
            $("#top-attacker").css('display', 'none')
            $("#top-attacker-load").html('<center><img src="img/ajax-loader-top-country.gif" /></center>');
            $("#top-attacker-load").css('margin', '10px');
        },
        success: function (msg) {
            console.log('Data dari API getAttackerByCountryByTime.');
            console.log(msg);
//            $("#attacker-by-country").html('');
//            $("#sumAtt").removeAttr('style');
            $('#top-attacker > tbody').html('');

            redrawTopAttacker(msg.data);
        }});
}

function redrawTopAttacker(msg) {
    console.log('Function: redrawTopAttacker called.');
    $("#top-attacker-load").html('')
    $("#top-attacker-load").removeAttr("style")
    $("#top-attacker").removeAttr("style")
    var d1 = [];
    var counter = 1;
    $.each(msg, function (i, data) {
        $('#top-attacker > tbody:last').append("<tr><td>" + counter + "</td><td><span data-toggle='tooltip' title=\"" + data.countryName + "\" class='flag-icon flag-icon-" + data.countryCode.toLowerCase() + "'></span></td><td class='text'>" + numberWithCommas(data.count) + "</td></tr>");
        counter++;
        d1[i] = {label: data.countryName, data: Math.floor(data.count)};
    });
}

function getTopFiveMalwareProvince(baseurl, provinceIso) {
    console.log('Function: getTopFiveMalwareProvince called.');
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getMalwareProvince/" + provinceIso + "/AhnLab-V3/" + 5 + "/" + 0,
        data: "",
        cache: false,
        beforeSend: function () {
            $("#malware-by-country-indo").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>')
        },
        error: function (e) {
        },
        success: function (msg) {
            if (msg == '') {
                $("#malware-by-country-indo").html("<span class='not-found'>Malware Tidak Ditemukan</span>");
            }
            else {
                $("#malware-by-country-indo").html('');
                var d1 = [];
                $.each(msg, function (i, data) {
                    d1[i] = {label: data.malware_name, data: Math.floor(data.count)};

                    //$("#topAttacker ul").append("<li><a href='#'><span class='red'>"+data.a+"</span>"+data.b+"</li>");
                });

                //chartnya
                if ($("#malware-by-country-indo").length)
                {
                    $.plot($("#malware-by-country-indo"), d1,
                            {
                                series: {
                                    pie: {
                                        show: true,
                                    }
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true
                                },
                                legend: {
                                    backgroundColor: "transparent",
                                    show: true
                                }
                            });


                    function pieHover(event, pos, obj)
                    {
                        if (!obj)
                            return;
                        percent = parseFloat(obj.series.percent).toFixed(2);
                        $("#hover").html('<span style="font-weight: bold; color: black">' + obj.series.label + ' (' + percent + '%)</span>');
                    }
                    $("#malware-by-country-indo").bind("plothover", pieHover);
                }
            }
        }});
}

function getTopFiveMalwareProvinceByTime(baseurl, provinceIso, from, till) {
    console.log('Function: getTopFiveMalwareProvinceByTime called.');
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getMalwareProvinceByTime/" + provinceIso + "/AhnLab-V3/" + from + "/" + till + "/" + 5 + "/" + 0,
        data: "",
        cache: false,
        beforeSend: function () {
            $("#malware-by-country-indo").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>')
        },
        error: function (e) {
        },
        success: function (msg) {
            if (msg == '') {
                $("#malware-by-country-indo").html("<span class='not-found'>Malware Tidak Ditemukan</span>");
            }
            else {
                $("#malware-by-country-indo").html('');
                var d1 = [];
                $.each(msg, function (i, data) {
                    d1[i] = {label: data.malware_name, data: Math.floor(data.count)};

                    //$("#topAttacker ul").append("<li><a href='#'><span class='red'>"+data.a+"</span>"+data.b+"</li>");
                });

                //chartnya
                if ($("#malware-by-country-indo").length)
                {
                    $.plot($("#malware-by-country-indo"), d1,
                            {
                                series: {
                                    pie: {
                                        show: true,
                                    }
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true
                                },
                                legend: {
                                    backgroundColor: "transparent",
                                    show: true
                                }
                            });


                    function pieHover(event, pos, obj)
                    {
                        if (!obj)
                            return;
                        percent = parseFloat(obj.series.percent).toFixed(2);
                        $("#hover").html('<span style="font-weight: bold; color: black">' + obj.series.label + ' (' + percent + '%)</span>');
                    }
                    $("#malware-by-country-indo").bind("plothover", pieHover);
                }
            }
        }});
}

function getTopFiveAttackedPortProvince(baseurl, provinceIso) {
    console.log('Function: getTopFiveAttackedPortProvince called.');
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getPortAttackedProvince/" + provinceIso + "/" + 5 + "/" + 0,
        data: "",
        cache: false,
        beforeSend: function () {
            $("#attacked-port-indo").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>')
        },
        error: function (e) {
        },
        success: function (msg) {
            if (msg == '') {
                $("#attacked-port-indo").html("<span class='not-found'>Port Tidak Ditemukan</span>")
            }
            else {
                $("#attacked-port-indo").html('');
                var d1 = [];
                $.each(msg, function (i, data) {
                    d1[i] = {label: data.port + " (" + data.service_name + ")", data: Math.floor(data.count)};

                    //$("#topAttacker ul").append("<li><a href='#'><span class='red'>"+data.a+"</span>"+data.b+"</li>");
                });

                //chartnya
                if ($("#attacked-port-indo").length)
                {
                    $.plot($("#attacked-port-indo"), d1,
                            {
                                series: {
                                    pie: {
                                        show: true,
                                    }
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true
                                },
                                legend: {
                                    backgroundColor: "transparent",
                                    show: true
                                }
                            });


                    function pieHover(event, pos, obj)
                    {
                        if (!obj)
                            return;
                        percent = parseFloat(obj.series.percent).toFixed(2);
                        $("#hover").html('<span style="font-weight: bold; color: black">' + obj.series.label + ' (' + percent + '%)</span>');
                    }
                    $("#attacked-port-indo").bind("plothover", pieHover);
                }
            }
        }});
}

function getTopFiveAttackedPortProvinceByTime(baseurl, provinceIso, from, till) {
    console.log('Function: getTopFiveAttackedPortProvinceByTime called.');
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getPortAttackedProvinceByTime/" + provinceIso + "/" + from + "/" + till + "/" + 5 + "/" + 0,
        data: "",
        cache: false,
        beforeSend: function () {
            $("#attacked-port-indo").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>')
        },
        error: function (e) {
        },
        success: function (msg) {
            if (msg == '') {
                $("#attacked-port-indo").html("<span class='not-found'>Port Tidak Ditemukan</span>")
            }
            else {
                $("#attacked-port-indo").html('');
                var d1 = [];
                $.each(msg, function (i, data) {
                    d1[i] = {label: data.port + " (" + data.service_name + ")", data: Math.floor(data.count)};

                    //$("#topAttacker ul").append("<li><a href='#'><span class='red'>"+data.a+"</span>"+data.b+"</li>");
                });

                //chartnya
                if ($("#attacked-port-indo").length)
                {
                    $.plot($("#attacked-port-indo"), d1,
                            {
                                series: {
                                    pie: {
                                        show: true,
                                    }
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true
                                },
                                legend: {
                                    backgroundColor: "transparent",
                                    show: true
                                }
                            });


                    function pieHover(event, pos, obj)
                    {
                        if (!obj)
                            return;
                        percent = parseFloat(obj.series.percent).toFixed(2);
                        $("#hover").html('<span style="font-weight: bold; color: black">' + obj.series.label + ' (' + percent + '%)</span>');
                    }
                    $("#attacked-port-indo").bind("plothover", pieHover);
                }
            }
        }});
}

function getTopFiveAttackerCountryProvince(baseurl, provinceIso) {
    console.log('Function: getTopFiveAttackerCountryProvince called.');
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getCountryProvince/" + provinceIso + "/" + 5 + "/" + 0,
        data: "",
        cache: false,
        beforeSend: function () {
            $("#top-country-attacker").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>')
        },
        error: function (e) {
        },
        success: function (msg) {
            if (msg == '') {
                $("#top-country-attacker").html("<span class='not-found'>Negara Penyerang Tidak Ditemukan</span>")
            }
            else {
                $("#top-country-attacker").html('');
                var d1 = [];
                $.each(msg, function (i, data) {
                    d1[i] = {label: data.country_name, data: Math.floor(data.count)};

                    //$("#topAttacker ul").append("<li><a href='#'><span class='red'>"+data.a+"</span>"+data.b+"</li>");
                });

                //chartnya
                if ($("#top-country-attacker").length)
                {
                    $.plot($("#top-country-attacker"), d1,
                            {
                                series: {
                                    pie: {
                                        show: true,
                                    }
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true
                                },
                                legend: {
                                    backgroundColor: "transparent",
                                    show: true
                                }
                            });


                    function pieHover(event, pos, obj)
                    {
                        if (!obj)
                            return;
                        percent = parseFloat(obj.series.percent).toFixed(2);
                        $("#hover").html('<span style="font-weight: bold; color: black">' + obj.series.label + ' (' + percent + '%)</span>');
                    }
                    $("#top-country-attacker").bind("plothover", pieHover);
                }
            }
        }});
}

function getTopFiveAttackerCountryProvinceByTime(baseurl, provinceIso, from, till) {
    console.log('Function: getTopFiveAttackerCountryProvinceByTime called.');
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getCountryProvinceByTime/" + provinceIso + "/" + from + "/" + till + "/" + 5 + "/" + 0,
        data: "",
        cache: false,
        beforeSend: function () {
            $("#top-country-attacker").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>')
        },
        error: function (e) {
        },
        success: function (msg) {
            if (msg == '') {
                $("#top-country-attacker").html("<span class='not-found'>Negara Penyerang Tidak Ditemukan</span>")
            }
            else {
                $("#top-country-attacker").html('');
                var d1 = [];
                $.each(msg, function (i, data) {
                    d1[i] = {label: data.country_name, data: Math.floor(data.count)};

                    //$("#topAttacker ul").append("<li><a href='#'><span class='red'>"+data.a+"</span>"+data.b+"</li>");
                });

                //chartnya
                if ($("#top-country-attacker").length)
                {
                    $.plot($("#top-country-attacker"), d1,
                            {
                                series: {
                                    pie: {
                                        show: true,
                                    }
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true
                                },
                                legend: {
                                    backgroundColor: "transparent",
                                    show: true
                                }
                            });


                    function pieHover(event, pos, obj)
                    {
                        if (!obj)
                            return;
                        percent = parseFloat(obj.series.percent).toFixed(2);
                        $("#hover").html('<span style="font-weight: bold; color: black">' + obj.series.label + ' (' + percent + '%)</span>');
                    }
                    $("#top-country-attacker").bind("plothover", pieHover);
                }
            }
        }});
}

function getTopFiveAttackerIPProvince(baseurl, provinceIso) {
    console.log('Function: getTopFiveAttackerIPProvince called.');
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getIpAttackerProvince/" + provinceIso + "/" + 5 + "/" + 0,
        data: "",
        cache: false,
        beforeSend: function () {
            $("#attacker-ip-indo").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>')
        },
        error: function (e) {
        },
        success: function (msg) {
            if (msg == '') {
                $("#attacker-ip-indo").html("<span class='not-found'>IP Penyerang Tidak Ditemukan</span>");
            }
            else {
                $("#attacker-ip-indo").html('');
                var d1 = [];
                $.each(msg, function (i, data) {
                    d1[i] = {label: data.ipaddress, data: Math.floor(data.count)};

                    //$("#topAttacker ul").append("<li><a href='#'><span class='red'>"+data.a+"</span>"+data.b+"</li>");
                });

                //chartnya
                if ($("#attacker-ip-indo").length)
                {
                    $.plot($("#attacker-ip-indo"), d1,
                            {
                                series: {
                                    pie: {
                                        show: true,
                                    }
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true
                                },
                                legend: {
                                    backgroundColor: "transparent",
                                    show: true
                                }
                            });


                    function pieHover(event, pos, obj)
                    {
                        if (!obj)
                            return;
                        percent = parseFloat(obj.series.percent).toFixed(2);
                        $("#hover").html('<span style="font-weight: bold; color: black">' + obj.series.label + ' (' + percent + '%)</span>');
                    }
                    $("#attacker-ip-indo").bind("plothover", pieHover);
                }
            }
        }});
}

function getTopFiveAttackerIPProvinceByTime(baseurl, provinceIso, from, till) {
    console.log('Function: getTopFiveAttackerIPProvinceByTime called.');
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: baseurl + "/getIpAttackerProvinceByTime/" + provinceIso + "/" + from + "/" + till + "/" + 5 + "/" + 0,
        data: "",
        cache: false,
        beforeSend: function () {
            $("#attacker-ip-indo").html('<center class="loading-ajax"><img src="img/ajax-loader.gif" /></center>')
        },
        error: function (e) {
        },
        success: function (msg) {
            if (msg == '') {
                $("#attacker-ip-indo").html("<span class='not-found'>IP Penyerang Tidak Ditemukan</span>");
            }
            else {
                $("#attacker-ip-indo").html('');
                var d1 = [];
                $.each(msg, function (i, data) {
                    d1[i] = {label: data.ipaddress, data: Math.floor(data.count)};

                    //$("#topAttacker ul").append("<li><a href='#'><span class='red'>"+data.a+"</span>"+data.b+"</li>");
                });

                //chartnya
                if ($("#attacker-ip-indo").length)
                {
                    $.plot($("#attacker-ip-indo"), d1,
                            {
                                series: {
                                    pie: {
                                        show: true,
                                    }
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true
                                },
                                legend: {
                                    backgroundColor: "transparent",
                                    show: true
                                }
                            });


                    function pieHover(event, pos, obj)
                    {
                        if (!obj)
                            return;
                        percent = parseFloat(obj.series.percent).toFixed(2);
                        $("#hover").html('<span style="font-weight: bold; color: black">' + obj.series.label + ' (' + percent + '%)</span>');
                    }
                    $("#attacker-ip-indo").bind("plothover", pieHover);
                }
            }
        }});
}