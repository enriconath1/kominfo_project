

$(document).ready(function () {

    $("#new_kategori").prop("disabled", true);
    $("#kategori_id").prop("disabled", false);
    $("#kategori_id").show();
    $("#new_kategori").hide();

    $("#institusi_id").prop("disabled", false);
    $("#institusi_id").show();

    $("#new_institusi").hide();

    $("#nama_institusi").prop("disabled", true);
    $("#alamat_institusi").prop("disabled", true);
    $("#provinsi").prop("disabled", true);


    $("#nama_user1").prop("disabled", true);
    $("#nomor_telp_user1").prop("disabled", true);
    $("#alamat_user1").prop("disabled", true);
    $("#user_institusi1").prop("disabled", true);
    $("#paran_user_institusi1").prop("disabled", true);
    $("#password1").prop("disabled", true);

    $("#nama_user2").prop("disabled", true);
    $("#nomor_telp_user2").prop("disabled", true);
    $("#alamat_user2").prop("disabled", true);
    $("#user_institusi2").prop("disabled", true);
    $("#paran_user_institusi2").prop("disabled", true);
    $("#password2").prop("disabled", true);


    $("input[name=is_category_registered]").on('change',function () {
        var value = $(this).val();
        if (value == "true") {
            $("#kategori_id").prop("disabled", false);
            $("#kategori_id").show();
            $("#new_kategori").prop("disabled", true);
            $("#new_kategori").hide();
        }
        else if (value == "false") {
            $("#new_kategori").prop("disabled", false);
            $("#kategori_id").prop("disabled", true);
            $("#kategori_id").hide();
            $("#new_kategori").show();
        }
    })
    $("input[name=is_institute_registered]").on('change',function () {
        var value = $(this).val();
        if (value == "true") {
            $("#institusi_id").prop("disabled", false);
            $("#institusi_id").show();

            $("#new_institusi").hide();

            $("#nama_institusi").prop("disabled", true);
            $("#alamat_institusi").prop("disabled", true);
            $("#provinsi").prop("disabled", true);
        }
        else if (value == "false") {
            $("#institusi_id").prop("disabled", true);
            $("#institusi_id").hide();

            $("#new_institusi").show();

            $("#nama_institusi").prop("disabled", false);
            $("#alamat_institusi").prop("disabled", false);
            $("#provinsi").prop("disabled", false);
        }
    })

    $("input[name=is_user_registered]").on('change',function () {
        var value = $(this).val();
        if (value == "true") {
            $("#user_id1").prop("disabled", false);
            $("#user_id2").prop("disabled", false);
            $("#user_id1").show();
            $("#user_id2").show();

            $("#new_user").hide();

            $("#nama_user1").prop("disabled", true);
            $("#nomor_telp_user1").prop("disabled", true);
            $("#alamat_user1").prop("disabled", true);
            $("#user_institusi1").prop("disabled", true);
            $("#paran_user_institusi1").prop("disabled", true);
            $("#password1").prop("disabled", true);

            $("#nama_user2").prop("disabled", true);
            $("#nomor_telp_user2").prop("disabled", true);
            $("#alamat_user2").prop("disabled", true);
            $("#user_institusi2").prop("disabled", true);
            $("#paran_user_institusi2").prop("disabled", true);
            $("#password2").prop("disabled", true);
        }
        else if (value == "false") {
            $("#user_id1").prop("disabled", true);
            $("#user_id2").prop("disabled", true);
            $("#user_id1").hide();
            $("#user_id2").hide();

             $("#new_user").show();

            $("#nama_user1").prop("disabled", false);
            $("#nomor_telp_user1").prop("disabled", false);
            $("#alamat_user1").prop("disabled", false);
            $("#user_institusi1").prop("disabled", false);
            $("#paran_user_institusi1").prop("disabled", false);
            $("#password1").prop("disabled", false);

            $("#nama_user2").prop("disabled", false);
            $("#nomor_telp_user2").prop("disabled", false);
            $("#alamat_user2").prop("disabled", false);
            $("#user_institusi2").prop("disabled", false);
            $("#paran_user_institusi2").prop("disabled", false);
            $("#password2").prop("disabled", false);
        }
    })

	var currentTab = 0; // Current tab is set to be the first tab (0)
	$('#prevBtn').on('click',function(){
		nextPrev(-1);
	});
	$('#nextBtn').on('click',function(){
		nextPrev(1);
	});
	showTab(currentTab); // Display the current tab

	function showTab(n) {
	  // This function will display the specified tab of the form ...
	  var x = document.getElementsByClassName("tab");
	  x[n].style.display = "block";
	  // ... and fix the Previous/Next buttons:
	  if (n == 0) {
	    document.getElementById("prevBtn").style.display = "none";
	  } else {
	    document.getElementById("prevBtn").style.display = "inline";
	  }
	  if (n == (x.length - 1)) {
	    document.getElementById("nextBtn").innerHTML = "Submit";
	  } else {
	    document.getElementById("nextBtn").innerHTML = "Next";
	  }
	  // ... and run a function that displays the correct step indicator:
	  fixStepIndicator(n)
	}

	function nextPrev(n) {
	  // This function will figure out which tab to display
	  var x = document.getElementsByClassName("tab");
	  // Exit the function if any field in the current tab is invalid:
	  if (n == 1 && !validateForm()) return false;
	  // Hide the current tab:
	  x[currentTab].style.display = "none";
	  // Increase or decrease the current tab by 1:
	  currentTab = currentTab + n;
	  // if you have reached the end of the form... :
	  if (currentTab >= x.length) {
	    //...the form gets submitted:
	    document.getElementById("regForm").submit();
	    return false;
	  }
	  // Otherwise, display the correct tab:
	  showTab(currentTab);
	}

	function validateForm() {
	  // This function deals with validation of the form fields
	  var x, y, i, valid = true;
	  x = document.getElementsByClassName("tab");
	  y = x[currentTab].getElementsByTagName("input");
	  // A loop that checks every input field in the current tab:
	  for (i = 0; i < y.length; i++) {
	  	if(!y[i].disabled){
		    // If a field is empty...
		    if (y[i].value == "") {
		      // add an "invalid" class to the field:
		      y[i].className += " invalid";
		      // and set the current valid status to false:
		      valid = false;
		    }
	  	}
	  }
	  // If the valid status is true, mark the step as finished and valid:
	  if (valid) {
	    document.getElementsByClassName("step")[currentTab].className += " finish";
	  }
	  return valid; // return the valid status
	}

	function fixStepIndicator(n) {
	  // This function removes the "active" class of all steps...
	  var i, x = document.getElementsByClassName("step");
	  for (i = 0; i < x.length; i++) {
	    x[i].className = x[i].className.replace(" active", "");
	  }
	  //... and adds the "active" class to the current step:
	  x[n].className += " active";
	}
})