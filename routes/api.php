<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([], function () {
// API CONTROLLER


    // Widget

    Route::get('widget/total-attack', 'ApiController@getTotalAttack')->name('api.widget.totalAttack');

    Route::get('widget/total-country', 'ApiController@getTotalCountry')->name('api.widget.totalCountry');

    Route::get('widget/total-unique-attack','ApiController@getUniqueAttack')->name('api.widget.uniqueAttacker');

    Route::get('widget/total-unique-attack', 'ApiController@getUniqueAttack')->name('api.widget.uniqueAttacker');

    // End Widget


    Route::get('province-list', 'ApiController@provinceList');

    // CountryController

    Route::get('country/country-list', 'API\CountryController@countryList')->name('api.country.country-list');

    Route::get('country/top-country', 'API\CountryController@topCountry');

    Route::get('country/top-five-country', 'API\CountryController@topFiveCountry')->name('api.country.top-five.get');

    Route::get('country/getAttackerByCountryByTime/{from}/{to}/{offset}/{limit}', 'API\CountryController@getAttackerByCountryByTime')->name('api.country.getAttackerByCountryByTime');

    Route::get('country/getDataByTime/{from}/{to}', 'API\CountryController@getDataByTime')->name('api.country.getDataByTime');

    // -- End Country Controller




    // IPController

    Route::get('ip/ip-list/{limit}/{offset}', 'API\IPController@IPList');

    Route::get('ip/detail/who-is/{ipaddress}', 'API\IPController@whoIs');

    Route::get('ip/detail/port-list/{ipaddress}/{detailLimit}/{detailOffset}', 'API\IPController@portList');

    Route::get('ip/detail/malware-list/{ipaddress}/{detailLimit}/{detailOffset}', 'API\IPController@malwareList');

    Route::get('ip/ip-search/{country}/{from}/{to}/{offset}/{limit}','API\IPController@IPSearch');

    Route::get('ip/search-ip/{ipAddress}/{offset}/{limit}', 'ApiController@searchIP');

    // kurang filter

    // -- End IP Controller




    // PORTCONTROLLER

    Route::get('port/top-five-port', 'ApiController@topFivePort');

    Route::get('port/count-port/{ipaddress}', 'API\PortController@countPortByIP');

    Route::get('port/port-list/{country}/{from}/{to}/{offset}/{limit}', 'API\PortController@portList');

    Route::get('port/port-list-data/{portNumber}/{offset}/{limit}' , 'ApiController@portListData');

    
    // -- End Port Controller




// MALWARECONTROLLER
Route::get('malware/malware-list/{country}/{from}/{to}/{offset}/{limit}','API\MalwareController@malwareList')->name('api.malware.malwareList');

Route::get('malware/total-malware-attack','API\MalwareController@totalMalwareAttack')->name('api.malware.totalMalware');

Route::get('malware/count-malware-not-detected','API\MalwareController@countMalwareNotDetected')->name('api.malware.countMalwareNotDetected');

    // MALWARECONTROLLER
    Route::get('malware/malware-list/{country}/{from}/{to}/{offset}/{limit}', 'API\MalwareController@malwareList')->name('api.malware.malwareList');
    Route::get('malware/total-malware-attack', 'API\MalwareController@totalMalwareAttack')->name('api.malware.totalMalware');
    Route::get('malware/count-malware-not-detected', 'API\MalwareController@countMalwareNotDetected')->name('api.malware.countMalwareNotDetected');


    Route::get('malware/get-malware-attack/{ipaddress}/{detailOffset}/{detailLimit}', 'ApiController@malwareAttackIpList');
    Route::get('malware/top-malware/{antivirus}/{offset}/{limit}', 'API\MalwareController@topMalware');
    Route::get('malware/top-malware-by-time/{antivirus}/{from}/{to}/{offset}/{limit}', 'API\MalwareController@topMalwareByTime');

    Route::get('malware/top-five-malware', 'ApiController@topFiveMalware');

    Route::get('malware/malware-list-data/{md5}', 'ApiCOntroller@malwareData');

    // -- End Malware Controller




    //INSTITUTIONCONTROLLER

    Route::get('institution/institution-list', 'API\InstitutionController@institutionList');

    Route::post('institution/store-institution', 'API\InstitutionController@storeInstitute');

    Route::get('institution/institution-count','ApiController@instituteCount');

    // -- End Institution Controller



    //SENSORCONTROLLER

    Route::get('sensor/sensor-list', 'API\SensorController@sensorList');

    Route::get('sensor/sensor-type', 'API\SensorController@sensorType');

    Route::get('sensor/get-ip-sensor/{sensorId}', 'API\SensorController@ipAttackedList');

    Route::get('sensor/get-port-sensor/{sensorId}', 'API\SensorController@PortAttackedList');

    Route::get('sensor/get-malware-sensor/{sensorIP}', 'API\SensorController@malwareAttackedList');

    Route::get('sensor/get-pic-sensor/{sensorID}', 'API\SensorController@picSensorDetail');

    // -- End Sensor Controller



    //USERCONTROLLER

    Route::get('user/user-list/{offset}/{limit}/{sessionKey}', 'API\UserController@userList');

    Route::post('user/store-user', 'Backend\UserController@storeUser')->name('api.backend.addUser');

    //Route::get('user/user-biodata/{userId}/{sessionKey}', 'API\UserController@userBiodata');

    Route::get('user/user-biodata/{userId}/{sessionKey}', 'API\UserController@userBiodata');

    //Route::get('user/user-biodata/{userId}', 'Backend\UserController@userBiodata');

    Route::post('user/update-biodata', 'Backend\UserController@updateBiodata')->name('api.backend.editUser');

    // -- End User Controller




    //MAPCONTROLLER

    Route::get('map/load-world-map', 'ApiController@loadWorldMap');

    Route::get('map/load-indonesia-map', 'ApiController@loadIndonesiaMap');

    // -- End Map Controller

    // FrontendController
	
    Route::get('getTotalAttack', 'API\FrontendController@getTotalAttack');
	
    Route::get('getAttackerByCountry/{from}/{till}', 'API\FrontendController@getAttackerByCountry');
	
    Route::get('getAttackerByCountryByTime/{from}/{till}/{offset}/{rowCount}', 'API\FrontendController@getAttackerByCountryByTime');
	
    Route::get('getMapData/{from}/{till}', 'API\FrontendController@getMapData');
	
    Route::get('getTopFiveAttackerIP/{country_short}/{offset}/{rowCount}', 'API\FrontendController@getTopFiveAttackerIP');
	
    Route::get('getTopFiveAttackerIPByTime/{from}/{till}/{country_short}/{offset}/{rowCount}', 'API\FrontendController@getTopFiveAttackerIPByTime');
	
    Route::get('getTopFiveAttackedPort/{country_short}/{offset}/{rowCount}', 'API\FrontendController@getTopFiveAttackedPort');
	
    Route::get('getTopFiveAttackedPortByTime/{from}/{till}/{country_short}/{offset}/{rowCount}', 'API\FrontendController@getTopFiveAttackedPortByTime');
	
    Route::get('getTopFiveMalware/{country_short}/{offset}/{rowCount}', 'API\FrontendController@getTopFiveMalware');
	
    Route::get('getTopFiveMalwareByTime/{country_short}/{from}/{till}/{offset}/{rowCount}', 'API\FrontendController@getTopFiveMalware');
	
    Route::get('getTotalPerDay/{month}/{year}', 'API\FrontendController@getTotalPerDay');
	
    Route::get('getDataByTime/{firstDayEpoch}/{lastDayEpoch}', 'API\FrontendController@getDataByTime');
	
    Route::get('getYears', 'API\FrontendController@getYear');
	
    Route::get('getMonthByYear/{year}', 'API\FrontendController@getMonthByYear');
	
    Route::get('getSensorCount', 'API\FrontendController@getSensorCount');
	
    Route::get('getAttackOnRange', 'API\FrontendController@getAttackOnRange');
	
    Route::get('getTopAttackerProvincePerCountry/{countryCode}/{limit}/{offset}', 'API\FrontendController@getTopAttackerProvincePerCountry');
	
    Route::get('getTopAttackerProvincePerCountryByTime/{countryCode}/{from}/{till}/{limit}/{offset}', 'API\FrontendController@getTopAttackerProvincePerCountryByTime');
	
    Route::get('getTopMalware/{antivirus}/{offset}/{limit}', 'API\FrontendController@getTopMalware');
	
    Route::get('getTopMalwareByTime/{antivirus}/{from}/{till}/{offset}/{limit}', 'API\FrontendController@getTopMalwareByTime');
	
    Route::get('getIndonesiaMapData', 'API\FrontendController@getIndonesiaMapData');
	
    Route::get('getIpAttackerProvince/{provinceIso}/{limit}/{offset}', 'API\FrontendController@getIpAttackerProvince');
	
    Route::get('getIpAttackerProvinceByTime/{provinceIso}/{from}/{till}/{limit}/{offset}', 'API\FrontendController@getIpAttackerProvinceByTime');
	
    Route::get('getPortAttackedProvince/{provinceIso}/{limit}/{offset}', 'API\FrontendController@getPortAttackedProvince');
	
    Route::get('getPortAttackedProvinceByTime/{provinceIso}/{from}/{till}/{limit}/{offset}', 'API\FrontendController@getPortAttackedProvinceByTime');
	
    Route::get('getMalwareProvince/{provinceIso}/{antivirus}/{limit}/{offset}', 'API\FrontendController@getMalwareProvince');
	
    Route::get('getMalwareProvinceByTime/{provinceIso}/{antivirus}/{from}/{till}/{limit}/{offset}', 'API\FrontendController@getMalwareProvinceByTime');
	
    Route::get('getCountryProvince/{provinceIso}/{limit}/{offset}', 'API\FrontendController@getCountryProvince');
	
    Route::get('getCountryProvinceByTime/{provinceIso}/{from}/{till}/{limit}/{offset}', 'API\FrontendController@getCountryAttackProvinceByTime');
	
    Route::get('getProvinceDataByTime/{firstDayEpoch}/{lastDayEpoch}', 'API\FrontendController@getProvinceDataByTime');
});
