<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['domain'=>env('APP_ADMIN_URL','admin.honeynet.id')],function(){
	Route::get('/', function() {
	    return redirect('/login');
	});

	Route::get('/login','Backend\UserController@getLogin')->name('backend.login.get');
	Route::post('/login','Backend\UserController@postLogin')->name('backend.login.post');
	Route::get('/logout','Backend\UserController@logout')->name('backend.logout');
	Route::post('/user/tambah-user', 'Backend\UserController@storeUser')->name('backend.user.store');

		//Route::get('/user/tambah-user', 'UserController@viewUser')->name('backend.user.view');

	Route::get('/dashboard','Backend\DashboardController@index')->name('backend.dashboard.get');



Route::get('/ip/sensor-list','ReportController@sensorList');
	// IP Controller

	Route::get('/ip/sensor-list','ReportController@sensorList');
	Route::get('/daftar-ip','Backend\IPController@index')->name('backend.ip.index');
	Route::get('/ip/export-pdf/{country}/{from}/{to}','Backend\IPController@exportPDF')->name('backend.ip.exportPDF');
	Route::get('/ip/export-csv/{country}/{from}/{to}', 'Backend\IPController@exportCSV')->name('backend.ip.exportCSV');
	Route::get('/daftar-port','Backend\PortController@index')->name('backend.port.index');
	Route::get('/daftar-malware','Backend\MalwareController@index')->name('backend.malware.index');
	Route::get('/daftar-sensor','Backend\SensorController@index')->name('backend.sensor.index');
	Route::get('/sensor/edit-sensor', 'Backend\SensorController@edit')->name('backend.sensor.edit');
	Route::get('/sensor/tambah-sensor','Backend\SensorController@create')->name('backend.sensor.create');

	Route::get('/institute/edit-institusi/{id}','Backend\InstitutionController@edit')->name('backend.institute.edit');
	Route::get('/user/daftar-user','Backend\UserController@index')->name('backend.user.index');
	Route::get('/user/tambah-user','Backend\UserController@create')->name('backend.user.create');
	Route::get('user/edit-user/{id}','Backend\UserController@edit')->name('backend.user.edit');
	Route::post('user/edit-user/{id}','Backend\UserController@updateBiodata')->name('backend.user.update');
	Route::get('/user/ganti-password','Backend\UserController@getChangePassword')->name('backend.user.change-password.get');
	Route::get('/daftar-institusi','Backend\InstitutionController@index')->name('backend.institution.index');
});
Route::get('/', 'Frontend\HomeController@index')->name('frontend.home.index');
