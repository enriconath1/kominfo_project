<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PortController extends Controller
{
    public function __construct()
    {
        $this->apiKey = config('app.api-key') ;
        $this->host = config('app.api-url') ;
    }

    public function countPortByIP($ipaddress)
    {
        $attackport = $this->host . "/attackerList/attackedPortByIpCount/" . $ipaddress . "/" . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $attackport);
        $attackportResp = $res->getBody();
        $attackportResp = json_decode($attackportResp);
        return response()->json(['data' => $attackportResp], 200);
    }

    public function portList($country, $from, $to, $offset, $limit)
    {
        if (Session::get('role_level') == 4) {
            $userId = Session::get('user_id');
            if ($from == "Date" || $to == "Date") {
                if ($country == 'all') {
                    $portList = $this->host . "/connections/portCountsUserNoTime/" . $userId . "/$offset/$limit/" . $this->apiKey;
                    $client = new \GuzzleHttp\Client();
                    $res = $client->request('GET', $portList);
                    $portListResp = $res->getBody();
                    $portListResp = json_decode($portListResp);
                    return response()->json([
                        'data' => $portListResp
                    ],200);
                } else {
                    $portList = $this->host . "/connections/portCountsUser/". $country . "/" . $userId . "/$offset/$limit/" . $this->apiKey;
                    $client = new \GuzzleHttp\Client();
                    $res = $client->request('GET', $portList);
                    $portListResp = $res->getBody();
                    $portListResp = json_decode($portListResp);
                    echo $portListResp;
                }
            } else {
                $fromEpoch = strtotime($from);
                $toEpoch = strtotime($to);
                if ($fromEpoch == $toEpoch) {
                    $toEpoch += 86399;
                }

                if ($country == 'all') {
                    $portList = $this->host . "/connections/portCounts/" . $fromEpoch . "/" . $toEpoch . "/" . $userId ."/$offset/$limit/" . $this->apiKey;
                    $client = new \GuzzleHttp\Client();
                    $res = $client->request('GET', $portList);
                    $portListResp = $res->getBody();
                    $portListResp = json_decode($portListResp);
                    echo $portListResp;
                } else {
                    $portList = $this->host . "/connections/portCounts/" . $fromEpoch . "/" . $toEpoch . "/" . $country . "/" . $userId ."/$offset/$limit/" . $this->apiKey;
                    $client = new \GuzzleHttp\Client();
                    $res = $client->request('GET', $portList);
                    $portListResp = $res->getBody();
                    $portListResp = json_decode($portListResp);
                    return response()->json([
                        'data' => $portListResp
                    ],200);
                }
            }
        } else {
            if ($from == "Date" || $to == "Date") {
                if ($country == 'all') {
                    $portList = $this->host . "/connections/portCounts/" . "/$offset/$limit/" . $this->apiKey;
                    $client = new \GuzzleHttp\Client();
                    $res = $client->request('GET', $portList);
                    $portListResp = $res->getBody();
                    $portListResp = json_decode($portListResp);
                    return response()->json([
                        'data' => $portListResp
                    ],200);
                } else {
                    $portList = $this->host . "/connections/portCounts/". $country . "/$offset/$limit/" . $this->apiKey;
                    $client = new \GuzzleHttp\Client();
                    $res = $client->request('GET', $portList);
                    $portListResp = $res->getBody();
                    $portListResp = json_decode($portListResp);
                    return response()->json([
                        'data' => $portListResp
                    ],200);
                }
            } else {
                $fromEpoch = strtotime($from);
                $toEpoch = strtotime($to);
                if ($fromEpoch == $toEpoch) {
                    $toEpoch += 86399;
                }

                if ($country == 'all') {
                    $portList = $this->host . "/connections/portCounts/" . $fromEpoch . "/" . $toEpoch ."/$offset/$limit/" . $this->apiKey;
                    $client = new \GuzzleHttp\Client();
                    $res = $client->request('GET', $portList);
                    $portListResp = $res->getBody();
                    $portListResp = json_decode($portListResp);
                    return response()->json([
                        'data' => $portListResp
                    ],200);
                } else {
                    $portList = $this->host . "/connections/portCounts/" . $fromEpoch . "/" . $toEpoch . "/" . $country ."/$offset/$limit/" . $this->apiKey;
                    $client = new \GuzzleHttp\Client();
                    $res = $client->request('GET', $portList);
                    $portListResp = $res->getBody();
                    $portListResp = json_decode($portListResp);
                    return response()->json([
                        'data' => $portListResp
                    ],200);
                }
            }
        }
    }
}
