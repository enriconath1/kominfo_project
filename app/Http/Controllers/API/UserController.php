<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function __construct()
    {
        $this->apiKey = config('app.api-key') ;
        $this->host = config('app.api-url') ;
    }
    public function userList($offset, $limit,$sessionKey)
    {
        $userList = $this->host . "users/list/$offset/$limit/$sessionKey/" . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $userList);
        $userListResp = $res->getBody();
        $userListResp = json_decode($userListResp);
        return response()->json(['data' => $userListResp], 200);
    }

    public function userBiodata($userId, $sessionKey)
    {
        $userlist = $this->host . "users/userBiodata/" . $userId .  "/" . $sessionKey . "/" .  $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $userlist);
        $userListResp = $res->getBody();
        $userListResp = json_decode($userListResp);
        return response()->json(['data' => $userListResp], 200);
    }

    // public function userBiodata($userId)
    // {

    //     $userlist = $this->host . "users/userBiodata/" . $userId .  "/" . $this->sessionKey . "/" .  $this->apiKey;
    //     $client = new \GuzzleHttp\Client();
    //     $res = $client->request('GET', $userlist);
    //     $userListResp = $res->getBody();
    //     $userListResp = json_decode($userListResp);
    //     return response()->json(['data' => $userListResp], 200);
    // }

}
