<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountryController extends Controller
{
    function __construct()
    {
		$this->apiKey = config('app.api-key') ;
	    $this->host = config('app.api-url') ;
    }
	public function countryList()
	{
        $countryList = $this->host . '/attackerList/distinctCountry/' . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET',$countryList);
        $listcountry = $res->getBody();
        $listcountry = json_decode($listcountry);
        return response()->json([
        	'data' => $listcountry
        ], 200);
		
	}

    public function topCountry()
    {
        $topCountry = $this->host . 'connections/countryCounts/0/10/' . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $topCountry);
        $topCountryResp = $res->getBody();
        $topCountryResp = json_decode($topCountryResp);
        return response()->json(['data' => $topCountryResp], 200);
    }

    public function topFiveCountry()
    {
        $topfiveCountry = $this->host . 'connections/countryCounts/0/5/' . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $topfiveCountry);
        $topCountryResp = $res->getBody();
        $topCountryResp = json_decode($topCountryResp);
        return response()->json(['data' => $topCountryResp], 200);
    }
    
    public function getAttackerByCountryByTime($from,$to,$offset,$limit)
    {
        $url = $this->host . "connections/countryCounts/byTime/" . $from . "/" . $to . "/" . $offset . "/" . $limit . "/" . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json(['data' => $data], 200);
    }
    
    public function getDataByTime($from, $to) {
        $url = $this->host . "connections/countryCounts/byTime/" . $from . "/" . $to . "/" . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json(['data' => $data], 200);
    }

}
