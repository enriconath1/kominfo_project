<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InstitusiController extends Controller
{
    public function institutionList() {
        $url = $this->host . "/institutes/instituteCount/". $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $institutionResp = $res->getBody();
        $institutionResp = json_decode($institutionResp);
        return response()->json([
        	'data' => $institutionResp
        ],200);
    }
}
