<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FrontendController extends Controller
{
    public function __construct()
    {
        $this->apiKey = config('app.api-key') ;
        $this->host = config('app.api-url') ;
    }
    public function getTotalAttack(){
        $url = $this->host . "attackerList/totalAttack/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }

    public function getAttackerByCountry($from, $till) {

        $url = $this->host . "connections/countryCounts/" . $from . "/" . $till . "/" . $this->apiKey;

        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }

    public function getAttackerByCountryByTime($from, $till, $offset, $rowCount) {

        $url = $this->host . "connections/countryCounts/byTime/" . $from . "/" . $till . "/" . $offset . "/" . $rowCount . "/" . $this->apiKey;

        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }

    public function getTopFiveAttackerIP($country_short, $offset, $rowCount) {
        $url = $this->host . "attackerList/attackerIpCount/" . $country_short . "/" . $offset . "/" . $rowCount . "/" . $this->apiKey;

        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }

    public function getTopFiveAttackerIPByTime($from, $till, $country_short, $offset, $rowCount) {
        $url = $this->host . "attackerList/attackerIpCount/" . $from . "/" . $till . "/" . $country_short . "/" . $offset . "/" . $rowCount . "/" . $this->apiKey;

        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }

    public function getTopFiveAttackedPort($country_short, $offset, $rowCount) {
        $url = $this->host . "connections/portCounts/" . $country_short . "/" . $offset . "/" . $rowCount . "/" . $this->apiKey;

        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }

    public function getTopFiveAttackedPortByTime($from, $till, $country_short, $offset, $rowCount) {
        $url = $this->host . "connections/portCounts/" . $from . "/" . $till . "/" . $country_short . "/" . $offset . "/" . $rowCount . "/" . $this->apiKey;

        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }

    public function getTopFiveMalware($country_short, $offset, $rowCount) {
        $url = $this->host . "malwares/distinctCounts/" . $country_short . "/" . $offset . "/" . $rowCount . "/" . $this->apiKey;

        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }

    public function getTopFiveMalwareByTime($country_short, $from, $till, $offset, $rowCount) {
        $url = $this->host . "malwares/malwareCount/" . $country_short . "/" . $from . "/" . $till . "/" . $offset . "/" . $rowCount . "/" . $this->apiKey;

        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }

    public function getMapData($from, $till) {

        $url = $this->host . "connections/countryCounts/" . $from . "/" . $till . "/" . $this->apiKey;

        $response = Httpful::get($url)->send();
        $mapJs = "{";
        $count = 0;
        $total = count($response->body);
        foreach ($response->body as $json) {
            if ($json->countryCode != "None") {
                $mapJs .= "\"$json->countryCode\"" . ":" . $json->count;

                if ($count != $total - 2)
                    $mapJs .= ",";
                else
                    $mapJs .= "}";
                $count++;
            }
        }
        echo $mapJs;
    }

    public function getIndonesiaMapData() {
        $url = $this->host . "province/getProvinceAttackerStats/" . $this->apiKey;

        $response = Httpful::get($url)->send();
        $mapJs = "{";
        $count = 0;
        $total = count($response->body);
        foreach ($response->body as $json) {
            $mapJs .= "\"$json->province_iso_id\"" . ":" . $json->count;

            if ($count != $total - 1)
                $mapJs .= ",";
            else
                $mapJs .= "}";
            $count++;
        }
        echo $mapJs;
    }

    public function getLatestConnection($offset, $count) {
        $url = $this->host . "connections/list/" . $offset . "/" . $count . "/" . $this->apiKey;

        $response = Httpful::get($url)->send();
        $jsonResponse = json_encode($response->body);
        foreach ($response->body as $json) {
            echo $json->ipSource . '<br>';
        }
    }

    public function getSpecificConnection($connId) {
        $url = $this->host . "connections/specific/" . $connId . "/" . $this->apiKey;

        $response = Httpful::get($url)->send();
        $jsonResponse = json_encode($response->body);
        echo $jsonResponse;
    }

    public function getPortList($offset, $count) {
        $url = $this->host . "connections/portCounts/" . $offset . "/" . $count . "/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }

    public function getPortListWithTime($from, $till, $offset, $count) {
        $url = $this->host . "connections/portCounts/" . $from . "/" . $till . "/" . $offset . "/" . $count . "/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }

    public function getYear() {
        $url = $this->host . "metadata/availableYears/" . $this->apiKey;

        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }

    public function getMonthByYear($year) {
        $url = $this->host . "metadata/availableMonths/" . $year . "/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }

    public function getTotalPerDay($month, $year) {
        $url = $this->host . "summaries/totalPerDay/" . $month . "/" . $year . "/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }

    public function getDataByTime($firstDayEpoch, $lastDayEpoch) {
        $url = $this->host . "connections/countryCounts/byTime/" . $firstDayEpoch . "/" . $lastDayEpoch . "/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }
    
    public function getProvinceDataByTime($firstDayEpoch, $lastDayEpoch) {
        $url = $this->host . "province/getProvinceAttackerStatsRange/" . $firstDayEpoch . "/" . $lastDayEpoch . "/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }

    public function getSensorCount() {
        $url = $this->host . "sensors/sensorActive/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }
    
    public function getTopAttackerProvincePerCountry($countryCode, $limit, $offset){
        $url = $this->host . "province/getProvinceRankByAttacker/" . $countryCode . "/" . $limit . "/" . $offset . "/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }
    
    public function getTopAttackerProvincePerCountryByTime($countryCode, $from, $till, $limit, $offset){
        $url = $this->host . "province/getProvinceRankByAttackerRange/" . $countryCode . "/" . $from . "/" . $till . "/" . $limit . "/" . $offset . "/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }

    public function getAttackOnRange() {
        $url = $this->host . "attackerList/attackCount/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }
    
    public function getIpAttackerProvince($provinceIso, $limit, $offset){
        $url = $this->host . "province/getProvinceIpAttackerCount/" . $provinceIso . "/" . $limit . "/" . $offset . "/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }
    
    public function getIpAttackerProvinceByTime($provinceIso, $from, $till, $limit, $offset){
        $url = $this->host . "province/getProvinceIpAttackerCountRange/" . $provinceIso . "/" . $from . "/" . $till . "/"  . $limit . "/" . $offset . "/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }
    
    public function getPortAttackedProvince($provinceIso, $limit, $offset){
        $url = $this->host . "province/getPortProvinceStatsLimit/" . $provinceIso . "/" . $limit . "/" . $offset . "/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }
    
    public function getPortAttackedProvinceByTime($provinceIso, $from, $till, $limit, $offset){
        $url = $this->host . "province/getPortProvinceStatsLimitRange/" . $provinceIso . "/" . $from . "/" . $till . "/" . $limit . "/" . $offset . "/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }
    
    public function getMalwareProvince($provinceIso, $antivirus , $limit, $offset){
        $url = $this->host . "province/getProvinceMalwareStats/" . $provinceIso . "/" . $antivirus . "/" . $limit . "/" . $offset . "/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }
    
    public function getMalwareProvinceByTime($provinceIso, $antivirus, $from, $till, $limit, $offset){
        $url = $this->host . "province/getProvinceMalwareStatsRange/" . $provinceIso . "/". $antivirus . "/" . $from . "/" . $till . "/"  . $limit . "/" . $offset . "/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }
    
    public function getCountryProvince($provinceIso, $limit, $offset){
        $url = $this->host . "province/getCountryAttackProvinceStatsLimit/" . $provinceIso . "/" . $limit . "/" . $offset . "/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }
    
    public function getCountryAttackProvinceByTime($provinceIso, $from, $till, $limit, $offset){
        $url = $this->host . "province/getCountryAttackProvinceStatsLimitRange/" . $provinceIso . "/" . $from . "/" . $till . "/" . $limit . "/" . $offset . "/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }
    
    public function getTopMalware($antivirus, $offset, $limit){
        $url = $this->host . "malwares/malwareStats/". $antivirus. "/". $offset . "/" . $limit . "/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }
    
    public function getTopMalwareByTime($antivirus, $from, $till, $offset, $limit){
        $url = $this->host . "malwares/malwareStatsRange/". $antivirus. "/". $from . "/" . $till . "/" . $offset . "/" . $limit . "/" . $this->apiKey;

        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = $res->getBody();
        $data = json_decode($data);
        return response()->json($data, 200);
    }

}
