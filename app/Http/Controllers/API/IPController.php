<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use phpWhois\Whois;


class IPController extends Controller
{
    
    function __construct()
    {
		$this->apiKey = config('app.api-key') ;
	    $this->host = config('app.api-url') ;
    }
	public function IPList($limit,$offset)
	{
        $attacker = $this->host . "/attackerList/attackerIpCount/$offset/$limit/" . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET',$attacker);
       	if($res->getStatusCode()==200){
	        $uniqueattack = $res->getBody();
	        $uniqueattack = json_decode($uniqueattack);
	        return response()->json([
	        	'data' => $uniqueattack
	        ], 200);
       	}
        return response()->json([
        	'error_message' => 'Access API Failed.'
        ], 400);
	}


    public function IPSearch($country,$from,$to,$limit,$offset)
    {
        $attacker = $this->host . "/attackerList/attackerIpCount/"  . $from . "/" . $to . "/" . $country . "/" . $offset . "/" . $limit . "/" . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET',$attacker);
        if($res->getStatusCode()==200){
            $uniqueattack = $res->getBody();
            $uniqueattack = json_decode($uniqueattack);
            return response()->json([
                'data' => $uniqueattack
            ], 200);
        }
        return response()->json([
            'error_message' => 'Access API Failed.'
        ], 400);
    }

    public function whoIs($ipaddress) {
        $whois = new Whois();
        $result = $whois->Lookup($ipaddress, false);
        echo json_encode($result);
    }
    public function portList($ipaddress, $detailLimit, $detailOffset) {
        $attackport = $this->host . "/attackerList/attackedPortByIpCount/" . $ipaddress . "/" . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET',$attackport);
        $attackportResp = $res->getBody();
        $attackportResp = json_decode($attackportResp);
        return response()->json(['data' => $attackportResp] , 200);
    }

    public function malwareList($ipaddress, $detailOffset, $detailLimit) {
        $malwareList = $this->host . "/attackerList/malwareByIp/" . $ipaddress . "/" . $detailOffset . "/" . $detailLimit . "/" . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET',$malwareList);
        $malwareListResp = $res->getBody();
        $malwareListResp = json_decode($malwareListResp);
        return response()->json(['data' => $malwareListResp] , 200);
    }
}
