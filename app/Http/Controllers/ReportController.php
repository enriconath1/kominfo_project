<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use League\Csv\Writer;
use Schema;
use SplTempFileObject;

class ReportController extends Controller
{
	function __construct()
    {
		$this->apiKey = config('app.api-key') ;
	    $this->host = config('app.api-url') ;
    }

    public function displayReport(Request $request) {
	    // Retrieve any filters
	    $fromDate = $request->input('from_date');
	    $toDate = $request->input('to_date');
	    $sortBy = $request->input('sort_by');

	    // Report title
	    $title = 'Registered User Report';
	    echo $title;
	    // For displaying filters description on header
	    $meta = [
	        'Registered on' => $fromDate . ' To ' . $toDate,
	        'Sort By' => $sortBy
	    ];

	    // Do some querying..
	    $queryBuilder = User::select(['name', 'balance', 'registered_at'])
	                        ->whereBetween('registered_at', [$fromDate, $toDate])
	                        ->orderBy($sortBy);

    	// Set Column to be displayed
	    $columns = [
	        'Name' => 'name',
	        'Registered At', // if no column_name specified, this will automatically seach for snake_case of column name (will be registered_at) column from query result
	        'Total Balance' => 'balance',
	        'Status' => function($result) { // You can do if statement or any action do you want inside this closure
	            return ($result->balance > 100000) ? 'Rich Man' : 'Normal Guy';
	        }
	    ];

	    /*
	        Generate Report with flexibility to manipulate column class even manipulate column value (using Carbon, etc).

	        - of()         : Init the title, meta (filters description to show), query, column (to be shown)
	        - editColumn() : To Change column class or manipulate its data for displaying to report
	        - editColumns(): Mass edit column
	        - showTotal()  : Used to sum all value on specified column on the last table (except using groupBy method). 'point' is a type for displaying total with a thousand separator
	        - groupBy()    : Show total of value on specific group. Used with showTotal() enabled.
	        - limit()      : Limit record to be showed
	        - make()       : Will producing DomPDF / SnappyPdf instance so you could do any other DomPDF / snappyPdf method such as stream() or download()
	    */
	    return PdfReport::of($title, $meta, $queryBuilder, $columns)
	                    ->editColumn('Registered At', [
	                        'displayAs' => function($result) {
	                            return $result->registered_at->format('d M Y');
	                        }
	                    ])
	                    ->editColumn('Total Balance', [
	                        'displayAs' => function($result) {
	                            return thousandSeparator($result->balance);
	                        }
	                    ])
	                    ->editColumns(['Total Balance', 'Status'], [
	                        'class' => 'right bold'
	                    ])
	                    ->showTotal([
	                        'Total Balance' => 'point' // if you want to show dollar sign ($) then use 'Total Balance' => '$'
	                    ])
	                    ->limit(20)
	                    ->stream(); // or download('filename here..') to download pdf
	}





	public function generateReportCSV($country, $from, $to) {
        
        
      if ($from == "Date" || $to == "Date") {
            $ipList = $this->host . "/attackerList/attackerIpCount/" . $country . "/" . $this->apiKey;
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $ipList);
            $ipListResp = $res->getBody();
            $ipListResp = json_decode($ipListResp);
      }
      else {
            $fromEpoch = strtotime($from);
            $toEpoch = strtotime($to);
            if ($fromEpoch == $toEpoch) {
                $toEpoch += 86399;
            }
      
      $ipList = $this->host . "/attackerList/attackerIpCountWithDate/" . $country . "/" . $fromEpoch . "/" . $toEpoch . "/" . $this->apiKey;
      $client = new \GuzzleHttp\Client();
      $res = $client->request('GET', $ipList);
      $ipListResp = $res->getBody();
      $ipListResp = json_decode($ipListResp);

      
            // $connections = Connection::select(DB::raw("distinct(remote_host) as host, local_port as port, count(remote_host) as count"))
                // ->where('country_name', 'like', $country . "%")
                // ->where('connection_type', '=', 'accept')
                // ->whereRaw("(connection_timestamp between '" . $from . "' and '" . $to . "') ")
                // ->groupBy('remote_host', 'port')
                // ->orderBy('host', 'DESC')
                // ->get();
        }
        $csv = Writer::createFromFileObject(new SplTempFileObject());

        // This creates header columns in the CSV file - probably not needed in some cases.
        $csv->insertOne(Schema::getColumnListing('main_meta'));

        foreach ($ipListResp as $data){
          $csv->insertOne($data->toArray());
        }

        $csv->output('main_meta' . '.csv');
    }

    public function sensorList()
    {
        $sensor = $this->host . 'sensors/sensorCount/' . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $sensor);
        $sensorlist = $res->getBody();
        $sensorlist = json_decode($sensorlist);
        return response()->json(['data' => $sensorlist], 200);
    }

}
