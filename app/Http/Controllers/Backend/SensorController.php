<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use Faker;

class SensorController extends Controller
{

    public function __construct()
    {
        $this->apiKey = config('app.api-key') ;
        $this->host = config('app.api-url') ;
    }

    public function index()
    {
        if (time() - Session::get('loginTime') < Session::get('sessionTime') && Session::has('username')) {
            $roleLevel = Session::get('role_level');
            $userID = Session::get('user_id');
            $sessionKey = Session::get('sessionKey');
            if (Session::get('loginTime') != 0 && Session::has('username')) {
                return view('backend.views.sensor')
                ->with('userID', $userID)
                ->with('roleLevel', $roleLevel)
                ->with('sessionKey', $sessionKey);
                echo $sessionKey;
            } else {
                return redirect('login');
            }
        } else {
            Session::flash('error', 'The session is timeout!');
            return redirect('login');
        }
    }
    public function create()
    {
        if (time() - Session::get('loginTime') < Session::get('sessionTime') && Session::has('username')) {
            $roleLevel = Session::get('role_level');
            $userID = Session::get('user_id');
            $sessionKey = Session::get('sessionKey');


            $category = $this->host . 'users/listCategory/' . Session::get('sessionKey') . '/' . $this->apiKey;
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $category);
            $categoryResp = $res->getBody();
            $categoryResp = json_decode($categoryResp);
            
            $institute = $this->host . 'institutes/instituteCount/' . $this->apiKey;
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $institute);
            $instituteResp = $res->getBody();
            $instituteResp = json_decode($instituteResp);
            
            $roleList = $this->host . 'users/listRole/' . Session::get('sessionKey') . '/' . $this->apiKey;
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $roleList);
            $roleListResp = $res->getBody();
            $roleListResp = json_decode($roleListResp);
            
            $sensorType = $this->host . 'sensors/sensorType/' . $this->apiKey;
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $sensorType);
            $sensorTypeResp = $res->getBody();
            $sensorTypeResp = json_decode($sensorTypeResp);
            
            $province = $this->host . 'province/getProvince/' . $this->apiKey;
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $province);
            $provinceResp = $res->getBody();
            $provinceResp = json_decode($provinceResp);




            if (Session::get('loginTime') != 0 && Session::has('username')) {
                return view('backend.views.sensor-add')
                ->with('userID', $userID)
                ->with('roleLevel', $roleLevel)
                ->with('sessionKey', $sessionKey) 
                ->with('category', $categoryResp)
                ->with('institute', $instituteResp)
                ->with('roleList', $roleListResp)
                ->with('sensorType', $sensorTypeResp)
                ->with('province', $provinceResp)
                ->with('faker', Faker\Factory::create('id_ID'));
                echo $sessionKey;
            } else {
                return redirect('login');
            }
        } else {
            Session::flash('error', 'The session is timeout!');
            return redirect('login');
        }
    }

    public function edit()
    {
        if (time() - Session::get('loginTime') < Session::get('sessionTime') && Session::has('username')) {
            $roleLevel = Session::get('role_level');
            $userID = Session::get('user_id');
            $sessionKey = Session::get('sessionKey');
            if (Session::get('loginTime') != 0 && Session::has('username')) {
                return view('backend.views.sensor-edit')
                ->with('userID', $userID)
                ->with('roleLevel', $roleLevel)
                ->with('sessionKey', $sessionKey)
                ->with('faker', Faker\Factory::create('id_ID'));
                echo $sessionKey;
            } else {
                return redirect('login');
            }
        } else {
            Session::flash('error', 'The session is timeout!');
            return redirect('login');
        }
    }



    public function storeSensor() {
        $rules = array(
            'sensor_name' => 'required',
            'sensor_ip' => 'required|ip',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()
                            ->withErrors($validator);
        } else {
            $categorySelectValue = Input::get('categorySelect');
            $instituteSelectValue = Input::get('instituteSelect');
            $userSelectValue = Input::get('userSelect');

            if ($categorySelectValue == 'new') { //if new category selected
                // $category = new Category();
                // $category->category_name = Input::get('category_name');
                // $category->timestamps = false;
                // $category->save();
                
                $encodedEmailUser1 = base64_encode(Input::get('user_email_1'));
                $encodedPasswordUser1 = base64_encode(Input::get('user_pass_1'));
                
                $encodedEmailUser2 = base64_encode(Input::get('user_email_2'));
                $encodedPasswordUser2 = base64_encode(Input::get('user_pass_2'));
                
                $category = $this->addCategoryInstance(Input::get('category_name'));
                $institute = $this->addInstituteInstance(Input::get('institution_name'),Input::get('institution_address'),$category->category_id,Input::get('institution_province'));
                
                $user1 = $this->addUserInstance(Input::get('user_name_1'),Input::get('user_telp_1'),$encodedEmailUser1,$encodedPasswordUser1,$institute->instituteId,Input::get('user_role_1'));
                $user2 = $this->addUserInstance(Input::get('user_name_2'),Input::get('user_telp_2'),$encodedEmailUser2,$encodedPasswordUser2,$institute->instituteId,Input::get('user_role_2'));
                
                $this->addSensorInstance(Input::get('sensor_name'), Input::get('sensor_ip'), $user1->userId, $user2->userId, Input::get('sensor_type_id'));
                
            } else { //if already existing category selected
                if ($instituteSelectValue == 'new') { //if new institute selected
                    // $institute = new Institute();
                    // $institute->institute_name = Input::get('institution_name');
                    // $institute->institute_address = Input::get('institution_address');
                    // $institute->category_id = Input::get('institution_category');
                    // $institute->institute_province = Input::get('institution_province');
                    // $institute->timestamps = false;
                    // $institute->save();
                    
                    $encodedEmailUser1 = base64_encode(Input::get('user_email_1'));
                    $encodedPasswordUser1 = base64_encode(Input::get('user_pass_1'));
                    
                    $encodedEmailUser2 = base64_encode(Input::get('user_email_2'));
                    $encodedPasswordUser2 = base64_encode(Input::get('user_pass_2'));
                    
                    $institute = $this->addInstituteInstance(Input::get('institution_name'),Input::get('institution_address'),Input::get('category_id'),Input::get('institution_province'));
                    
                    
                    $user1 = $this->addUserInstance(Input::get('user_name_1'),Input::get('user_telp_1'),$encodedEmailUser1,$encodedPasswordUser1,$institute->instituteId,Input::get('user_role_1'));
                    $user2 = $this->addUserInstance(Input::get('user_name_2'),Input::get('user_telp_2'),$encodedEmailUser2,$encodedPasswordUser2,$institute->instituteId,Input::get('user_role_2'));
                    
                    $this->addSensorInstance(Input::get('sensor_name'), Input::get('sensor_ip'), $user1->userId, $user2->userId, Input::get('sensor_type_id'));
                    
                } else { //if existing institute selected
                    if ($userSelectValue == 'new') { //if new user selected
                        $encodedEmailUser1 = base64_encode(Input::get('user_email_1'));
                        $encodedPasswordUser1 = base64_encode(Input::get('user_pass_1'));
                        
                        $encodedEmailUser2 = base64_encode(Input::get('user_email_2'));
                        $encodedPasswordUser2 = base64_encode(Input::get('user_pass_2'));
                    
                        // $user1 = new User();
                        // $user1->user_name = Input::get('user_name_1');
                        // $user1->user_telp = Input::get('user_telp_1');
                        // $user1->user_email = Input::get('user_email_1');
                        // $user1->user_pass = Hash::make(Input::get('user_pass_1'));
                        // $user1->institute_id = Input::get('user_institute_1');
                        // $user1->role_id = Input::get('user_role_1');
                        // $user1->user_last_activity = new \DateTime;
                        // $user1->user_last_update = new \DateTime;
                        // $user1->user_creation_time = new \DateTime;
                        // $user1->timestamps = false;
                        // $user1->save();

                        // $user2 = new User();
                        // $user2->user_name = Input::get('user_name_2');
                        // $user2->user_telp = Input::get('user_telp_2');
                        // $user2->user_email = Input::get('user_email_2');
                        // $user2->user_pass = Hash::make(Input::get('user_pass_2'));
                        // $user2->institute_id = Input::get('user_institute_2');
                        // $user2->role_id = Input::get('user_role_2');
                        // $user2->user_last_activity = new \DateTime;
                        // $user2->user_last_update = new \DateTime;
                        // $user2->user_creation_time = new \DateTime;
                        // $user2->timestamps = false;
                        // $user2->save();
                        
                        $user1 = $this->addUserInstance(Input::get('user_name_1'),Input::get('user_telp_1'),$encodedEmailUser1,$encodedPasswordUser1,Input::get('institution_id'),Input::get('user_role_1'));
                        $user2 = $this->addUserInstance(Input::get('user_name_2'),Input::get('user_telp_2'),$encodedEmailUser2,$encodedPasswordUser2,Input::get('institution_id'),Input::get('user_role_2'));
                        
                        $this->addSensorInstance(Input::get('sensor_name'), Input::get('sensor_ip'), $user1->userId, $user2->userId, Input::get('sensor_type_id'));
                    }
                    else{ //if existing user selected
                        $this->addSensorInstance(Input::get('sensor_name'), Input::get('sensor_ip'), Input::get('user_1_id'), Input::get('user_2_id'), Input::get('sensor_type_id'));
                        
                        // $sensor = new Sensor();
                        // $sensor->sensor_name = Input::get('sensor_name');
                        // $sensor->sensor_ip = Input::get('sensor_ip');
                        // $sensor->user_1_id = Input::get('user_1_id');
                        // $sensor->user_2_id = Input::get('user_2_id');
                        // $sensor->sensor_type_id = Input::get('sensor_type_id');
                        // $sensor->timestamps = false;
                        // $sensor->save();
                    }
                }
            }

            
            Session::flash('success', 'Successfully added new Sensor');
            return Redirect::to('sensorList');
        }
    }



    
}
