<?php

namespace App\Http\Controllers\Backend;

use Config;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class InstitutionController extends Controller
{

    public function __construct()
    {
        $this->apiKey = Config::get('app.api-key');
        $this->host = Config::get('app.api-url');
        
        $this->client = new \GuzzleHttp\Client;
    }

    public function index()
    {
        if (time() - Session::get('loginTime') < Session::get('sessionTime') && Session::has('username')) {
            $roleLevel = Session::get('role_level');
            $userID = Session::get('user_id');
            $sessionKey = Session::get('sessionKey');
            if (Session::get('loginTime') != 0 && Session::has('username')) {
                return view('backend.views.institusi')
                ->with('userID', $userID)
                ->with('roleLevel', $roleLevel)
                ->with('sessionKey', $sessionKey);
                echo $sessionKey;
            } else {
                return redirect('login');
            }
        } else {
            Session::flash('error', 'The session is timeout!');
            return redirect('login');
        }
    }



    public function edit($id)
    {
        if (time() - Session::get('loginTime') < Session::get('sessionTime') && Session::has('username')) {
            $roleLevel = Session::get('role_level');
            $userID = Session::get('user_id');
            $sessionKey = Session::get('sessionKey');


            $institute = $this->host . '/institutes/instituteCount/' . $this->apiKey;
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET',$institute);
            $instituteResp = $res->getBody();
            $instituteResp = json_decode($instituteResp);

            $province = $this->host . 'province/getProvince/' . $this->apiKey;
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $province);
            $provinceResp = $res->getBody();
            $provinceResp = json_decode($provinceResp);


            if (Session::get('loginTime') != 0 && Session::has('username')) {
                return view('backend.views.institusi-edit')
                ->with('userID', $userID)
                ->with('roleLevel', $roleLevel)
                ->with('sessionKey', $sessionKey)
                ->with('institute', $instituteResp)
                ->with('province',$provinceResp);
                echo $sessionKey;
            } else {
                return redirect('login');
            }
        } else {
            Session::flash('error', 'The session is timeout!');
            return redirect('login');
        }
    }
	
}
