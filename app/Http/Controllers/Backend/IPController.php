<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Excel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use League\Csv\Writer;
use PDF;
use Schema;
use SplTempFileObject;
use Carbon\Carbon;

class IPController extends Controller
{
    function __construct()
    {
        $this->apiKey = config('app.api-key') ;
        $this->host = config('app.api-url') ;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (time() - Session::get('loginTime') < Session::get('sessionTime') && Session::has('username')) {
          $roleLevel = Session::get('role_level');
          $userID = Session::get('user_id');
          $sessionKey = Session::get('sessionKey');
          if (Session::get('loginTime') != 0 && Session::has('username')) {
                return view('backend.views.ip')
                ->with('userID', $userID)
                ->with('roleLevel', $roleLevel)
                ->with('sessionKey',$sessionKey);
                echo $sessionKey;
          } else {
              return redirect('login');
          }
        } else{
            Session::flash('error', 'The session is timeout!');
            return redirect('login');
        }
    }


    public function exportPDF($country,$from,$to){
        if($from=='Date' || $to=='Date'){
          $url = $this->host . "/attackerList/attackerIpCount/" . $country . "/" . $this->apiKey;
        } else {
          $fromDate = Carbon::createFromFormat('d-m-Y', $from);
          $fromDate = $fromDate->timestamp;
          $toDate = Carbon::createFromFormat('d-m-Y', $to);
          $toDate = $toDate->timestamp;
          $url = $this->host . "/attackerList/attackerIpCountWithDate/" . $country . "/" . $fromDate . "/" . $toDate . "/" . $this->apiKey;
        }
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET',$url);
        
        $res = $res->getBody();
        $res = json_decode($res);
        
        $pdf = PDF::loadView('backend.views.pdf', compact('res'));
        return $pdf->stream("daftar-ip.pdf");
    }


    public function exportCSV($country,$from,$to) {
        if($from=='Date' || $to=='Date'){
          $url = $this->host . "/attackerList/attackerIpCount/" . $country . "/" . $this->apiKey;
        } else {
          $url = $this->host . "/attackerList/attackerIpCountWithDate/" . $country . "/" . $fromDate . "/" . $toDate . "/" . $this->apiKey;
        }
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET',$url);
        
        $listresp = $res->getBody();
        $listresp = json_decode($listresp);

        Excel::create('daftar-ip', function($excel) use ($listresp) {
          $excel->sheet('daftar-ip2', function($sheet) use ($listresp) {
              $sheet->loadView('backend.views.csv',compact('listresp'));
          });
        })->download('csv');
    }


   

}
