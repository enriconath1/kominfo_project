<?php

namespace App\Http\Controllers\Backend;

use Config;
use Illuminate\Support\Facades\Session;
use Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    protected $apiKey;
    protected $host;
    protected $client;

    public function __construct()
    {
        $this->apiKey = Config::get('app.api-key');
        $this->host = Config::get('app.api-url');
        
        $this->client = new \GuzzleHttp\Client;
    }

    public function getLogin()
    {
        return view('backend.views.login');
    }

    public function postLogin(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');

        $encodedEmail = base64_encode($email);
        $encodedPassword = base64_encode($password);
        $url = $this->host."users/login/".$this->apiKey;
        $client = new \GuzzleHttp\Client;
        $response = $client->request('POST', $url, [
            'form_params' => [
                'encodedEmail' => $encodedEmail,
                'encodedPassword' => $encodedPassword
            ]]);


        try {
            if ($response->getStatusCode()==200) {
                $response = $response->getBody();
                $obj = json_decode($response);
                $sessionKey = $obj->sessionKey;
                
                $reqUserDetail = $client->request('GET', $this->host . 'users/byEmail/' . $encodedEmail . '/' . $sessionKey . '/' . $this->apiKey);
                $user = $reqUserDetail->getBody()->getContents();
                $userInfo = json_decode($user);

                Session::put('user_id', $userInfo->userId);
                Session::put('username', $userInfo->name);
                Session::put('role', $userInfo->roleName);
                Session::put('role_level', $userInfo->roleLevel);
                Session::put('loginTime', $obj->loginTime);
                Session::put('sessionTime', $obj->sessionTime);
                Session::put('sessionKey', $sessionKey);

                if (Session::get('role_level') == 4) {
                    $url = $this->host . "sensors/sensorByUser/".$userInfo->userId."/" . $this->apiKey;
                    $response = Httpful::get($url)->send();
                    Session::put('sensors', $response->body);
                }

                $reqUserConfig = $client->request('GET', $this->host . 'dashboard/getConfiguration/' . $userInfo->userId . '/' . $sessionKey . '/' . $this->apiKey);
                $config = $reqUserConfig->getBody()->getContents();
                echo $config;
                if ($config == '[]') {
                    $url = $this->host . "dashboard/saveNewConfiguration/" . $userInfo->userId . "/" . $sessionKey . '/' . $this->apiKey;
                    $client = new \GuzzleHttp\Client;
                    $jsonString = '[{"position":0,"divId":"widget-1","type":"pie","selectedData":"topCountry"},{"position":1,"divId":"widget-2","type":"donut","selectedData":"topCountry"}]';
                    $req = $client->request(
                        'POST',
                        $url,
                        ['JSON' => ['json' => $jsonString]]
                    );

                    $postBody = $req->getBody();
                    dd($req);
                    $postBody->setField('configuration', $jsonString);
                }
                
                Config::set('app.session-key', $sessionKey);
                return redirect()->route('backend.dashboard.get');
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            if ($e->getResponse()->getStatusCode() == 400) {
                Session::flash('failure', 'Wrong Username or Password');
            }
            return redirect()->route('backend.login.get');
        }
    }

    public function logout()
    {
        Session::flush();
        
        return redirect()->route('backend.login.get');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (time() - Session::get('loginTime') < Session::get('sessionTime') && Session::has('username')) {
            $roleLevel = Session::get('role_level');
            $userID = Session::get('user_id');
            $sessionKey = Session::get('sessionKey');

            if (Session::get('loginTime') != 0 && Session::has('username')) {
                return view('backend.views.user')
                ->with('userID', $userID)
                ->with('roleLevel', $roleLevel)
                ->with('sessionKey', $sessionKey);
                echo $sessionKey;
            } else {
                return redirect('login');
            }
        } else {
            Session::flash('error', 'The session is timeout!');
            return redirect('login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (time() - Session::get('loginTime') < Session::get('sessionTime') && Session::has('username')) {
            $roleLevel = Session::get('role_level');
            $userID = Session::get('user_id');
            $sessionKey = Session::get('sessionKey');


            $institute = $this->host . 'users/listInstitution/' . Session::get('sessionKey') . '/' . $this->apiKey;
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $institute);
            $institutes = $res->getBody();
            $institutes = json_decode($institutes);
            $selectInst = array();
            foreach($institutes as $inst){
                $selectInst[$inst->instituteId] = $inst->instituteName;
            }

            $roleList = $this->host . 'users/listRole/' . Session::get('sessionKey') . '/' . $this->apiKey;
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $roleList);
            $roles = $res->getBody();
            $roles = json_decode($roles);
            $selectRole = array();
            foreach($roles as $role){
                $selectRole[$role->id] = $role->roleName;
            }



            if (Session::get('loginTime') != 0 && Session::has('username')) {
                return view('backend.views.user-add')
                ->with('userID', $userID)
                ->with('roleLevel', $roleLevel)
                ->with('sessionKey', $sessionKey)
                ->with('institute',$institutes)
                ->with('role',$roles);
                echo $sessionKey;
            } else {
                return redirect('login');
            }
        } else {
            Session::flash('error', 'The session is timeout!');
            return redirect('login');
        }
    }

    public function edit($id)
    {
        $userIdEdit=$id;
        if (time() - Session::get('loginTime') < Session::get('sessionTime') && Session::has('username')) {
            $roleLevel = Session::get('role_level');
            $userID = Session::get('user_id');
            $sessionKey = Session::get('sessionKey');


            $userList = $this->host . 'users/userBiodata/' . $userIdEdit . '/' . Session::get('sessionKey') . '/' . $this->apiKey;
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $userList);
            $userListResp = $res->getBody();
            $userListResp = json_decode($userListResp);


            $institute = $this->host . 'users/listInstitution/' . Session::get('sessionKey') . '/' . $this->apiKey;
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $institute);
            $institutes = $res->getBody();
            $institutes = json_decode($institutes);
            $selectInst = array();
            foreach($institutes as $inst){
                $selectInst[$inst->instituteId] = $inst->instituteName;
            }

            $roleList = $this->host . 'users/listRole/' . Session::get('sessionKey') . '/' . $this->apiKey;
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $roleList);
            $roles = $res->getBody();
            $roles = json_decode($roles);
            $selectRole = array();
            foreach($roles as $role){
                $selectRole[$role->id] = $role->roleName;
            }


            if (Session::get('loginTime') != 0 && Session::has('username')) {
                return view('backend.views.user-edit')
                ->with('userID', $userID)
                ->with('roleLevel', $roleLevel)
                ->with('sessionKey', $sessionKey)
                ->with('userList', $userListResp)
                ->with('institute',$institutes)
                ->with('role',$roles);
                echo $sessionKey;
            } else {
                return redirect('login');
            }
        } else {
            Session::flash('error', 'The session is timeout!');
            return redirect('login');
        }
    }


    public function updateBiodata(Request $request , $id){
        $roleLevel = Session::get('role_level');
        $userID = Session::get('user_id');
        $sessionKey = $request->input('sessionKey');
        $name = $request->input('user-name');
        $phone_number = $request->input('user-phone');
        $institute = $request->input('user-institution-select');
        $role = $request->input('user-role');

        $rules = array
        (
            'name' => 'required',
            'telp' => 'required|numeric'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($name == null) 
                {
                    // return redirect::back()
                    //                 ->withErrors($validator);
                    echo "<script>console.log( 'Debug Objects: " . $name . "' );</script>";
                    return redirect()->route('api.backend.editUser');
                }

       
        else 
                {
                    
                    $formValue = [
                        'name' => $name,
                        'telp' => $phone_number,
                        'instituteId' => $institute,
                        'roleId' => $role
                    ];

                    try {
                        $url = $this->host . "users/updateBiodata/" . $id . "/" . $sessionKey . "/" . $this->apiKey;
                        $client = new \GuzzleHttp\Client;
                        $response = $client->request('POST', $url, [
                        'form_params' => $formValue]);
                        $response = $response->getBody();
                        $obj = json_decode($response);

                    } catch (\GuzzleHttp\Exception\ClientException $e) {
                        if ($e->getResponse()->getStatusCode() == 400) {
                            Session::flash('failure', 'Wrong Username or Password');
                        }
                        return redirect()->route('backend.login.get');
                    }

                    return redirect()->route('backend.user.index');
                }




    }



    public function getChangePassword()
    {
        if (time() - Session::get('loginTime') < Session::get('sessionTime') && Session::has('username')) {
            $roleLevel = Session::get('role_level');
            $userID = Session::get('user_id');
            $sessionKey = Session::get('sessionKey');
            if (Session::get('loginTime') != 0 && Session::has('username')) {
                return view('backend.views.user-change-password')
                ->with('userID', $userID)
                ->with('roleLevel', $roleLevel)
                ->with('sessionKey', $sessionKey);
                echo $sessionKey;
            } else {
                return redirect('login');
            }
        } else {
            Session::flash('error', 'The session is timeout!');
            return redirect('login');
        }
    }


    public function viewUser()
    {
        return view('backend.views.user-add');
    }

    public function storeUser(Request $request) 
    {

            $roleLevel = Session::get('role_level');
            $userID = Session::get('user_id');
            $sessionKey = $request->input('sessionKey');
                $name = $request->input('user-name');
                $phone_number = $request->input('user-phone');
                $email = $request->input('user-email');
                $institute = $request->input('user-institution-select');
                $role = $request->input('user-role');
                $password = $request->input('user-password');
                $repassword = $request->input('user-repassword');

                //echo "<script>console.log( 'Debug Objects: " . $name . "' );</script>";

                $rules = array
                (
                    'name' => 'required',
                    'telp' => 'required|numeric',
                    'email' => 'required|email',
                    'password' => 'required|min:6',
                    'confirmPassword' => 'required|same:password|min:6'
                );
                $validator = Validator::make(Input::all(), $rules);

                if ($name == null) 
                {
                    // return redirect::back()
                    //                 ->withErrors($validator);
                    echo "<script>console.log( 'Debug Objects: " . $name . "' );</script>";
                    return redirect()->route('api.backend.addUser');
                } 

                else 
                {
                    $encodedEmail = base64_encode($email);
                    $encodedPassword = base64_encode($password);
                    $formValue = [
                        'name' => $name,
                        'telp' => $phone_number,
                        'encodedEmail' => $encodedEmail,
                        'encodedPassword' => $encodedPassword,
                        'instituteId' => $institute,
                        'roleId' => $role
                    ];

                    try {
                        $url = $this->host . "users/add/" . $sessionKey . "/" . $this->apiKey;
                        $client = new \GuzzleHttp\Client;
                        $response = $client->request('POST', $url, [
                        'form_params' => $formValue]);
                        $response = $response->getBody();
                        $obj = json_decode($response);

                    } catch (\GuzzleHttp\Exception\ClientException $e) {
                        if ($e->getResponse()->getStatusCode() == 400) {
                            Session::flash('failure', 'Wrong Username or Password');
                        }
                        return redirect()->route('backend.login.get');
                    }

                    return redirect()->route('backend.user.index');
                }
        
    }



    public function userBiodata($userId)
    {
        $userlist = $this->host . "users/userBiodata/" . $userId . "/" . Session::get('sessionKey') . "/" .  $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $userlist);
        $userListResp = $res->getBody();
        $userListResp = json_decode($userListResp);
        return response()->json(['data' => $userListResp], 200);
    }

}
