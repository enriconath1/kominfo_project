<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PortController extends Controller
{
    function __construct()
    {
        $this->apiKey = config('app.api-key') ;
        $this->host = config('app.api-url') ;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (time() - Session::get('loginTime') < Session::get('sessionTime') && Session::has('username')) {
          $roleLevel = Session::get('role_level');
          $userID = Session::get('user_id');
          $sessionKey = Session::get('sessionKey');


          $countryList = $this->host . '/attackerList/distinctCountry/' . $this->apiKey;
          $client = new \GuzzleHttp\Client();
          $res = $client->request('GET',$countryList);
          $listcountry = $res->getBody();
          $listcountry = json_decode($listcountry);


          if (Session::get('loginTime') != 0 && Session::has('username')) {
                return view('backend.views.port')
                ->with('userID', $userID)
                ->with('roleLevel', $roleLevel)
                ->with('countryList',$listcountry)
                ->with('sessionKey',$sessionKey);
                echo $sessionKey;
          } else {
              return redirect('login');
          }
        } else{
            Session::flash('error', 'The session is timeout!');
            return redirect('login');
        }
    }
}
