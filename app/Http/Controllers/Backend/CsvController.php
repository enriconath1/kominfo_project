<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Excel;
use App\Http\Controllers\Controller;

class CsvController extends Controller
{
    function __construct()
    {
        $this->apiKey = config('app.api-key') ;
        $this->host = config('app.api-url') ;
    }

}
