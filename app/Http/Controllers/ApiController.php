<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    public function __construct()
    {
        $this->apiKey = config('app.api-key') ;
        $this->host = config('app.api-url') ;
    }


    // public function index(request $request){


    // 	return view('pages.dashboard',compact('totalAttackResp'));
    // }
    public function index()
    {
        $attacker = $this->host . "/attackerList/attackerIpCount/" . 0 . "/" . 50 . "/" . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $attacker);
        $uniqueattack = $res->getBody();
        $uniqueattack = json_decode($uniqueattack);
        //return response()->json(['total' => $uniqueattack] , 200);
        return view('pages.ipList', compact('uniqueattack'));
    }


    public function provinceList()
    {
        $province = $this->host . 'province/getProvince/' . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $province);
        $provinceResp = $res->getBody();
        $provinceResp = json_decode($provinceResp);
        return response()->json(['data' => $provinceResp], 200);
    }

    public function getTotalAttack()
    {
        $totalAttack = $this->host . 'attackerList/totalAttack/' . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $totalAttack);
        $totalAttackResp = $res->getBody();
        $totalAttackResp = json_decode($totalAttackResp);
        return response()->json([
            'data' => [
                'count' => $totalAttackResp[0]->count,
                'timestamp' => $totalAttackResp[0]->timestamp,
            ]
        ], 200);
    }

    public function getTotalCountry()
    {
        $totalCountry = $this->host . 'attackerList/totalCountry/' . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $totalCountry);
        $getTotal = $res->getBody();
        $getTotal = json_decode($getTotal);
        //return response()->json(['total' => $getTotal] , 200);
        return response()->json([
            'data' => [
                'count' => $getTotal[0]->count,
                'timestamp' => $getTotal[0]->timestamp,
            ]
        ], 200);
    }



    public function getUniqueAttack()
    {
        $uniqueAttacker = $this->host . '/attackerList/uniqueAttacker/' . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $uniqueAttacker);
        $uniqueattack = $res->getBody();
        $uniqueattack = json_decode($uniqueattack);
        //return response()->json(['total' => $uniqueattack] , 200);
        return response()->json([
            'data' => [
                'count' => $uniqueattack[0]->count,
                'timestamp' => $uniqueattack[0]->timestamp,
            ]
        ], 200);
    }


    public function countryList()
    {
        $countryList = $this->host . '/attackerList/distinctCountry/' . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $countryList);
        $listcountry = $res->getBody();
        $listcountry = json_decode($listcountry);
        return response()->json(['data' => $listcountry], 200);
    }
    

    public function topCountry()
    {
        $topCountry = $this->host . 'connections/countryCounts/0/10/' . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $topCountry);
        $topCountryResp = $res->getBody();
        $topCountryResp = json_decode($topCountryResp);
        return response()->json(['data' => $topCountryResp], 200);
    }

    public function topFiveCountry()
    {
        $topfiveCountry = $this->host . 'connections/countryCounts/0/5/' . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $topfiveCountry);
        $topCountryResp = $res->getBody();
        $topCountryResp = json_decode($topCountryResp);
        return response()->json(['data' => $topCountryResp], 200);
    }

    public function topFivePort()
    {
        $fivePort = $this->host . 'connections/portCounts/0/5/' . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $fivePort);
        $fivePortResp = $res->getBody();
        $fivePortResp = json_decode($fivePortResp);
        return response()->json(['data' => $fivePortResp], 200);
    }


    public function topFiveMalware()
    {
        $topMalware = $this->host . 'malwares/malwareStats/AhnLab-V3/0/5/' . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $topMalware);
        $malwareResp = $res->getBody();
        $malwareResp = json_decode($malwareResp);
        return response()->json(['data' => $malwareResp], 200);
    }

    public function instituteCount()
    {
        $institute = $this->host . '/institutes/instituteCount/' . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $institute);
        $instituteResp = $res->getBody();
        $instituteResp = json_decode($instituteResp);
        return response()->json(['data' => $instituteResp], 200);
    }


    public function loadWorldMap()
    {
        $WorldStat = $this->host . "connections/countryCounts/" . 0 . "/" . 250 . "/" . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $WorldStat);
        $WorldStatResp = $res->getBody();
        $WorldStatResp = json_decode($WorldStatResp);
        return response()->json(['data' => $WorldStatResp], 200);
    }


    public function loadIndonesiaMap()
    {
        $provinceStat = $this->host . 'province/getProvinceAttackerStats/' . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $provinceStat);
        $provinceStatResp = $res->getBody();
        $provinceStatResp = json_decode($provinceStatResp);
        return response()->json(['data' => $provinceStatResp], 200);
    }

    
    public function attackerList()
    {
        $attacker = $this->host . "/attackerList/attackerIpCount/" . 0 . "/" . 300 . "/" . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $attacker);
        $uniqueattack = $res->getBody();
        $uniqueattack = json_decode($uniqueattack);
        return response()->json(['data' => $uniqueattack], 200);
        //return view('pages.ipList',compact('uniqueattack'));
    }


    public function malwareAttackIpList($ipaddress, $detailOffset, $detailLimit)
    {
        $malwareList = $this->host . "/attackerList/malwareByIp/" . $ipaddress . "/" . $detailOffset . "/" . $detailLimit . "/" . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $malwareList);
        $malwareListResp = $res->getBody();
        $malwareListResp = json_decode($malwareListResp);
        return response()->json(['data' => $malwareListResp], 200);
        //return view('pages.ipList',compact('uniqueattack'));
    }


    public function searchIP($ipAddress, $offset, $limit)
    {
        $searchIP = $this->host . "/attackerList/ip/" . $ipAddress . "/" . $offset . "/" . $limit . "/" . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $searchIP);
        $searchIPResp = $res->getBody();
        $searchIPResp = json_decode($searchIPResp);
        return response()->json(['data' => $searchIPResp], 200);
        //return view('pages.ipList',compact('uniqueattack'));
    }



    public function grabberIP($ipaddress)
    {
        $whois = new Whois();
        $result = $whois->Lookup($ipaddress, false);
        echo json_encode($result);
    }


    public function attackDate($country, $fromEpoch, $toEpoch)
    {
        $attackDate = $this->host . "/attackerList/attackerIpCountWithDate/" . $country . "/" . $fromEpoch . "/" . $toEpoch . "/" . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $attackDate);
        $attackDateResp = $res->getBody();
        $attackDateResp = json_decode($attackDateResp);
        return response()->json(['data' => $attackDateResp], 200);
        //return view('pages.ipList',compact('uniqueattack'));
    }



    public function portListData($portNumber , $offset , $limit)
    {
        $portListData =$this->host . "/connections/countryAndIPByPort/" . $portNumber . "/" . $offset . "/" . $limit . "/"  . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET',$portListData);
        $portListDataResp = $res->getBody();
        $portListDataResp = json_decode($portListDataResp);
        return response()->json(['data' => $portListDataResp], 200);
    }


    public function malwareData($md5)
    {
        $malwareData = $this->host . 'malwares/scanResult/' . $md5 . '/' . $this->apiKey;
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $malwareData);
        $malwaresResp = $res->getBody();
        $malwaresResp = json_decode($malwaresResp);
        return response()->json(['data' => $malwaresResp], 200);
    }

}
