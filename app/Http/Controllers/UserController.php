<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{
    function __construct()
    {
		$this->apiKey = config('app.api-key') ;
	    $this->host = config('app.api-url') ;
    }



    public function loginUser($email,$password) 
    {
        $email = $request->input('email');
        $password = $request->input('password');

        $encodedEmail = base64_encode($email);
        $encodedPassword = base64_encode($password);
        $url = $this->host . "users/login/" . $this->apiKey;
        $client = new \GuzzleHttp\Client;
        $client->setDefaultOption('verify', false);

        $req = $client->createRequest('POST', $url);
        $req->setHeader('Content-Type', 'application/x-www-form-urlencoded');

        $postBody = $req->getBody();
        $postBody->setField('encodedEmail', $encodedEmail);
        $postBody->setField('encodedPassword', $encodedPassword);


		try {
			$resp = $client->send($req);
				if ($resp) {
				$response = $resp->getBody()->getContents();

				$obj = json_decode($response);
				$sessionKey = $obj->sessionKey;

				$reqUserDetail = $client->createRequest('GET', $this->host . 'users/byEmail/' . $encodedEmail . '/' . $sessionKey . '/' . $this->apiKey);
				$userDetail = $client->send($reqUserDetail);
				$user = $userDetail->getBody()->getContents();
				$userInfo = json_decode($user);

				Session::put('user_id', $userInfo->userId);
				Session::put('username', $userInfo->name);
				Session::put('role', $userInfo->roleName);
				Session::put('role_level', $userInfo->roleLevel);
				Session::put('loginTime', $obj->loginTime);
				Session::put('sessionTime', $obj->sessionTime);
				Session::put('sessionKey', $sessionKey);

        if(Session::get('role_level') == 4){
          $url = $this->host . "sensors/sensorByUser/".$userInfo->userId."/" . $this->apiKey;
          $response = Httpful::get($url)->send();
          Session::put('sensors', $response->body);
        }

				$reqUserConfig = $client->createRequest('GET', $this->host . 'dashboard/getConfiguration/' . $userInfo->userId . '/' . $sessionKey . '/' . $this->apiKey);
				$userConfig = $client->send($reqUserConfig);
				$config = $userConfig->getBody()->getContents();
				echo $config;
				if ($config == '[]') {
					$url = $this->host . "dashboard/saveNewConfiguration/" . $userInfo->userId . "/" . $sessionKey . '/' . $this->apiKey;
					$client = new \GuzzleHttp\Client;
					$client->setDefaultOption('verify', false);
					$req = $client->createRequest('POST', $url);
					$req->setHeader('Content-Type', 'application/x-www-form-urlencoded');

					$jsonString = '[{"position":0,"divId":"widget-1","type":"pie","selectedData":"topCountry"},{"position":1,"divId":"widget-2","type":"donut","selectedData":"topCountry"}]';
					$postBody = $req->getBody();
					$postBody->setField('configuration', $jsonString);
					$resp = $client->send($req);
				}
				return redirect('dashboard');
			}
		}

		catch (\GuzzleHttp\Exception\ClientException $e){
			if($e->getResponse()->getStatusCode() == 400){
				Session::flash('failure', 'Wrong Username or Password');
			}
			return redirect()->route('dashboard');
		}

        // $email = Input::get('email');
        // $password = Input::get('pass');
        echo $email ; 
        echo "<br>";
        echo $password;
        $encodedEmail = base64_encode($email);
        $encodedPassword = base64_encode($password);
        $url = $this->host . "users/login/" . $this->apiKey;
        $client = new \GuzzleHttp\Client;
        $req = $client->request('POST', $url);
        $req->setHeader('Content-Type', 'application/x-www-form-urlencoded');

        $postBody = $req->getBody();
        
        $postBody->setField('encodedEmail', $encodedEmail);
        $postBody->setField('encodedPassword', $encodedPassword);

        
        try {
			$resp = $client->send($req);
				if ($resp) {
				$response = $resp->getBody()->getContents();

				$obj = json_decode($response);
				$sessionKey = $obj->sessionKey;

				$reqUserDetail = $client->createRequest('GET', $this->host . 'users/byEmail/' . $encodedEmail . '/' . $sessionKey . '/' . $this->apiKey);
				$userDetail = $client->send($reqUserDetail);
				$user = $userDetail->getBody()->getContents();
				$userInfo = json_decode($user);

				Session::put('user_id', $userInfo->userId);
				Session::put('username', $userInfo->name);
				Session::put('role', $userInfo->roleName);
				Session::put('role_level', $userInfo->roleLevel);
				Session::put('loginTime', $obj->loginTime);
				Session::put('sessionTime', $obj->sessionTime);
				Session::put('sessionKey', $sessionKey);

        if(Session::get('role_level') == 4){
          $url = $this->host . "sensors/sensorByUser/".$userInfo->userId."/" . $this->apiKey;
          $response = Httpful::get($url)->send();
          Session::put('sensors', $response->body);
        }

				$reqUserConfig = $client->createRequest('GET', $this->host . 'dashboard/getConfiguration/' . $userInfo->userId . '/' . $sessionKey . '/' . $this->apiKey);
				$userConfig = $client->send($reqUserConfig);
				$config = $userConfig->getBody()->getContents();
				echo $config;
				if ($config == '[]') {
					$url = $this->host . "dashboard/saveNewConfiguration/" . $userInfo->userId . "/" . $sessionKey . '/' . $this->apiKey;
					$client = new \GuzzleHttp\Client;
					$client->setDefaultOption('verify', false);
					$req = $client->createRequest('POST', $url);
					$req->setHeader('Content-Type', 'application/x-www-form-urlencoded');

					$jsonString = '[{"position":0,"divId":"widget-1","type":"pie","selectedData":"topCountry"},{"position":1,"divId":"widget-2","type":"donut","selectedData":"topCountry"}]';
					$postBody = $req->getBody();
					$postBody->setField('configuration', $jsonString);
					$resp = $client->send($req);
				}
    			dd(Session::all());
				return Redirect::to('dashboard');
			}
		}

		catch (\GuzzleHttp\Exception\ClientException $e){
			if($e->getResponse()->getStatusCode() == 400){
				Session::flash('failure', 'Wrong Username or Password');
			}

			return Redirect::to('login');
		}


			
    }


    


   //  public function storeUser() {
   //      $rules = array(
   //          'name' => 'required',
   //          'telp' => 'required|numeric',
   //          'email' => 'required|email',
   //          'password' => 'required|min:6',
   //          'confirmPassword' => 'required|same:password|min:6'
   //      );
   //      $validator = Validator::make(Input::all(), $rules);

   //      if ($validator->fails()) {
   //          return Redirect::back()
   //                          ->withErrors($validator);
   //      } else {
			// $encodedEmail = base64_encode(Input::get('email'));
			// $encodedPassword = base64_encode(Input::get('pass'));

			// $url = $this->host . "users/add/" . Session::get('sessionKey') . "/" . $this->apiKey;
			// $client = new \GuzzleHttp\Client;
			// $client->setDefaultOption('verify', false);

			// $req = $client->createRequest('POST', $url);
			// $req->setHeader('Content-Type', 'application/x-www-form-urlencoded');

			// $postBody = $req->getBody();
			// $postBody->setField('name', Input::get('name'));
			// $postBody->setField('telp', Input::get('telp'));
			// $postBody->setField('encodedEmail', $encodedEmail);
			// $postBody->setField('encodedPassword', $encodedPassword);
			// $postBody->setField('instituteId', Input::get('institute'));
			// $postBody->setField('roleId', Input::get('role'));


			// try {
			// 	$resp = $client->send($req);
			// 	if($resp){
			// 		Session::flash('success', 'Successfully created User!');
			// 		return Redirect::to('userList');
			// 	}
			// }

			// catch (\GuzzleHttp\Exception\ClientException $e){
			// 	echo($e);
			// }

         
   //      }
   //  }



}
